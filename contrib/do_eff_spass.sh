#!/bin/bash

set -e

SPASS="../Fonderie/singlepass"

SRCDIR=$HOME"/Essais/PS-eye/frames/"
DSTPOL=$HOME"/TMP/"
echo "source   = " $SRCDIR
echo "spool    = " $DSTPOL

LINKFARM=$DSTPOL"LinkFarm"
echo "linkfarm = " $LINKFARM

# VIDZ="$HOME/BU/vrac/all_effects.mp4"
VIDZ="foo.mp4"
echo "lolvidz  = " $VIDZ

LINKNUM=0

#	--------------------------------------------

do_an_effect_pass()
{
local effect=$1
local ddir=$2

figlet "$effect" ; echo
echo "  files to ===> " $ddir

rm -f    $ddir/?????.png

$SPASS  -F $effect 				\
	-g $SRCDIR/'?????.fimg'			\
	-O $ddir				\
	-r 1

}

#	--------------------------------------------
insert_blank()
{
local count=$1

local imgname="$DSTPOL/blank.fimg"

if [ ! -r $imgname ] ; then
	mkfimg -v -t black $imgname 640 480
	fimg2png -v $imgname $DSTPOL/blank.png
	echo "blankimage done" | boxes
	# display $DSTPOL/blank.png &
	# exit
fi

for foo in $(seq 0 $count)
do
	linkname=$(printf "%s/L%05d.png" $LINKFARM $LINKNUM)
	ln --force --symbolic $DSTPOL/blank.png $linkname
	# file $linkname
	LINKNUM=$(( LINKNUM + 1 ))
done	
}
#	--------------------------------------------

make_the_linkfarm_from()
{
local effname=$1
local sdir=$2

echo	"====== Linkfarming from " $sdir 		\
	"====== avec" $(ls $sdir | wc -l) "images"

mogrify							\
	-font Utopia-Bold				\
	-pointsize 64					\
	-kerning 9					\
	-fill Gray90					\
	-stroke Gray10					\
	-strokewidth 2					\
	-gravity South-East				\
	-annotate +30+20 $effname			\
	$sdir/*.png

for img in $(ls -1 $sdir/?????.png)
do
	
	linkname=$(printf "%s/L%05d.png" $LINKFARM $LINKNUM)
	# echo "image = " $img 
	# echo "link  = " $linkname

	ln --force --symbolic $img $linkname

	LINKNUM=$(( LINKNUM + 1 ))
done
echo "          linkfarming done"
}
#	--------------------------------------------
#		traite tous les effets

do_all_the_effects()
{
EFFECTS=$( $SPASS -L | sort )

banner 'FULL RUN'

for effect in $EFFECTS
do
	DDIR=$DSTPOL"/$effect"

	if [ -d $DDIR ] ; then
		rm -f $DDIR"/?????.png"
	fi
	if [ ! -r $DDIR ] ; then
		mkdir -v $DDIR
	fi

	do_an_effect_pass $effect $DDIR
	make_the_linkfarm_from $effect $DDIR

	insert_blank 30
done
}

#	--------------------------------------------

debug_run()
{
local eff=$1
local DDIR=$DSTPOL"/$eff"

banner 'DEBUG RUN'
echo "DDIR = " $DDIR

do_an_effect_pass       $eff  $DDIR
make_the_linkfarm_from  $eff  $DDIR
}

#	--------------------------------------------
#			MAIN

echo 

rm -v -f $LINKFARM/L*.png ; echo

insert_blank 30

# debug_run 'rndblks'
do_all_the_effects

banner 'encoding'

ffmpeg  -nostdin                                                \
	-loglevel   warning					\
        -y -r 30 -f image2 -i ${LINKFARM}/L%05d.png		\
	-metadata artist='---[ tTh ]---'			\
	-metadata title='---[ All the 'Fonderie' effects ]---'	\
 	-preset veryslow					\
        -c:v libx264 -pix_fmt yuv420p                            \
        $VIDZ

echo "   encoding of " $VIDZ " . . . . . [done]"


