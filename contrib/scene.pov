
/*    scene demo conversion floatimg -> height field  */

#version 3.7;

global_settings {
  ambient_light rgb <0.07, 0.07, 0.07>
  assumed_gamma 1.0
  }

#include  "colors.inc"

height_field {
	tga "hf.tga"
	smooth
	pigment { color Orange*0.7  }
	translate <-0.5, 0, -0.5>
	// scale 2
	translate y*0.002
	}

camera	{
	location	<-0.86, 4, -6.20>
	look_at		<0, 0, 0>
	angle		14
	}

plane {
 <0, 1, 0>, 0
 pigment {
   hexagon 
   pigment { color Gray20 },
   pigment { color Gray10 },
   pigment { color Gray30 }
   }
  scale 0.27
 }


light_source { <-9, 2,  3> color White }

