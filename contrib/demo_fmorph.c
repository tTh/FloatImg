/*
 *		DEMONSTRATOR FOR FMORPHO
 */

#include  <stdio.h>
#include  <stdlib.h>

#include  <floatimg.h>

int		verbosity = 1;

#define    DF_W   512
#define    DF_H   128

/* ~~~ -------------------------------------------------------------- +++ */
int demo_fmorph(char *srcf, char *dsti, int mode)
{
FloatImg		simg, 		// image source
			timg, uimg,	// intermediaires
			dimg;		// destination
FimgArea51		rect;
char			line[200];

int			foo, pass, hpos;

fprintf(stderr, ">>> %s ( '%s' '%s' %d )\n", __func__, srcf, dsti, mode);

/*
 *	chargement image source
 */
foo = fimg_create_from_dump(srcf, &simg);
fprintf(stderr, "    load of %s --> %d\n", srcf, foo);
if (foo) {
	return foo;
	}

/*
 *	images temporaires
 */
foo = fimg_create(&timg, DF_W, DF_H, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "err %d create T img\n", foo);
	return foo;
	}
foo = fimg_create(&uimg, DF_W, DF_H, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "err %d create U img\n", foo);
	return foo;
	}

/*
 *	image de destination
 */
foo = fimg_create(&dimg, DF_W, DF_H*9, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "err %d create D img\n", foo);
	return foo;
	}

/*
 *	this is an iterator 
 */
rect.x = 20;		rect.y = 20;
rect.w = DF_W;		rect.h = DF_H;

for (pass=0; pass<9; pass++) {
	hpos = pass * DF_H;
	fprintf(stderr, "pass %d, hpos %d\n", pass, hpos);
	foo = fimg_extractor(&simg, &timg, &rect);

	// sprintf(line, "S_%02d.png", pass);
	// foo = fimg_save_as_png(&timg, line, 0);

	foo = fimg_filtre_morpho_0(&timg, &uimg, pass);
	if (foo) {
		fprintf(stderr, "err %d filtre morpho, pass %d\n", 
					foo,			pass);
		exit(1);
		}
	sprintf(line, "D_%02d.png", pass);
	foo = fimg_save_as_png(&uimg, line, 0);

	foo = fimg_incrustator_0(&uimg, &dimg, 0, hpos, 0);
	if (foo) {
		fprintf(stderr, "err %d incrustator\n", foo);
		exit(1);
		}
	}

foo = fimg_save_as_png(&dimg, dsti, 0);
/*
 *	do some cleanup
 */
fimg_destroy(&simg); fimg_destroy(&timg); fimg_destroy(&dimg);


return 0;
}
/* ~~~ -------------------------------------------------------------- +++ */
int main(int argc, char *argv[])
{
int		foo;

fimg_print_version(1);

if (3 != argc) {
	fprintf(stderr, "usage:\n\t%s src.fimg dst.png\n", argv[0]);
	exit(1);
	}

foo = demo_fmorph(argv[1], argv[2], 0);
fprintf(stderr, "    got --> %d\n", foo);

return 0;
}
/* ~~~ -------------------------------------------------------------- +++ */
