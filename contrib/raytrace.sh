#!/bin/bash

src="/dev/shm/foo.fimg"
dst="hf.tga"
TMPDIR=${HOME}/TMP

POVOPT="-w512 -h342 +q9 -a "

rm $TMPDIR/hf???.png

for idx in $(seq 0 60)
do

	echo "========================== " $idx

	grabvidseq -v				\
		-d /dev/video0 -s 640x480	\
		-n 60 -p 1.0			\
		-o ${src} 

	./fimg2povhf $src $dst

	out=$(printf "%s/hf%03d.png" $TMPDIR $idx)
	echo "raytracing   " ${POVOPT} $out
	povray -iscene.pov ${POVOPT}  -o${out} 2> pov.stderr
	# tail -15 pov.stderr

done

convert -delay 10 -colors 240 $TMPDIR/hf???.png foo.gif

