#!/bin/bash

# 		CAPTURE

source ./conf.sh

DEVICE="/dev/video0"
GOPT=" -v -s 1920x1080  -p 0 -n 10"
OUTD=$GRABDIR
TMPG="/dev/shm/tmpgrab.fimg"

for idx in $(seq  0 2399)
do

	# take the picz
	${GRAB} -d ${DEVICE} ${GOPT} -o ${TMPG}

	# display $TMPG

	# only take the good spot
	fimg=$(printf "%s/%05d.fimg" ${OUTD} ${idx})
	${EXTR}  -o ${fimg} ${TMPG} 800,600,560,240

	# display ${fimg} ;  exit

	echo
	sleep $DELAY_GRAB

done

