#!/bin/bash

CMDPATH="/usr/local/bin"

WS="${HOME}/TMP"		# our workspace

GRABOPT=" -d /dev/video0 -v -n 240 -p 0.04 "

GRAB=${WS}/"quux.fimg"
COS01=${WS}/"cos01.fimg"
COS010=${WS}/"cos010.fimg"
SQRT=${WS}/"sqrt.fimg"
POW2="${WS}/pow2.fimg"

# --------- conversion fimg -> pnm
f2p ()
{
dst=EC_$(basename $1 .fimg).pnm
echo == converting $1 to $dst
${CMDPATH}/fimg2pnm  $1 ${WS}/$dst
}

# --------- capturer une image
#
${CMDPATH}/grabvidseq ${GRABOPT} -o ${GRAB}
echo ERRCODE $?
f2p ${GRAB} 

# ----------- traitements
#
${CMDPATH}/fimgfx pow2 $GRAB $POW2
f2p $POW2

${CMDPATH}/fimgfx sqrt $GRAB $SQRT
f2p $SQRT

${CMDPATH}/fimgfx cos01 $GRAB $COS01
f2p $COS01

${CMDPATH}/fimgfx cos010 $GRAB $COS010
f2p $COS010

# ----------- présentation finale
#
echo == Making gif89a
convert -delay 40 ${WS}/EC_*.pnm foo.gif
