
# Sat Jul  8 10:41:58 UTC 2023

#
#	location of some tools
#

GRAB="$HOME/Devel/FloatImg/v4l2/grabvidseq"
MKFX="$HOME/Devel/FloatImg/tools/fimgfx"
MDAT="$HOME/Devel/FloatImg/tools/fimgmetadata"
EXTR="$HOME/Devel/FloatImg/tools/fimgextract"

INTERPOLATOR="$HOME/Devel/FloatImg/Fonderie/interpolator"
FONDERIE="$HOME/Devel/FloatImg/Fonderie/fonderie"
SINGLEPASS="$HOME/Devel/FloatImg/Fonderie/singlepass"

#
#	working directories
#
GRABDIR="rush"
GIFDIR="gif89a"

#
#	text plotting conf
FONT="Noto-Sans-Bold"
KERNING=1
SIGNATURE="... tTh 2023 ..."

#
#	filter chains for fondulations
#
IF="cos01:colmixa:pow2"
OF="shiftln0:liss3x3"

