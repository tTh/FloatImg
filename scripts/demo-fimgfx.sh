#!/bin/bash

source="00000.fimg"
tmpimg="/dev/shm/demofx.fimg"

effects=$(fimgfx -l)

for fx in $effects
do

	echo "============= " $fx
	dst="${fx}.png"
	echo "       " ${source} ${dst}

	fimgfx -v ${fx} ${source} ${tmpimg}

	convert $tmpimg				\
		-fill black -stroke white	\
		-strokewidth 1			\
		-pointsize 34			\
		-font "Noto-Sans-Bold"		\
		-gravity South-East		\
		-annotate +32+10 "${fx}"	\
		${dst}

done

