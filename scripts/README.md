# Exemples de scripts

_Attention_, ce ne sont que des exemples, pas forcément adaptés
à une utilisation dans le monde réel. Mais vous pouvez vous en
inspirer pour vos usecases personnels.

## shoot.sh

Front-end de prise de photographies floues. C'est un script assez
simple à configurer : les valeurs par défaut fonctionnent.
Il faut juste renseigner l'emplacement de votre `grabviseq`
en début du script.

## contrast-test.sh

Démonstrateur d'ajustements de contraste. La configuration est
en dur dans le code.

## echomix.sh

Comment générer des videos psychotiques avec un peu de bash.
Ce script est expliqué dans la documentation PDF.

## capture.sh & conf.sh

Ce [script](./capture.sh) sert à capturer une séquence d'image depuis
une webcam.
Deux choses à noter : il utilise un fichier de configuration
(`source [./conf.sh](./conf.sh)) pour ajuster son fonctionnement, et il
extrait juste une portion de l'image capturée. Une belle avancée,
puisque cela ajoute quelques possibilités de recadrage dès
la prise de vue.

