#!/usr/bin/env bash

echo "=== make a lot of float img ==="

MKFIMG="../tools/mkfimg"
SIZE=" 640x480 "

for type in $(${MKFIMG} -L)
do
	picname="/tmp/${type}.fimg"
	echo $picname
	${MKFIMG} -v -t $type $picname $SIZE
done
