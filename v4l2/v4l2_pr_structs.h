/*
 *	fonctions pour afficher les structures de V4L2
 *
 *	WARNING : this is a work in progress !
 */

/* --------------------------------------------------------------------- */
int pr_v4l2_capability(char *txt, struct v4l2_capability *ptr);
int pr_v4l2_input(char *txt, struct v4l2_input *ptr);
int pr_v4l2_format(char *txt, struct v4l2_format *ptr);
int pr_v4l2_requestbuffers(char *txt, struct v4l2_requestbuffers *ptr);
int pr_v4l2_fmtdesc(char *txt, struct v4l2_fmtdesc *ptr);

char *str_input_type(int t);
char *str_ctrl_type(int type);
char *str_buf_type(int type);
char *str_fourcc(uint32_t fcc);		/* NOT REENTRANT */

void pr_ctrl_id(uint32_t id);		/* bit dissector */

/* --------------------------------------------------------------------- */

