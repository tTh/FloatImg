#!/bin/bash

#  -----------------------------------------------------

FIMG="bar.fimg"
CAM="/dev/video0"

clear

#  -----------------------------------------------------
SEQ=" -n 60 -p 0 "
./grabvidseq -vv -m -d $CAM $SEQ -o $FIMG

echo ; echo ================= STATS ======================
../tools/fimgstats -v  $FIMG

exit

echo
echo ================ METADATA ====================
../tools/fimgmetadata -v timestamp $FIMG
echo 
../tools/fimgmetadata -v all $FIMG

#  -----------------------------------------------------
