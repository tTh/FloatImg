# Images en virgule flottante, video 4 linux

Quelques gruikwares en rapport avec la capture vidéo.


## grabvidseq

Logiciel de capture d'une image flottante par accumulation de trames.

```
tth@lubitel:~/Devel/FloatImg/v4l2$ ./grabvidseq -h
options :
        -d /dev/?       select video device
        -g              convert to gray
        -n NNN          how many frames ?
        -O ./           set Output dir
        -o bla.xxx      set output filename
        -p NN.N         period in seconds
        -r 90           rotate picture
        -s WxH          size of capture
        -c mode         contrast enhancement
        -u              try upscaling...
        -v              increase verbosity
```

Les formats d'exportation sont : Fimg, Png, Tiff, Pnm et Fits. Chacun d'eux
ayant son propre lot de limitations et de bugs. Le choix est fait
selon l'extension du nom de fichier demandé~: ".fimg", ".tiff"...

## video-infos

```
Options :
        -e N            examine that, please
        -d              select the video device
        -K              set the K parameter
        -l              list video devices
        -T bla          add a title
        -v              increase verbosity
```

