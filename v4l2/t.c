/*
 *		tests pour capturer les webcams
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <math.h>
#include  <string.h>
#include  <sys/ioctl.h>
#include  <inttypes.h>
#include  <linux/videodev2.h>

#include  "../floatimg.h"
#include  "funcs.h"

#include  "v4l2_pr_structs.h"


int		verbosity;

/* --------------------------------------------------------------------- */
int essai_get_fmt(char *dev, int k)
{
int				vfd, foo;
struct v4l2_format		fmt;
// struct v4l2_requestbuffers      reqbuf;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, dev, k);
#endif

vfd = open_device(dev);
if (verbosity) fprintf(stderr, "\topen %s -> %d\n", dev, vfd);

	memset(&fmt, 0, sizeof(fmt));
	fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	foo = ioctl(vfd, VIDIOC_G_FMT, &fmt);
	if (verbosity) fprintf(stderr, "%s: ioctl -> %d\n", __func__, foo);
	if (0 != foo)	{
		perror("ioctl G_FMT");
		return -1;
		}

pr_v4l2_format("after ioctl VIDIOC_G_FMT", &fmt);

/* this function is bugged */

close(vfd);


return k;
}
/* --------------------------------------------------------------------- */

void help(int k)
{
puts("Options :");
puts("\t-d\tselect the video device");
puts("\t-K\tset the K parameter");
puts("\t-v\tincrease verbosity");

if (verbosity) { puts(""); fimg_print_version(1); }

exit(0);
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;

char		*device = "/dev/video0";
int		K = 0;

while ((opt = getopt(argc, argv, "d:hK:v")) != -1) {
	switch(opt) {
		case 'd':	device = optarg;	break;
		case 'h':	help(0);		break;
		case 'K':	K = atol(optarg);	break;
		case 'v':	verbosity++;		break;
		}
	}

if (verbosity) fimg_print_version(0);

foo = essai_get_fmt(device, K);
if (verbosity || foo) fprintf(stderr, "%s: essai -> %d\n", __func__, foo);

return 0;
}
/* --------------------------------------------------------------------- */


