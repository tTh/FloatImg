/*
 *		UGLY CODE INSIDE
 */
#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include "../floatimg.h"

#include  "funcs.h"

extern	int	verbosity;

/*
 *		Be careful, these functions are not yet fireproof,
 *		and calling conventions are fluctuating.
 */

/* --------------------------------------------------------------------- */
int x_upscaler_0(unsigned char *src, int w, int h, FloatImg *d)
{
int				x, y, xx, yy, ox, oy;
// float				*rp, *gp, *bp;
float				r, g, b;
static unsigned short		modz;

/*
 *	check in image sizes are correct
 */
if ( d->width != w*2 || d->height != h*2 ) {
	fprintf(stderr, "%s: dimension error\n", __func__);
	fprintf(stderr, "\tw = %d   h = %d\n", w, h);
	fprintf(stderr, "\tdest image is %dx%d\n", d->width, d->height);
#if MUST_ABORT
	abort();
#endif
	return -2;
	}

ox = ! ! (modz & 2);
oy = ! ! (modz & 1);

if (verbosity>2)	fprintf(stderr, "%s   %5d  %d %d\n", __func__,
					modz, ox, oy);

for (y=0; y<h; y++) {
	yy = (y*2) + oy;
	for (x=0; x<w; x++) {
		xx = (x*2) + ox;
		r = (float)*src++;
		g = (float)*src++;
		b = (float)*src++;

		/* may be, here, we can speed up the job */
		fimg_add_rgb(d, xx, yy, r, g, b);
		/* or may be jump directly to asm and SSE2
		   http://www.mikekohn.net/stuff/image_processing.php */
		}
	} 

modz++;			/* next displacment index */

if ( ! (modz & 0x03)) {
	d->count++;		/* one more frame in the accumulator */
	}

return -1;
}
/* --------------------------------------------------------------------- */
int x_rgb2fimg(unsigned char *src, int w, int h, FloatImg *d)
{
int			iter, size;
float			*rp, *gp, *bp;

size = w * h;
rp = d->R, gp = d->G, bp = d->B;

for (iter=0; iter<size; iter++) {
	*rp++ = (float)*src++;
	*gp++ = (float)*src++;
	*bp++ = (float)*src++;
	}

return 0;
}
/* --------------------------------------------------------------------- */
int x_add_rgb2fimg(unsigned char *src, int w, int h, FloatImg *d)
{
int			iter, size;
float			*rp, *gp, *bp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %d %d  %p )\n", __func__,
				src, w, h, d);
#endif

size = w * h;
rp = d->R, gp = d->G, bp = d->B;

for (iter=0; iter<size; iter++) {
	*rp++ += (float)*src++;
	*gp++ += (float)*src++;
	*bp++ += (float)*src++;
	}

d->count++;		/* one more frame in the accumulator */

return 0;
}
/* --------------------------------------------------------------------- */

int x_rgb2file(unsigned char *src, int w, int h, char *fname)
{
FloatImg		buff;
int			foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d '%s' )\n", __func__,
			src, w, h, fname);
#endif

foo = fimg_create(&buff, w, h, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "Crash on create in %s %s\n", __FILE__, __func__);
	exit(1);
	}

foo = x_rgb2fimg(src, w, h, &buff);
if (foo) {
	fprintf(stderr, "Crash on bit massage in %s %s\n", __FILE__, __func__);
	exit(1);
	}

foo = fimg_dump_to_file(&buff, fname, 0);
if (foo) {
	fprintf(stderr, "Crash on dump in %s %s\n", __FILE__, __func__);
	exit(1);
	}

fimg_destroy(&buff);

return -1;
}
/* --------------------------------------------------------------------- */
