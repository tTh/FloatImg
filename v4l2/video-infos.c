/*
 *		tests pour capturer les webcams
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <math.h>
#include  <string.h>
#include  <sys/ioctl.h>
#include  <errno.h>
#include  <inttypes.h>
#include  <linux/videodev2.h>

#include  "../floatimg.h"

#include  "v4l2_pr_structs.h"
#include  "funcs.h"

int		verbosity;

/* --------------------------------------------------------------------- */
/*
 *	this code was written from a strace output :)
 */
int enum_image_framesizes(int fd, char *txt, int k)
{
int				foo, idx;
struct v4l2_frmsizeenum		fmtsz;

printf("## image framesizes enumeration (%s)\n", txt);

for (idx=0; ; idx++) {
	memset(&fmtsz, 0, sizeof(fmtsz));
	fmtsz.index = idx;
	foo = ioctl(fd, VIDIOC_ENUM_FRAMESIZES, &fmtsz);

	if (foo) {
		if (EINVAL==errno) {
			break;
			}
		else	{
			perror(__func__);
			break;
			}
		}

	printf("%4d   %4d   %4d\n", idx, fmtsz.pixel_format, fmtsz.type);

	}

return 0;
}
/* --------------------------------------------------------------------- */
int enum_image_formats(int fd, char *txt, int k)
{
int				foo, idx;
struct v4l2_fmtdesc		fmtd;

printf("## image formats enumeration (%s)\n", txt);

idx = 0;
for (;;) {
	memset(&fmtd, 0, sizeof(fmtd));
	fmtd.index = idx;
	fmtd.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

	foo = ioctl(fd, VIDIOC_ENUM_FMT, &fmtd);
	// fprintf(stderr, "B idx=%d, foo=%d, errno=%d\n", idx, foo, errno);
	if (foo) {
		if (EINVAL==errno) {
			break;
			}
		else	{
			perror(__func__);
			break;
			}
		}

	// pr_v4l2_fmtdesc(__func__, &fmtd);
	printf(" %2d  %-10s 0x%02x   %s   %-32s \n", 
		fmtd.index, str_buf_type(fmtd.type), fmtd.flags, 
			str_fourcc(fmtd.pixelformat), fmtd.description);

	idx++;
	}
return -1;
}
/* --------------------------------------------------------------------- */
static int enum_inputs(int fd, char *txt, int k)
{
int 				index, foo;
struct v4l2_input		input;
char				ligne[50];

printf("## inputs enumeration (%s)\n", txt);

index = 0;
for(;;)	{
	memset (&input, 0, sizeof (input));
	input.index = index;
	foo = ioctl(fd, VIDIOC_ENUMINPUT, &input);
	if (foo) {
		if (EINVAL==errno) { break; }
		else	{
			perror("enuminput");
			return -1;
			}
		}

	if (verbosity) {
		sprintf(ligne, "input %d", index);
		pr_v4l2_input(ligne, &input);
		}
	else	{
		printf("%-32s | %-10s\n",	input.name,
				str_input_type(input.type));
		}

	index++;
	}

return 0;
}
/* --------------------------------------------------------------------- */

int enum_controls(int fd, char *txt, int k)
{
struct v4l2_queryctrl		qctrl;
int				idx;


printf("## controls enumeration '%s'\n", txt);

memset (&qctrl, 0, sizeof (qctrl));

/* V4L2_CID_BASE defined in linux/v4l2-controls.h */

for (idx=V4L2_CID_BASE; idx<V4L2_CID_LASTP1; idx++) {

	qctrl.id = idx;

	// if (verbosity>1) printf("  id %d  ", idx);

	if (0 == ioctl (fd, VIDIOC_QUERYCTRL, &qctrl)) {
		if (qctrl.flags & V4L2_CTRL_FLAG_DISABLED) {
			printf("disabled\n");
			continue;
			}

		printf("   %-40s    %-10s  [%d..%d]\n",
			qctrl.name,
			str_ctrl_type(qctrl.type),
			qctrl.minimum, qctrl.maximum);

		}
	else if (EINVAL==errno) {
#if DEBUG_LEVEL
		if (verbosity) fprintf(stderr, "id %d einval\n", idx);
#endif
		continue;
		}
	else	{
		printf("err %d %s\n", errno, strerror(errno));
		}
	fflush(stdout); fflush(stderr);
	}

return -1;
}
/* --------------------------------------------------------------------- */
/*
 *	code based on :
https://www.linuxtv.org/downloads/legacy/video4linux/API/V4L2_API/spec-single/v4l2.html
 *
 */
int enum_extended_controls(int fd, char *txt, int k)
{
struct v4l2_queryctrl		qctrl;
int				idx;

printf("##- extended controls enumeration '%s'\n", txt);

memset(&qctrl, 0, sizeof(qctrl));
qctrl.id = V4L2_CTRL_FLAG_NEXT_CTRL;

idx = 0;
while (0 == ioctl (fd, VIDIOC_QUERYCTRL, &qctrl)) {

	if (verbosity) pr_ctrl_id(qctrl.id);
	printf("  %-32s   %-10s  [%d..%d]\n",
		qctrl.name,
		str_ctrl_type(qctrl.type),
		qctrl.minimum, qctrl.maximum);

	qctrl.id |= V4L2_CTRL_FLAG_NEXT_CTRL;
	idx++;
	}

return -1;
}
/* --------------------------------------------------------------------- */
int show_webcam_infos(char *devname, char *title, int k, int type)
{
int			vfd, foo;
char			ligne[100];

struct v4l2_capability		cap;
struct v4l2_format		fmt;
// struct v4l2_input		input;
// int 				index;

// struct v4l2_requestbuffers      reqbuf;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d %d)\n", __func__, devname, k, type);
#endif

vfd = open_device(devname);
if (vfd < 0) {
	perror(devname);
	return -3;
	}

fprintf(stderr, "\topen %s -> %d\n", devname, vfd);

memset(&cap, 0, sizeof(cap));
foo = ioctl(vfd, VIDIOC_QUERYCAP, &cap);
if (foo < 0) {
	perror("ioctl QUERYCAP");
	return -4;
	}
pr_v4l2_capability(devname, &cap);

foo = enum_inputs(vfd, "on peut voir quoi ?", 0);


memset(&fmt, 0, sizeof(fmt));
fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
foo = ioctl(vfd, VIDIOC_G_FMT, &fmt);
if (0 != foo)	{
	perror("ioctl G_FMT");
	}
else	{
	pr_v4l2_format("Experimental", &fmt);
	}

if (type) {

			;

	}
else	{
	foo = enum_image_formats(vfd, "Experimental", 0);
	foo = enum_controls(vfd, "is that working ?", 0);
	foo = enum_extended_controls(vfd, "looking for extended", 0);
	enum_image_framesizes(vfd, "code pas fini", 0);
	}

close(vfd);

return 0;
}
/* --------------------------------------------------------------------- */
int liste_des_devices(int flag)
{
fprintf(stderr, "%s not implemented\n", __func__);
return -1;
}
/* --------------------------------------------------------------------- */
static void help(int k)
{
puts("Options :");
puts("\t-e N\t\texamine that, please");
puts("\t-d\t\tselect the video device");
puts("\t-K\t\tset the K parameter");
puts("\t-l\t\tlist video devices");
puts("\t-T bla\t\tadd a title");
puts("\t-v\t\tincrease verbosity");

// if (verbosity)
{ puts(""); fimg_print_version(1); }
exit(0);
}
/* --------------------------------------------------------------------- */
static void print_title(char *txt)
{
int	foo, l;

l = strlen(txt);
for (foo=0; foo<l+18; foo++)
	putchar('*');
puts("");
printf("******   %s   ******\n", txt);
for (foo=0; foo<l+18; foo++)
	putchar('*');
puts("\n");
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;
int		etype = 0;
char		*device = "/dev/video0";
char		*title = NULL;
int		K = 0;

while ((opt = getopt(argc, argv, "d:e:hK:lT:v")) != -1) {
	switch(opt) {
		case 'd':	device = optarg;	break;
		case 'e':	etype = atol(optarg);	break;
		case 'h':	help(0);		break;
		case 'K':	K = atol(optarg);	break;
		case 'l':	liste_des_devices(0);	break;
		case 'T':	title = optarg;		break;
		case 'v':	verbosity++;		break;
		}
	}

if (NULL != title) {
	print_title(title);
	}

foo = show_webcam_infos(device, "", K, etype);
fprintf(stderr, "\n\tshow_webcam_infos -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */


