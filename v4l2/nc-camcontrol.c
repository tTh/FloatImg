/*
 *		tests pour capturer les webcams
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <math.h>
// #include  <string.h>
#include  <sys/ioctl.h>
#include  <errno.h>
#include  <inttypes.h>
#include  <curses.h>
#include  <linux/videodev2.h>

#include  "../floatimg.h"

#include  "v4l2_pr_structs.h"
#include  "funcs.h"

int		verbosity;

/* --------------------------------------------------------------------- */
void help(int n)
{

puts("camera controls");
puts("\t-d bla\t\tselect video device");
puts("\t-e nnn\t\tset 'etype'");
puts("\t-K nnn\t\tinteger parameter");
puts("\t-n bla\t\tset title");
exit(0);
}
/* --------------------------------------------------------------------- */
int init_screen(char *title)
{
int			foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, title);
#endif

initscr();

standout(); mvaddstr(1, 5, title); standend(); refresh();


return -1;
}
/* --------------------------------------------------------------------- */
int end_screen(void)
{
endwin();
return 0;
}
/* --------------------------------------------------------------------- */
int preparation_v4l2(char *devname, int param)
{
int			fd, foo;
struct v4l2_capability	cap;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, devname, param);
#endif

fd = open_device(devname);
if (fd < 0) {
	fprintf(stderr, "err %d on %s opening\n", errno, devname);
	return -2;
	}

/* est-ce un device qui permet la capture video */
foo = ioctl(fd, VIDIOC_QUERYCAP, &cap);
if (-1 == foo) {
	perror("VIDIOC_QUERYCAP");
	return -2;
	}
if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)) {
	fprintf(stderr, "%s is not a video capture device\n", devname);
	return -3;
	}

return fd;
}
/* --------------------------------------------------------------------- */
int interactive(int fd, char *text, int notused)
{

fprintf(stderr, "file descriptor = %d\n", fd);

init_screen("prototype");

sleep(2);

end_screen();

return 0;
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt, devnum;
int		etype = 0;
char		*device = "/dev/video0";
char		*title = NULL;
int		K = 0;

while ((opt = getopt(argc, argv, "d:e:hK:lT:v")) != -1) {
	switch(opt) {
		case 'd':	device = optarg;	break;
		case 'e':	etype = atol(optarg);	break;
		case 'h':	help(0);		break;
		case 'K':	K = atol(optarg);	break;
		// case 'l':	liste_des_devices(0);	break;
		case 't':	title = optarg;
		case 'v':	verbosity++;		break;
		}
	}

devnum = preparation_v4l2(device, K);
if (devnum < 0) {
	fprintf(stderr, "%s : erreur init video device\n", argv[0]);
	exit(1);
	}

foo = interactive(devnum, title, 0);

return 0;
}
/* --------------------------------------------------------------------- */
