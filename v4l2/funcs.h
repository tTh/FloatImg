/*
 *	V4L2 functions - header file
 */

int open_device(char *dev_name);

int init_device(int notused);

/* --------------------------------------------------------------------- */

int x_upscaler_0(unsigned char *src, int w, int h, FloatImg *d);

int x_add_rgb2fimg(unsigned char *src, int w, int h, FloatImg *d);

int x_rgb2fimg(unsigned char *src, int w, int h, FloatImg *d);
int x_rgb2file(unsigned char *src, int w, int h, char *fname);

/* --------------------------------------------------------------------- */


