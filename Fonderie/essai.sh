#!/bin/bash



# ------------------------------------------------------------
essai_filtres ()
{
FILTRES=$(./t -L | awk 'NR>1 { print $1 }' | sort)

rm /tmp/fstack*.png

SRC=$(ls -rt1 $HOME/Essais/FondageDePlomb/capture/* | tail -1)
# SRC=mire.fimg

for F in $FILTRES
do
	I="/tmp/fstack-"$F".png"
	echo ; echo ======== $I
	./t -v -i $SRC -F $F -o foo.png
	txt=$(printf "( %-10s )" $F)
	convert foo.png -pointsize 48 -kerning 0	\
		-fill Gray80 -undercolor Gray20		\
		-font Courier-Bold			\
		-annotate +10+50 "$txt"			\
		$I
done

echo ; echo "making gif89a..."
convert -delay 200 /tmp/fstack*.png foo.gif
}
# ------------------------------------------------------------
essai_singlepass ()
{
MP4="/home/tth/Essais/FondageDePlomb/foo.mp4"
INPUT="/home/tth/Essais/FondageDePlomb/capture/02[0123]??.fimg"
FILTRE="multidots:liss3x3:nothing:liss3x3:liss3x3"
OUTDIR="/tmp/x8/"

echo '********* essai single *********'

rm $OUTDIR/*.png

time ./singlepass -v -g "$INPUT" -F $FILTRE -O $OUTDIR -s

echo ; echo "encoding picz to " $MP4

ffmpeg	-nostdin				\
	-loglevel error					\
	-y -r 25 -f image2 -i /tmp/x8/%05d.png	 	\
	-c:v libx264 -pix_fmt yuv420p			\
	$MP4

}
# ------------------------------------------------------------
#			MAIN

essai_filtres

# ------------------------------------------------------------
