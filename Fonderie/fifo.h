/*
 *		Machine qui fait des fils flous par la
 *		méthode de la moyenne mobile.
 */
/* -------------------------------------------------------------- */

typedef struct {
	unsigned long	magic;
	int		nbslots;	/* how many pics used ? */

	FloatImg	total;		/* computing workspace */
	FloatImg	wspace;		/* computing workspace */

	FloatImg	*slots;		/* circular buffer */

	int		next;		/* incremented with modulo */

	int		export_type;	/* fimg/png/pnm/... */
	} A_Fifo;

#define MAGIC_FIFO	0xabcd9876

typedef struct {
	unsigned long	magic;

	int		blanks;		/* ???? */
	float		fk;
	int		preproc, postproc;

	int		step;		/* for fifo export */

	} ControlBloc;

#define MAGIC_CTRLB	0xabcdfefe

/* -------------------------------------------------------------- */

int create_fifo(int nbslot, int w, int h, int t);
int set_fifo_output_format(int filetype);

int export_fifo(char *fname, int notused);

int insert_picture(FloatImg *src);


/* -------------------------------------------------------------- */
/*
 *		funcs in 'sfx.c'  ---> sfx.h
 */
