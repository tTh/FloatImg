#!/usr/bin/awk -f

#
#		this utility script make a file who
#		is includet by 'crapulator.c'
#
BEGIN	{
	print "// -------------------------------------"
	print "// generated file, do not edit by hand !";
	print "// -------------------------------------"
	print "Crapulor  CrapL[] = {";
	}

#		$1 is the badly brain-designed numeric id
#		$2 is the user name of the filter
#		$3 and $4 are two not used parameters
#
	{
	name = sprintf("CR_%s", $2)
	printf "   { %-14s,  \"%s\",       %d, %f },      // #%d\n",
                        name,    $2,   $3, $4,           $1;
	}

END	{
	print "   { -1, NULL }"
	print " };"
	print "// ! generated file, do not edit by hand !"
	}
