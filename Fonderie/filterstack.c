/*
 *		filterstack.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <string.h>
#include  <ctype.h>
#include  <alloca.h>

#include  "../floatimg.h"
#include  "crapulator.h"
#include  "filterstack.h"

// #undef  DEBUG_LEVEL
// #define DEBUG_LEVEL 1

/* -------------------------------------------------------------- */

extern int		verbosity;

static	FilterStack 	f_stacks[NUMBER_OF_STACK];

/* -------------------------------------------------------------- */
int filterstack_init(int numid, int notused)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d %d )\n", __func__, numid, notused);
#endif

if (numid < 0 || numid > NUMBER_OF_STACK) {
	fprintf(stderr, "%s: slot number %d is invalid\n", __func__, numid);
	exit(1);
	}

memset(&f_stacks[numid], 0, sizeof(FilterSlot));

return 0;
}
/* -------------------------------------------------------------- */
int filterstack_add(int numid, int code, int ival, float fval)
{
int		idxsl;
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d %d %f )\n", __func__, code, ival, fval);
#endif

if (numid < 0 || numid > NUMBER_OF_STACK) {
	fprintf(stderr, "%s: slot number %d is invalid\n", __func__, numid);
	exit(1);
	}

if (f_stacks[numid].count == FILTER_BY_STACK) {
	fprintf(stderr, "%s: stack is full\n", __func__);
	return -1;
	}

idxsl = f_stacks[numid].count;			/* aliasing */

f_stacks[numid].slots[idxsl].numero = code;
f_stacks[numid].slots[idxsl].ival   = ival;
f_stacks[numid].slots[idxsl].fval   = fval;

f_stacks[numid].count++;

return 0;
}
/* -------------------------------------------------------------- */
int filterstack_list(int numid, const char *txt)
{
int		idx;

if (numid < 0 || numid > NUMBER_OF_STACK) {
	fprintf(stderr, "%s: slot number %d is invalid\n", __func__, numid);
	exit(1);
	}

fprintf(stderr, "---- %2d ---- %-20s -----------\n", numid, txt);
// fprintf(stderr, "stack at %p, size %d, current %d\n",
//			f_slots, nbre_filters, idx_slot); 
fprintf(stderr, "idx ___ fx# _ name ________ ival _ fval ___\n");

for (idx=0; idx<f_stacks[numid].count; idx++) {

	fprintf(stderr, "%3d    %3d    %-10s  %3d      %f\n", idx,
			f_stacks[numid].slots[idx].numero,
	crap_name_from_number(f_stacks[numid].slots[idx].numero),
			f_stacks[numid].slots[idx].ival,
			f_stacks[numid].slots[idx].fval);

	}

return 0;
}
/* -------------------------------------------------------------- */
int filterstack_run(int numid, FloatImg *target, int notused)
{
int		idx, foo, eff;
float		fv;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, target, notused);
#endif

if (numid < 0 || numid > NUMBER_OF_STACK) {
	fprintf(stderr, "%s: slot number %d is invalid\n", __func__, numid);
	exit(1);
	}

if (0==f_stacks[numid].count) {
	fprintf(stderr, "%s: stack %d empty ?\n", __func__, numid);
	return -11;
	}

for (idx=0; idx<f_stacks[numid].count; idx++) {

	eff = f_stacks[numid].slots[idx].numero;
	fv  = f_stacks[numid].slots[idx].fval;

	if (verbosity > 1)
		fprintf(stderr, "stack %d  idx %d : effect %2d on %p\n",
				numid, idx, eff, target);

	foo = crapulator(target, eff, fv);

	if (foo) {
		fprintf(stderr,
			"crapulator give me error %d on effect %d (%s)\n",
						foo, 		eff,
				crap_name_from_number(eff));
		return foo;
		}
	}

return 0;
}
/* -------------------------------------------------------------- */
int load_stack_from_file(int numid, char *fname, int notused)
{
FILE		*fp;
// int		a, b;
// float		f;
// char		line[100];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, fname, notused);
#endif

if (numid < 0 || numid > NUMBER_OF_STACK) {
	fprintf(stderr, "%s: slot number %d is invalid\n", __func__, numid);
	exit(1);
	}

if (NULL==(fp=fopen(fname, "r"))) {
	perror(fname);
	return -1;
	}

/*
 *		here was dragons
 */
		/* hadoc parser ? */

fclose(fp);

return -1;
}
/* -------------------------------------------------------------- */
int parse_filter_chain(int numid, char *argument)
{
char		*cptr, *tmparg;
int		value, foo;

if (numid < 0 || numid > NUMBER_OF_STACK) {
	fprintf(stderr, "%s: slot number %d is invalid\n", __func__, numid);
	exit(1);
	}

#if DEBUG_LEVEL
fprintf(stderr, "\n%s: arg = '%s'\n", __func__, argument);
#endif

foo = filterstack_init(numid, 8);
if (foo) {
	fprintf(stderr, "%s: filterstack init --> %d\n", __func__, foo);
	return foo;
	}

/*  BUG ?
  If the 'argument' string is coming from a default value (as defined
  here in main), strtok make a nice segfault. so I make a copy of that
  string...
*/
tmparg = alloca(strlen(argument) + 1);
if (NULL==tmparg) {
	fprintf(stderr, "memory panic in %s:%s\n", __FILE__, __func__);
	exit(1);
	}
strcpy(tmparg, argument);

for (;;) {
	cptr = strtok(tmparg, ":");
	// fprintf(stderr, "cptr %p\n", cptr);
	if (NULL==cptr)	break;
	tmparg = NULL;		/* for the next pass of strtok */
	// fprintf(stderr, "  parsing '%s'\n", cptr);

	if (isalpha(*cptr)) {
		value = crap_number_from_name(cptr);
		// fprintf(stderr, "%s: '%s' -> %d\n", __func__,
		//			cptr, value);
		if (value < 0) {
			fprintf(stderr, "%s: '%s' not found\n",
				__func__, cptr);
			return -1;
			}
		foo = filterstack_add(numid, value, 1, 1.0);
		continue;
		}

	if ('@' == cptr[0]) {
		fprintf(stderr, "%s: got indirect '%s'\n", __func__,
							cptr+1);
		continue;
		}

	if (1 == sscanf(cptr, "%d", &value)) {
		foo = filterstack_add(numid, value, 1, 1.0);
		if (foo) {
			fprintf(stderr, "%s: err %d add\n",
					__func__, foo);
			}
		continue;
		}
	}

if (verbosity > 1) filterstack_list(numid, __func__);
return 0;
}
/* ----------------------------------------------------------- */


