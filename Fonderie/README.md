# Fonderie et Interpolator

Avec toutes ces fonctions disponibles et `grabvidseq`, nous
savons faire des images **floues**. L'étape suivante, les plus
pervers d'entre vous le savent déja, est celle de la création
de **films flous** dans le domaine spatio-temporel. 

À l'heure actuelle, il y a plusieurs programmes distincts. Le premier
(fonderie) fait une moyenne mobile sur N images consécutives,
le second (interpolator) fait un fondu-enchainé de N pas
entre deux images consécutives,
et le troisième (singlepass) fait d'autres trucs, comme
répéter N fois un image pour avoir une vidéo sacadée.

Mais avant et après un de ces deux traitements, il y a des chaines
de filtres...

## Chaine de filtres

Ce système connait un certain nombre de filtres et d'effets spéciaux
destinés à augmenter la kitchitude du produit final. Ils peuvent être chainés
les uns après les autres, à l'entrée et à la sortie du process
de floutagement.

Ces filtres ont chacun un nom, utilisable dans une chaine de filtres. 
L'option `-L` de ces logiciels permet d'obtenir la liste des filtres.
Pour ne rien filtrer, utilisez le filtre `none`, il est là pour ça.

Une chaine de filtres est constituée d'une liste de nom
de filtre, séparés par le caractère `:`, une façon de faire très
classique dans notre univers, en fait.

`mirsplit:ctr2x2:killlines`

Nous allons donc voir quelques exemples un peu plus loin.

## Fonderie

Le programme principal, utilisé à partir de la ligne de commande
avec une foule d'options aux mnémoniques abscons et à la syntaxe
perverse.

Rassurez-vous, en général il est wrappable dans des scripts
écrits en Bash. Il est même possible un jour qu'ils puissent lire des 
paramètres dans `$(env)`.

```
./fonderie, compiled Dec 30 2020, 14:09:18, pid 5013
*** FloatImg library, alpha v116 (Dec 27 2020, 22:39:28)
        FONDERIE
options:
        -E      input:filter:chain
        -F      output:filter:chain
        -g      convert to gray
        -I      input glob pattern
        -L      list available filters
        -O      output directory
        -T      fifo size
        -v      increase verbosity
```

## exemple d'utilisation

Voici comment appeler cette machinerie depuis la ligne de commande
tel qu'il m'arrive de le pratiquer :

```
#!/bin/bash

GRABDIR="/spool/tth/fonderie"
FONDEUR="$HOME/Devel/FloatImg/Fonderie/fonderie"
GLOB=${GRABDIR}'/?????.fimg'

${FONDEUR} -I "$GLOB" -E cos01:trinitron -F classtrial -T 30
```

Par défaut le répertoire où sont exportées les images est nommé
`p8/` et il doit être là, c'est donc à vous de le créer.
Votre machine va maintenant mouliner avec entrain et persévérance,
puis
ensuite il vous suffira d'encoder toutes les images générées dans
`p8/` (répertoire de sortie par défaut)
avec une incantation de ffmpeg :

```
ffmpeg  -nostdin                                        \
        -loglevel error                                 \
        -y -r 30 -f image2 -i p8/%05d.png               \
        -c:v libx264 -pix_fmt yuv420p                   \
        foo.mp4
```

## crapulator.c 

C'est dans ce module qu'est codé le moteur de filtrage, utilisé
aussi bien en entrée qu'en sortie. Il est, à l'heure actuelle,
assez rudimentaire, avec un paramétrage simpliste, et un manque
criant de documentation...

Dans la même équipe, vous pouvez aussi aller contempler `glitches.c`
pour voir le genre de traitement que l'on fait subir à nos pixels
flottants.

## Interpolator

Un logiciel dont l'inspiration vient du passé et les améliorations
d'une résidence à Terre-Blanque, ça ne peut pas être complètement
malsain.

```
*** interpolator.c : compiled by tTh, Jan 12 2021 16:18:58
*** FloatImg library, alpha v116 (Dec 27 2020, 22:39:28)
        INTERPOLATOR
usage:
        interpolator [options] <inglob> <outdir> <nbsteep>
options:
        -S nn           mysterious sort
        -E foo:bla:kr   input  filter chain
        -F name:just    output filter chain
        -L              list available filters
        -v              increase verbosity
```
## Singlepass

Le monde à l'envers : pas de traitement inter-frames, mais par contre,
on peut facilement tester une chaine de filtres sur une image unique.

```
*** singlepass.c : compiled Mar 17 2021 11:21:45
*** FloatImg library, alpha 122 (Mar 16 2021, 18:44:00)
------ Single pass serial filter ------
usage:
        -F      define:the:filter:chain
        -g      input glob pattern
        -L      list available filters
        -r N    repetiiing factor
        -O      /output/directory
        -v      spit more messages
```

## Conclusion

Tout cela est bien rigolo :)


**Use the source, Luke**

