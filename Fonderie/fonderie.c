/*
 *		FONDERIE
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <glob.h>
#include  "../floatimg.h"

#include  "utilfuncs.h"
#include  "fifo.h"
#include  "glitches.h"
#include  "crapulator.h"
#include  "filterstack.h"

int			verbosity;

/* -------------------------------------------------------------- */
/*
 *	this is the real worker ? or just a wrapper ?
 */
int traite_une_image(FloatImg *image, char *outd)
{
static int		numero;
int			foo;
char			ligne[200];

#if DEBUG_LEVEL
fprintf(stderr, "\n>>> %s ( %p '%s' )\n", __func__, image, outd);
#endif

/* here, we put the picz in the fifo machinery */
foo = insert_picture(image);
if (foo) {
	fprintf(stderr, "%s: err %d on insert\n", __func__, foo);
	return foo;
	}

/* and now, we pull the result on the magic computation
 */
sprintf(ligne, "%s/%05d.png", outd, numero);
if (verbosity > 1) fprintf(stderr, "    exporting to '%s'\n", ligne);

foo = export_fifo(ligne, 1);
if (foo) {
	fprintf(stderr, "%s: err %d on export\n", __func__, foo);
	return foo;
	}
numero++;			/* VERY IMPORTANT :) */

return 0;
}
/* -------------------------------------------------------------- */
int insert_blank(FloatImg *image, int nbre, char *outd)
{
int		idx, foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %d  '%s' )\n", __func__,
			image, nbre, outd);
#endif

fimg_clear(image);
for (idx=0; idx<nbre; idx++) {
	if ((foo=traite_une_image(image, outd))) {
		fprintf(stderr, "%s : err %d from 'traite_une_image'\n",
				__func__, foo);
		break;
		}
	printf("\t%c\r", "ABCDEFGH"[idx%8]);  fflush(stdout);
	}
puts("");

return 0;
}
/* -------------------------------------------------------------- */

int demarre_la_machine(char *pattern, char *outdir, int szfifo,
					int outfmt, int blk)
{
int			foo, idx, width, height;
glob_t			globbuf;
char			*cptr;
FloatImg		input;
double			fin;
float			maxvalue;
int			datas[3];

#if DEBUG_LEVEL
fprintf(stderr, "\n>>> %s ( '%s' -> '%s'  %d )\n", __func__,
			pattern, outdir, szfifo);
#endif

(void)fimg_timer_set(0);

memset(&globbuf, 0, sizeof(glob_t));
foo = glob(pattern, 0, NULL, &globbuf);
if (foo) {
	fprintf(stderr, "glob (%s) failure %d\n", pattern, foo);
	exit(1);
	}

fprintf(stderr, "glob '%s' -> %d, %d files found\n", pattern, foo,
					(int)globbuf.gl_pathc);

/*	get the size of the inputs images */
foo = fimg_fileinfos(globbuf.gl_pathv[0], datas);
width = datas[0];		height = datas[1];
if (verbosity) fprintf(stderr, "first image size %dx%d\n", width, height);

fimg_create(&input, width, height, 3);

/* get the maximum value of the first pic */
foo = fimg_load_from_dump(globbuf.gl_pathv[0], &input);
if (foo) {
	fprintf(stderr, "%s: err %d loading %s\n",
				__func__, foo, globbuf.gl_pathv[0]);
	exit(1);
	}
maxvalue = fimg_get_maxvalue(&input);
if (verbosity) fprintf(stderr, "first image maxvalue %f\n", maxvalue);

foo = create_fifo(szfifo, width, height, FIMG_TYPE_RGB);
if (verbosity) fprintf(stderr, "init fifo (%d slots) = %d\n", szfifo, foo);

/* XXX inject a few strange pics in the fifo */
insert_blank(&input, blk, outdir);

for (idx=0; idx<globbuf.gl_pathc; idx++) {

	cptr = globbuf.gl_pathv[idx];
	/* first step : read the current grabed picz from disk,
	   and put it in our private buffer */
	// fprintf(stderr, "\n######### loading '%s'\n", cptr);
	foo = fimg_load_from_dump(cptr, &input);
	if (foo) {
		fprintf(stderr, "load #%d from dump -> %d\n", idx, foo);
		continue;
		}

	/*		fscking input filter here 		*/
	foo = filterstack_run(0, &input, 0);
	if (foo) {
		fprintf(stderr, "%s: input filter -> %d\n", __func__, foo);
		exit(1);
		}
#if 0
	if (idx==42) fimg_dump_to_file(&input, "inputXXX.fimg", 0);
#endif
	foo = traite_une_image(&input, outdir);

	if (foo) {
		fprintf(stderr, "traitement %s -> %d WTF?\n", cptr, foo);
		break;
		}
	fprintf(stderr, "\t%5d / %5d\r", idx, (int)globbuf.gl_pathc);
	}

fputs("\n", stderr);

insert_blank(&input, blk, outdir);

/*
 *		PLEASE, FLUSH THE FIFO !
 */

fin = fimg_timer_get(0);
if (idx) {
	fprintf(stderr, "\nelapsed %.2f seconds, %.2f s/pic\n", fin, fin/idx);
	}
else	{
	fprintf(stderr, "\nelapsed %.2f seconds\n", fin);
	}

return 8;		/* why 9 ? */
}
/* -------------------------------------------------------------- */
void help(void)
{
puts("\tFONDERIE\noptions:");

puts("\t-E\tinput:filter:chain");
puts("\t-F\toutput:filter:chain");
// puts("\t-g\tconvert to gray");
puts("\t-I\tinput glob pattern");
puts("\t-L\tlist available filters");
puts("\t-O\toutput directory (default p8/)");
puts("\t-T\tfifo size");
puts("\t-v\tincrease verbosity");

if (verbosity) { puts(""); fimg_print_version(2); puts(""); }

exit(0);
}
/* -------------------------------------------------------------- */
int main (int argc, char *argv[])
{
int		foo, opt;
int		fifosize = 10;
char		*in_pattern = "capture/?????.fimg";
char		*out_dir = "p8";
int		outfmt = FILE_TYPE_PNG;
int		blanks = 10;
char		*InFchain  = "none";
char		*OutFchain = "none";

fprintf(stderr, "*** %s\n\tcompiled %s, %s, pid %d\n",
			argv[0], __DATE__, __TIME__, getpid());

while ((opt = getopt(argc, argv, "B:E:F:ghI:LO:T:vw:x:")) != -1) {
	switch(opt) {
		case 'E':	InFchain = optarg;		break;
		case 'F':	OutFchain = optarg;		break;
		case 'B':	blanks = atoi(optarg);
				break;
		case 'g':	// convert_to_gray = 1;
				break;
		case 'h':	help();	
				break;
		case 'I':	in_pattern = optarg;
				break;
		case 'L':
			list_crapulors("available filters");
			exit(0);
		case 'O':	out_dir = optarg;
				break;
		case 'T':	fifosize = atoi(optarg);
				break;
		case 'v':	verbosity++;
				break;
		default:	exit(1);
		}
	}

if (verbosity) {
	fprintf(stderr, "\tinput glob        '%s'\n", in_pattern);
	fprintf(stderr, "\toutput dir        '%s'\n", out_dir);
	fprintf(stderr, "\tsrc filter        '%s'\n", InFchain);
	fprintf(stderr, "\tout filter        '%s'\n", OutFchain);
	fprintf(stderr, "\tfifo size         %d\n",   fifosize);
	}
/*
 *	check for the export directory
 */
foo = check_if_export_dir_is_valid(out_dir, 0);
if ( !foo ) {
	fprintf(stderr, "** export dir (%s) not valid\n", out_dir);
	fprintf(stderr, "** may be you have to create it ?\n");
	exit(1);
	}

foo = parse_filter_chain(0, InFchain);
if (foo) {
	fprintf(stderr, "err %d parsing '%s'\n", foo, InFchain);
	exit(1);
	}
foo = parse_filter_chain(1, OutFchain);
if (foo) {
	fprintf(stderr, "err %d parsing '%s'\n", foo, OutFchain);
	exit(1);
	}

if (verbosity > 1) {
	filterstack_list(0, "input");
	filterstack_list(1, "ouput");
	fprintf(stderr, ".\n");
	}

foo = demarre_la_machine(in_pattern, out_dir, fifosize, outfmt, blanks);

fprintf(stderr, "retour du bigrun de la machine -> %d\n", foo);

return 0;
}
/* -------------------------------------------------------------- */
