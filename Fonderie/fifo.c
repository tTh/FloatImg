/*
 *	Fonctions de la Fonderie du Cumul
 *	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *	Du code bien cracra / tTh / Tetalab
 */
#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <stdlib.h>
#include  <malloc.h>

#include  "../floatimg.h"

#include  "fifo.h"
#include  "crapulator.h"
#include  "filterstack.h"

/* -------------------------------------------------------------- */
/*	global vars from main
 */
extern	int		verbosity;

/*	private vars of this module - it was very dirty,
 *	but simple and efficient.
 */
static	A_Fifo		g_fifo;

/* -------------------------------------------------------------- */
static inline int big_adder(FloatImg *from, FloatImg *to)
{
int		size, idx;

size = from->width * from->height;

for (idx=0; idx<size; idx++)		to->R[idx] += from->R[idx];
for (idx=0; idx<size; idx++)		to->G[idx] += from->G[idx];
for (idx=0; idx<size; idx++)		to->B[idx] += from->B[idx];

return 0;
}
/* -------------------------------------------------------------- */
/*
 *	97% of the total run time was in that function \o_
 */
int faire_la_somme(A_Fifo *pfifo, FloatImg *destination, int step)
{
int		idx, foo;
FloatImg	*pdest;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__,
				pfifo, destination, step);
#endif

if (step<1){
	fprintf(stderr, "***** %s invalid step %d\n", __func__, step);
	exit(1);
	}

if (NULL==destination) {
	pdest = &(pfifo->total);		}
else	{
	pdest = destination;			}

fimg_clear(pdest);

for (idx=0; idx<pfifo->nbslots; idx += step) {

	foo = big_adder(&(pfifo->slots[idx]), pdest);
	if (foo)
		{
		fprintf(stderr, "%s: err %d on add_2\n", __func__, foo);
		abort();
		}
	}

#if 0
fprintf(stderr, "*** %s:%s writing debugB file ***\n",
			__FILE__, __func__);
fimg_dump_to_file(&g_fifo.total, "debugB.fimg", 0);
#endif

return 0;
}
/* -------------------------------------------------------------- */
/*			called by 'fonderie.c'
 */
int export_fifo(char *fname, int notused)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, fname, notused);
#endif

foo = faire_la_somme(&g_fifo, NULL, 1);

foo = filterstack_run(1, &g_fifo.total, 0);
if (foo) {
	fprintf(stderr, "%s: ERR post process picture -> %d\n",
				__func__, foo);
	return foo;
	}

#if 0
	fimg_dump_to_file(&g_fifo.total, "outputXXX.fimg", 0);
#endif

foo = fimg_export_picture(&g_fifo.total, fname, 0);
if (foo) {
	fprintf(stderr, "ERR EXPORT '%s' is %d\n", fname, foo);
	exit(3);
	}

return 0;
}
/* -------------------------------------------------------------- */
int insert_picture(FloatImg *src)
{
FloatImg	*dst;
int		nbre;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, src);
#endif

dst = &g_fifo.slots[g_fifo.next];

nbre = dst->width * dst->height * sizeof(float);
memcpy(dst->R, src->R, nbre);
memcpy(dst->G, src->G, nbre);
memcpy(dst->B, src->B, nbre);

/* XXX
fprintf(stderr, "*** %s:%s writing debugA file ***\n",
			__FILE__, __func__);
foo = fimg_dump_to_file(dst, "debugA.fimg", 0);
fprintf(stderr, "    ok file dumped %d\n", foo);
XXX */

g_fifo.next++;  g_fifo.next %= g_fifo.nbslots;

if (verbosity > 2) fprintf(stderr, "%s : next slot %d\n",
				__func__, g_fifo.next);

return 0;
}
/* -------------------------------------------------------------- */
/*
 *	this function must have a lot of new  parameters,
 *	-- filetype of exported pictures...
 */

int create_fifo(int nbslot, int w, int h, int imgtype)
{
int		foo, idx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d  %dx%d  %d )\n", __func__,
			nbslot, w, h, imgtype);
#endif

memset(&g_fifo, 0, sizeof(A_Fifo));

g_fifo.nbslots = nbslot;

g_fifo.slots = calloc(nbslot, sizeof(FloatImg));
if (NULL==g_fifo.slots)	abort();
for (idx=0; idx<nbslot; idx++) {
	foo = fimg_create(&g_fifo.slots[idx], w, h, imgtype);
	if (foo) {
		fprintf(stderr, "%s idx %d err%d\n", __func__, idx, foo);
		return foo;
		}
	fimg_clear(&g_fifo.slots[idx]);
	}
foo = fimg_create(&g_fifo.total, w, h, imgtype);
g_fifo.next = 0;
g_fifo.magic = MAGIC_FIFO;

return 0;
}
/* -------------------------------------------------------------- */
/*
 *	omfg ! I've write a setter !
 */
int set_fifo_output_format(int filetype)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, filetype);
#endif

g_fifo.export_type = filetype;

return -1;
}

