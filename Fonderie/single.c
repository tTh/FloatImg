/*
		SINGLE PICZ PROCESSOR

	experimental and/or testing code, do not use in
	production.

*/

#include   <stdio.h>
#include   <string.h>
#include   <sys/stat.h>
#include   <stdint.h>
#include  <sys/time.h>

#include   "../floatimg.h"

#include   "sfx.h"
#include   "filterstack.h"
#include   "crapulator.h"
#include   "single.h"

/* -------------------------------------------------------------- */
/*
 *		singleton/private variables
 */
static int		nextpng, counter;
static char		*destination;
static int		chainfilter;
// static int		outtype;

/*		and the classic global var			*/
extern int		verbosity;

/* -------------------------------------------------------------- */
int single_init(int next, char *dest, int fxchain, int outfmt)
{
int			foo;
struct stat		stbuf;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d '%s' %d %d )\n", __func__,
			next, dest, fxchain, outfmt);
#endif

nextpng = next;
chainfilter = fxchain;

foo = stat(dest, &stbuf);
if (foo) {
	perror("stat dest dir");
	return -2;
	}
if (S_IFDIR != (stbuf.st_mode & S_IFMT)) {
	fprintf(stderr, "%s: %s must be a directory\n", __func__, dest);
	return -3;
	}

destination = strdup(dest);		/* have a static copy */

return 0;
}
/* -------------------------------------------------------------- */
int single_push_picture(FloatImg *pimg)
{
int		foo;
char		line[1000], buff[100];
char		*extension = "png";

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, pimg);
#endif

strncpy(line, destination, 100);
if ('/' != line[strlen(line)-1]) {
	// fprintf(stderr, "adding '/'\n");
	strcat(line, "/");
	}

sprintf(buff, "%05d.%s", nextpng, extension);
strcat(line, buff);

// fprintf(stderr, "writing %p to '%s'\n", pimg, line);
foo = fimg_export_picture(pimg, line, 0);
if (foo) {
	fprintf(stderr, "%s: err %d on export\n", __func__, foo);
	return foo;
	}

nextpng++;		counter++;

return 0;
}
/* -------------------------------------------------------------- */
int single_print_state(char *title, int k)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, title, k);
#endif

fprintf(stderr, "%s : %s\n", __FILE__, title);
fprintf(stderr, "   nextpng          %d\n", nextpng);
fprintf(stderr, "   counter          %d\n", counter);
fprintf(stderr, "   chainfilter      %d\n", chainfilter);
fprintf(stderr, "   destination      %s\n", destination);

if (k) {
		/* XXX */
	}

return -1;
}
/* -------------------------------------------------------------- */
