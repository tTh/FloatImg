/*
 *		utilfuncs.c
 *
 * new Sat Nov  9 22:15:15 UTC 2024
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <sys/types.h>
#include  <sys/stat.h>

#include  "utilfuncs.h"

extern int verbosity;

/* -------------------------------------------------------------- */
int check_if_export_dir_is_valid(char *path, int action)
{
int		foo;
struct stat	statbuf;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s', %d )\n", __func__, path, action);
#endif

foo = stat(path, &statbuf);
if (foo) {
	if (verbosity) perror(path);
	return 0;
	}

#if DEBUG_LEVEL
fprintf(stderr, "    mode 0x%x\n", statbuf.st_mode);
#endif
if ((statbuf.st_mode & S_IFMT) != S_IFDIR) {
	if (verbosity) fprintf(stderr, "%s: Not a directory\n", path);
	return 0;
	}

/* OK, c'est un repertoire, mais peut-on écrire dedans ? */
foo = access(path, W_OK);
#if DEBUG_LEVEL
fprintf(stderr, "    access '%s' = %d\n", path, foo);
#endif
if (0 != foo) {
	if (verbosity) fprintf(stderr, "%s: can't write\n", path);
	return 0;
	}

return 1;		/* export dir is OK */
}
/* -------------------------------------------------------------- */

