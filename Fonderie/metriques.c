/*
 * 		metriques.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>

#include  "../floatimg.h"
#include  "metriques.h"

extern int		verbosity;

/*   --------------------------------------------------------------   */
/*   usage --> sfx.c:trinitron					*/
int stat_zone(FloatImg *pimg, int geom[4], float v3[3])
{
int		x, y, xe, ye;
int		off;
double		accus[3], divisor;

#if DEBUG_LEVEL
fprintf(stdout, "geom: %5d %5d %5d %5d\n", 
			geom[0], geom[1], geom[2], geom[3]);
#endif

xe = geom[0] + geom[2];
ye = geom[1] + geom[3];

accus[0] = accus[1] = accus[2] = 0.0;
for (y=geom[1]; y<ye; y++) {
	for (x=geom[0]; x<xe; x++) {
		off = (y*pimg->width) + x;
		accus[0] += (double) pimg->R[off];
		accus[1] += (double) pimg->G[off];
		accus[2] += (double) pimg->B[off];
		}
	}

divisor = (double)(geom[2] * geom[3]);		/* array of zone */
v3[0] = (float)(accus[0] / divisor);
v3[1] = (float)(accus[1] / divisor);
v3[2] = (float)(accus[2] / divisor);

return 0;
}
/*   --------------------------------------------------------------   */
/*
 *	premier essai : moyenne de toutes les composantes
 *			de tous les pixels.
 *
 *	Question: pourquoi pas le retour en double precision ?
 */
int get_float_metric_avg(FloatImg *pimg, float *where)
{
float		means[4];	/* four values : R G B A */
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, pimg, where);
#endif

foo = fimg_meanvalues(pimg, means);
if (foo) {
	fprintf(stderr, "fatal error in %s\n", __func__);
	exit(1);
	}
*where = means[0] + means[1] + means[2];
return 0;
}
/*   --------------------------------------------------------------   */
/*		echantillonage des pixels rouges		*/
int get_float_metric_iRed(FloatImg *pimg, float *where)
{
int	idx, size, nbre;
double	adder;

adder = 0.0;
nbre = 0;
size = pimg->width * pimg->height;
for (idx=20; idx < size; idx+=42) {
	adder += (double)pimg->R[idx];
	nbre++;
	}
*where = (float)(adder/(nbre+1));
return 0;
}
/*   --------------------------------------------------------------   */
/*
 *		LR mean left/right
 */
int get_float_metric_LR(FloatImg *pimg, float *where)
{
int	coords[4], foo;
float	valL[3], valR[3];

coords[0] = 0;				//  X
coords[1] = 0;				//  Y
coords[2] = pimg->width / 2;		//  W
coords[3] = pimg->height;		//  H

foo = stat_zone(pimg, coords, valL);
if (foo) {
	fprintf(stderr, "err %d stat zone in %s\n", foo, __func__);
	return foo;
	}
coords[1] = pimg->width / 2;
foo = stat_zone(pimg, coords, valR);
if (foo) {
	fprintf(stderr, "err %d stat zone in %s\n", foo, __func__);
	return foo;
	}

*where = valL[1] - valR[1];

return 0;
}
/*   --------------------------------------------------------------   */
/*
 *		et voici le grand dispactheur
 */
int get_float_metric_from_file(char *fname, float *where, int mode)
{
FloatImg	image;
int		foo;
float		fval;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p %d )\n", __func__, fname, where, mode);
#endif

foo = fimg_create_from_dump(fname, &image);
if (foo) {
	fprintf(stderr, "%s: err %d loading %s\n", __func__, foo, fname);
	return foo;
	}
fval = -1.0;				/* sensible default value */
switch (mode) {
	case 0: case 1:
		foo = get_float_metric_avg(&image, &fval);
		break;
	case 2:
		foo = get_float_metric_iRed(&image, &fval);
		break;
	case 3:
		foo = get_float_metric_LR(&image, &fval);
		break;
	default:
		fprintf(stderr, "%s: method %d invalid\n", 
				__func__, mode);
		exit(1);
		break;		/* not reached */
	}

*where = fval;

fimg_destroy(&image);

return 0;
}
/*   --------------------------------------------------------------   */
