#!/usr/bin/awk -f

#
#		this utility script make a file who
#		is includet by 'crapulator.h'
#

BEGIN	{
	print "// -------------------------------------"
	print "// generated file, do not edit by hand !"
	print "// -------------------------------------"
	}

#		$1 is the badly brain-designed numeric id
#		$2 is the user name of the filter

	{
	name = sprintf("CR_%s", $2)
	printf "#define    %-15s    (%d)\n", name, $1
	}

END	{
	print "\n// generated file, do not edit by hand !"
	}
