/*
 *		crapulator.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <string.h>
#include  <unistd.h>

#include  "../floatimg.h"

#include  "fifo.h"
#include  "crapulator.h"
#include  "glitches.h"
#include  "sfx.h"

extern int		verbosity;

/* -------------------------------------------------------------- */
static int trier_les_pixels(FloatImg *pimg)
{
FloatImg	copy;
int		foo;

fimg_clone(pimg, &copy, 1);

foo = fimg_qsort_rgb_a(pimg, &copy, 0);
if (foo) {
	fprintf(stderr, "%s: err qsort rgb = %d\n", __func__, foo);
	return foo;
	}

incrustation_vignette(pimg, &copy, 0);

fimg_copy_data(&copy, pimg);

fimg_destroy(&copy);

return 0;
}
/* -------------------------------------------------------------- */
static int effect_3(FloatImg *image)
{
float		value;
int		foo;

value = fimg_get_maxvalue(image);
fimg_mul_cste(image, -1.0);
fimg_add_cste(image, value);
foo = fimg_count_negativ(image);
if (foo) {
	fimg_dump_to_file(image, "err.fimg", 0);
	fprintf(stderr, "%s negativ %d\n",
				__func__, foo);
	return -78;
	}
return 0;
}
/* -------------------------------------------------------------- */
static int insitu_ctr2x2(FloatImg *pimg)
{
FloatImg		img;
int 			retval;

fimg_clone(pimg, &img, 0);
retval = fimg_contour_2x2(pimg, &img, 0);
if (retval) {
	fprintf(stderr, "%s : err contour %d\n",
			__func__, retval);
	exit(1);
	}
fimg_copy_data(&img, pimg);
fimg_destroy(&img);

return 0;
}
/* -------------------------------------------------------------- */
static int insitu_filtre3x3(FloatImg *pimg, int typef)
{
FloatImg		img;
int 			foo, retval;
FimgFilter3x3		*pfiltre;

static FimgFilter3x3	lowpass = {
		{
		1.0,	2.0,	1.0,
		2.0,	4.0,	2.0,
		1.0,	2.0,	1.0,
		},
	16.0,	0.0
	};

static FimgFilter3x3	hipass = {
	/* there was a bug with this filter */
		{
		-1.0,	-1.0,	-1.0,
		-1.0,	 9.0,	-1.0,
		-1.0,	-1.0,	-1.0,
		},
	1.0,	0.0
	};

static FimgFilter3x3	diagonal = {
	/* there was a bug with this filter */
		{
		4.0,	1.0,	0.0,
		1.0,	0.0,	-1.0,
		0.0,	-1.0,	-4.0,
		},
	1.0,	0.0
	};

switch (typef) {
	case 0:		pfiltre = &lowpass;		break;
	case 1:		pfiltre = &hipass;		break;
	case 2:		pfiltre = &diagonal;		break;
	default:
		fprintf(stderr, "%s: bad filter number %d\n",
				__func__, typef);
		return -6;
		break;
	}

fimg_clone(pimg, &img, 0);

retval = fimg_filter_3x3(pimg, &img, pfiltre);
if (retval) {
	fprintf(stderr, "%s error %d on filter\n", __func__, retval);
	exit(1);
	}
(void)fimg_killborders(&img);			/* nice try ? */

foo = fimg_auto_shift_to_zero(&img, &img);
if (foo) {
	fprintf(stderr, "%s: err %d zero shift\n", __func__, foo);
	exit(1);
	}

/**	may be, we can check for negative values ? */
/**	or is this useless because whe have shifted to zero ? */
if (verbosity > 1) {
	foo = fimg_count_negativ(&img);
	if (foo) {
		fprintf(stderr, "%s -> %d / %d negative pixels\n", __func__,
					foo, img.width*img.height);
		}
	}

fimg_killborders(&img);
fimg_copy_data(&img, pimg);
fimg_destroy(&img);

return retval;
}
/* -------------------------------------------------------------- */
int run_killrgb_0(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;

// fprintf(stderr, "----> %s\n", __func__);

memset(&tmp, 0, sizeof(FloatImg));

fimg_clone(img, &tmp, 0);
ret = fimg_killrgb_v(img, &tmp, k);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

// fprintf(stderr, "%s ---->\n", __func__);

return ret;
}
/* -------------------------------------------------------------- */
static int run_pixelize_0(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;

memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret = fimg_pixelize_h_0(img, &tmp, k);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
static int run_pixelize_random(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;

memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret = fimg_pixelize_h_rnd(img, &tmp, k);
if (verbosity > 1) fprintf(stderr, "in %s, pixelize H rnd -> %d\n", 
					__func__, ret);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
/* added Mon Apr  1 15:56:54 UTC 2024 */
static int run_triptyq(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;
memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret = fimg_make_triptyq(img, &tmp, k);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
static int run_decomprgbz_color(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;

memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret = fimg_decomp_rgbz_color(img, &tmp, k);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
/* nouveau 14 mai 2022 */
static int run_gr2popcol(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;
memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret =  graylevel2popcolors(img, &tmp, k);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
/*	new Sun Feb 12 11:30:02 CET 2023			  */
static int run_rndfluffy(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;
memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret = fimg_make_rndfluffy_lines(img, &tmp, k);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
static int run_decomprgbz_gray(FloatImg *img, int k)
{
FloatImg		tmp;
int			ret;

/* ugly code here */
memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret = fimg_decomp_rgbz_gray(img, &tmp, k);
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
/* new Sun Jul 16 11:27:43 UTC 2023 */
int run_suprbg(FloatImg *img, int notused)
{
FloatImg		tmp;
int			ret;

if (notused) {
	fprintf(stderr, "FATAL: invalid notused in %s\n", __func__);
	abort();
	}

/* ugly code here */
memset(&tmp, 0, sizeof(FloatImg));
fimg_clone(img, &tmp, 0);
ret = poke_sup_rb_g(img, &tmp);
if (ret) { 
	fprintf(stderr, "%s: ret = %d\n", __func__, ret);
	}
fimg_copy_data(&tmp, img);
fimg_destroy(&tmp);

return ret;
}
/* -------------------------------------------------------------- */
/*
 *	This is the main filter engine used both for input and
 *	output processing. It can be called by the filterstack
 *	processor.
 */

#define DEBUG_THIS_CRAP		0

int crapulator(FloatImg *image, int idFx, float fval)
{
int			retval;
// FloatImg		imgtmp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %f )\n", __func__,
				image, idFx, fval);
#endif

#if DEBUG_THIS_CRAP
static int		count = 0;
int			flag_debug = 0;
if (DEBUG_THIS_CRAP==count) {
	flag_debug = 1;
	fprintf(stderr, "DEBUG PT 1 in %s:%d\n", __func__, __LINE__);
	fimg_save_as_png(image, "crap_before.png", 0);
	}
#endif

retval = 0;

switch (idFx) {
	case CR_none:			/* DO NOTHING */
		retval = 0;
		break;

	case CR_cos01:
		fimg_cos_01(image, image,
				fimg_get_maxvalue(image));
		break;
	case CR_cos010:
		fimg_cos_010(image, image,
				fimg_get_maxvalue(image));
		break;
	case CR_fx3:
		retval = effect_3(image);
		break;
	case CR_rnd48a:
		brotche_rand48_a(image, 0.20,
				fimg_get_maxvalue(image));
		break;
	case CR_rnd48b:
		brotche_rand48_b(image, 0.10,
				fimg_get_maxvalue(image)*0.8);
		break;
	case CR_killcola:
		retval = fimg_killcolors_a(image, 0.0);
		break;
	case CR_colmixa:
		retval = fimg_colors_mixer_a(image, 2.0);
		break;
	case CR_ctr2x2:
		retval = insitu_ctr2x2(image);
		break;
	case CR_classtrial:
		retval = fimg_classif_trial(image, image, 0.37, 0);
		break;
	case CR_binarize:
		retval = fimg_binarize(image, 0);
		break;
	case CR_trinarize:
		retval = fimg_trinarize(image, 0);
		break;
	case CR_liss2x2:
		retval = fimg_lissage_2x2(image);
		// (void)fimg_killborders(image);
		break;
	case CR_liss3x3:
				/* smooth filter */
		retval = insitu_filtre3x3(image, 0);
		break;
	case CR_desaturate:
		retval = fimg_desaturate(image, image, 0);
		break;
	case CR_killlines:
		retval = kill_a_few_lines(image, fval,
						image->height/19);
		break;
	case CR_water:
		retval = bouger_les_pixels(image, 12);
		break;
	case CR_mirsplit:
		retval = mirror_split(image, 0);
		break;
	case CR_updown:
		retval = upside_down(image);
		break;
	case CR_hipass:
				/* hipass filter */
		retval = insitu_filtre3x3(image, 1);
		break;
	case CR_diagonal:
		retval = insitu_filtre3x3(image, 2);
		break;
	case CR_octotree:
		retval = octotree_classif(image, 0.500, 0);
		break;

	case CR_trinitron:
		retval = trinitron(image, 0);
		break;

	case CR_sqrt:
		retval = fimg_square_root(image, image, 1000.0);
		break;
	case CR_pow2:
		retval = fimg_power_2(image, image, 1000.0);
		break;

	case CR_triplemul:
		retval = fimg_sfx_triplemul(image, image, 0);
		break;

	/* here are the glitches */
	case CR_bsombra:			/* experiment ! */
		retval = des_bords_sombres_a(image, 160);
		break;
	case CR_bsombrb:			/* experiment ! */
		retval = des_bords_sombres_b(image, 120);
		break;

	case CR_vsglitch:
		/* please make this function more tweakable */
		retval = vertical_singlitch(image, 290+rand()%45,
						fval, 0.19, 0);
		break;

	case CR_crumphard:	/* new june 1st 2021, in the bibcave */
		retval = fimg_crump_hard(image, image, fval, 0);
		break;

	case CR_rndblks:
		retval = random_blocks(image, 70);
		break;

	case CR_shiftln0:
		retval = multilines_shift_0(image, 11, 120);
		break;

	case CR_qsortrgb:
		retval = trier_les_pixels(image);
		break;

	case CR_multidots:
		retval = plot_multidots(image, 42);
		break;

	case CR_nothing:
		retval = do_something(image, 3);
		break;

	case CR_killrgb0:
		retval = run_killrgb_0(image, 0);
		break;

	case CR_pixelizh:
		retval = run_pixelize_0(image, 8);
		break;
	case CR_pixelizv:
		retval = run_pixelize_0(image, 32);
		break;

	case CR_pixelrand:
		retval = run_pixelize_random(image, 16);
		break;

	case CR_splitlevel:
		retval = fimg_split_level(image, image, 0);
		break;

	case CR_decrgbzc:
		retval = run_decomprgbz_color(image, 0);
		break;
	case CR_decrgbzg:
		retval = run_decomprgbz_gray(image, 0);
		break;

	case CR_hilightr:
		retval = fimg_highlight_color(image, image, 'R', 1.717);
		break;

	case CR_gr2popcol:
		retval = run_gr2popcol(image, -1);
		break;

	case CR_fluffy:
		retval = run_rndfluffy(image, 75);
		break;

	case CR_suprbg:
		retval = run_suprbg(image, 0);
		break;

	case CR_thermocol:
		/* new Wed Mar 20 19:42:42 UTC 2024 */
		retval = fimg_auto_thermique(image, image, 0);
		break;

	case CR_triptyq:
		/* added  Mon Apr  1 15:52:32 UTC 2024*/
		retval = run_triptyq(image, 0);
		break;

	case CR_togray:
		retval = fimg_to_gray(image);
		break;

	case CR_shift0:
		/* added Sun Sep 29 16:55:54 UTC 2024 */
		retval = fimg_auto_shift_to_zero(image, image);
		// fprintf(stderr, "******** Shift Zero -> %d\n", retval);
		break;

	default :
		fprintf(stderr, "%s : effect #%d invalid\n", 
				__func__, idFx);
		return -77;
	}

#if DEBUG_THIS_CRAP
if (flag_debug) {
	fprintf(stderr, "DEBUG PT 2 in %s:%d\n", __func__, __LINE__);
	fimg_save_as_png(image, "crap_after.png", 0);
	flag_debug = 0;
	}
count++;
#endif

return retval;
}
/* -------------------------------------------------------------- */
typedef struct {
	int		id;
	char		*name;
	int		ipar;
	float		fpar;
	int		flags;
	} Crapulor;

/*	Warning: overengeniring inside			*/
#include  "crapstr.h"		    /* generated file ! */
#define NBCRAP   (sizeof(CrapL)/sizeof(Crapulor))

void list_crapulors(char *texte)
{
int		idx;

#define OUT	stdout

if (verbosity) {
	fprintf(stderr, "We have around %ld filters now !\n", NBCRAP);
	}

if (NULL!=texte && verbosity)
	fprintf(OUT, " _________ %s _________\n", texte);

for (idx=0; CrapL[idx].id!=-1; idx++) {
	if (verbosity) {
		fprintf(OUT, " %-12s | %4d  |  %5d  |  %8.3f\n",
					CrapL[idx].name,
					CrapL[idx].id,
					CrapL[idx].ipar,
					CrapL[idx].fpar);
		}
	else	{
		fprintf(OUT, "%s\n", CrapL[idx].name);
		}
	}
#undef OUT
}
/* -------------------------------------------------------------- */
char * crap_name_from_number(int num)
{
int		idx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, num);
#endif

for (idx=0; CrapL[idx].id!=-1; idx++) {
	if (num == CrapL[idx].id) {
		return CrapL[idx].name;
		}
	}

return "???";
}
/* -------------------------------------------------------------- */
int crap_number_from_name(char *name)
{
int		idx, retval;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, name);
#endif

retval = -1;		/* not found */
for (idx=0; CrapL[idx].id!=-1; idx++) {
	if (0 == strcmp(CrapL[idx].name, name)) {
		// fprintf(stderr, "found '%s' -> %d\n", name, 
		//				CrapL[idx].id);
		retval = CrapL[idx].id;
		break;
		}
	}

return retval;		/* not found */
}
/* -------------------------------------------------------------- */

