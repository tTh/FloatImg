/*
 *		+-------------------------+
 *		|   S I N G L E P A S S   |
 *		+-------------------------+
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <string.h>
#include  <glob.h>

#include  "../floatimg.h"

#include  "utilfuncs.h"
#include  "crapulator.h"
#include  "filterstack.h"
#include  "single.h"
#include  "glitches.h"

/* ----------------------------------------------------------- */

#define   FILTERS	0

int		verbosity;

/* ----------------------------------------------------------- */
/*
 *	parameter 'duplic' is the repetition factor
 */
int run_the_singlepass(char *globber, char *destdir, int duplic,
					int fchain, int outfmt)
{
FloatImg	image = { 0 };
int		idx, foo, loop;
glob_t		globbuf;
char		*fname;
double		elapsed;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s'  %d  %d %d )\n", __func__,
				globber, destdir, duplic, fchain, outfmt);
#endif

if (verbosity) filterstack_list(fchain, "Run the single pass");

(void)fimg_timer_set(0);

foo = single_init(0, destdir, fchain, outfmt);
if (foo) {
	fprintf(stderr, "erreur %d single_init\n", foo);
	return foo;
	}
// single_print_state("just after init", 0);

memset(&globbuf, 0, sizeof(glob_t));
foo = glob(globber, 0, NULL, &globbuf);
// fprintf(stderr, "globbing '%s' -> %d, %d files found\n",
//			globber, foo, (int)globbuf.gl_pathc);
switch (foo) {
	case GLOB_NOSPACE:
		fprintf(stderr, "%s: glob run out of memory\n", __func__);
		break;
	case GLOB_ABORTED:
		fprintf(stderr, "%s: glob read error\n", __func__);
		break;
	case GLOB_NOMATCH:
		fprintf(stderr, "%s: glob found no matches\n", __func__);
		break;
	}

if (0 == globbuf.gl_pathc) {
	fprintf(stderr, "%s: no file found, aborting\n", __func__);
	return -1;
	}

for (idx=0; idx<globbuf.gl_pathc; idx++) {

	fname = globbuf.gl_pathv[idx];		/* alias of filename */

	if (0==image.width && 0==image.height) {
		foo = fimg_create_from_dump(fname, &image);
		}
	else	{
		foo = fimg_load_from_dump(fname, &image);
		}
	if (verbosity > 1) {
		fprintf(stderr, "%s: image '%s' loaded in %p\n",
					__func__, fname, &image);
		}
	if (foo) {
		fprintf(stderr, "get image -> %d\n", foo);
		return -1;
		}

	if (0==idx && verbosity) {
		fprintf(stderr, "\tfirst image size %dx%d\n",
					image.width, image.height);
		}

	// fprintf(stderr, "  %6ld  %s\r", (long)globbuf.gl_pathc-idx, fname);

	foo = filterstack_run(fchain, &image, 0);
	if (foo) {
		fprintf(stderr, "%s: filterstack run --> %d\n",
				__func__,                 foo);
		return foo;
		}

	/* HERE WE CAN REPEAT THE INSERT OF THE PICZ */
	for (loop=0; loop<duplic; loop++) {
		foo = single_push_picture(&image);
		if (foo) {
			fprintf(stderr, "error %d on push_picture\n", foo);
			return foo;
			}
		}
	}

fprintf(stderr, "\n");

globfree(&globbuf);

fimg_destroy(&image);
// single_print_state("end of run", 0);

elapsed = fimg_timer_get(0);
fprintf(stderr, "%s: %ld frames, elapsed %.3f s, %.3f fps\n",
				__func__,
				(long)globbuf.gl_pathc, elapsed,
				(double)globbuf.gl_pathc/elapsed);

return 0;
}
/* ----------------------------------------------------------- */
static void help(void)
{
puts("------ Single pass serial filter ------\nusage:");

puts("\t-F\tdefine:the:filter:chain");
puts("\t-g\tinput glob pattern");
puts("\t-L\tlist available filters");
puts("\t-O\t/output/directory   (default ./p8)");
puts("\t-r N\trepetiiing factor");
// puts("\t-s\tdo single test");
puts("\t-v\tspit more messages");

exit(0);
}
/* ----------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo, opt;
char		*filterchain =   "none";
char		*globbing    =   "./capture/?????.fimg";
char		*outdir      =   "./p8";
// char		*outtype     =   ".png";
int		do_xper = 0;
int		repeat = 1;

fprintf(stderr, "*** %s\n\tcompiled %s %s\n", argv[0],
                        __DATE__, __TIME__);
if (verbosity) fimg_print_version(2);

if (argc < 2) {
	fprintf(stderr, "\t/!\\ %s is option driven\n", argv[0]);
	help();
	}

while ((opt = getopt(argc, argv, "hF:g:LO:r:svx")) != -1) {
	switch (opt) {
		case 'h':	help();			break;

		case 'F':	filterchain = optarg;	break;
		case 'g':	globbing = optarg;	break;

		case 'L':
			list_crapulors("available filters");
			exit(0);

		case 'O':	outdir = optarg;	break;

		case 'r':	repeat = atoi(optarg);	break;

		case 'v':	verbosity++;		break;

		case 'x':	do_xper = 1;		break;

		default:
			fprintf(stderr, "%s ABEND\n", argv[0]);
			exit(1);
		}
	}

if (repeat < 1) {
	fprintf(stderr, "%s: loop %d invalid\n", argv[0], repeat);
	exit(2);
	}

foo = check_if_export_dir_is_valid(outdir, 0);
if ( !foo ) {
	fprintf(stderr, "** export dir (%s) not valid\n", outdir);
	fprintf(stderr, "** may be you have to create it ?\n");
	exit(1);
	}

foo = parse_filter_chain(FILTERS, filterchain);
if (foo) {
	fprintf(stderr, "err %d in parse_filter_chain\n", foo);
	exit(1);
	}

if (verbosity) {
	fprintf(stderr, "\tpid               %d\n", getpid());
	fprintf(stderr, "\tinput glob        %s\n", globbing);
	fprintf(stderr, "\tfilter chain      %s\n", filterchain);
	fprintf(stderr, "\toutput dir        %s\n", outdir);
	fprintf(stderr, "\trepeat            %d\n", repeat);
	// fprintf(stderr, "\toutput type       %s\n", outtype);
	fprintf(stderr, "\tdo xper           %d\n", do_xper);
	}

/*
 *		REAL action here
 */
foo = run_the_singlepass(globbing, outdir, repeat,
					FILTERS, FILE_TYPE_PNG);
fprintf(stderr, "\tRun the single pass --> %d\n", foo);

return 0;
}

/* ----------------------------------------------------------- */
