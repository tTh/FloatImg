/*
 * 		test des trucs
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <string.h>
#include  <glob.h>

#include  "../floatimg.h"
#include  "glitches.h"
#include  "sfx.h"
#include  "filterstack.h"
#include  "crapulator.h"
#include  "single.h"

#include  "utilfuncs.h"

/* ----------------------------------------------------------- */

int		verbosity;

#define		PNG	"out.png"
#define		W	800
#define		H	600
#define		LMAX	255.0
#define		TIMER	1

#define		STK	6

/* ----------------------------------------------------------- */
/* new Sat Nov  9 22:17:46 UTC 2024 */
int essai_test_export_dir(char *footxt)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, footxt);
#endif

foo = check_if_export_dir_is_valid("/tmp/quux", 0);
fprintf(stderr, "    got %d\n\n", foo);

foo = check_if_export_dir_is_valid("/home", 0);
fprintf(stderr, "    got %d\n\n", foo);

foo = check_if_export_dir_is_valid("./", 0);
fprintf(stderr, "    got %d\n\n", foo);

foo = check_if_export_dir_is_valid("Makefile", 0);
fprintf(stderr, "    got %d\n\n", foo);

return -1;
}
/* ----------------------------------------------------------- */

int essai_filterstack(char *fIname, char *fOname)
{
int		foo;
FloatImg	image;
double		debut, fin;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s' )\n", __func__, fIname, fOname);
#endif


// filterstack_list(STK, __func__);

foo = fimg_create_from_dump(fIname, &image);
if (foo) {
	fprintf(stderr, "%s: err %d create from dump\n", __func__, foo);
	exit(1);
	}

srand(getpid()); srand48(getpid());

debut = fimg_timer_set(TIMER);

foo = filterstack_run(STK, &image, 0);
if (foo) {
	fprintf(stderr, "filterstack run --> %d\n", foo);
	return foo;
	}

fin = fimg_timer_set(TIMER);

foo = fimg_export_picture(&image, fOname, 0);
if (foo) {
	fprintf(stderr, "erreur export %d\n", foo);
	}

fprintf(stderr, "elapsed %f\n", fin-debut);

fimg_destroy(&image);

return 0;
}
/* ----------------------------------------------------------- */
/*
 *		test-only function !

foo = essayer_single("capture/???42.fimg", "/tmp/x8/", STK);
fprintf(stderr, "essayer single -> %d\n", foo);
 */
int essayer_single(char *globpattern, char *destdir, int chain)
{
FloatImg	image = { 0 };
int		idx, foo;
glob_t		globbuf;
char		*fname;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s' %d )\n", __func__,
			globpattern, destdir, chain);
#endif

filterstack_list(chain, "essai du single");

foo = single_init(0, destdir, chain, -1);
if (foo) {
	fprintf(stderr, "erreur %d single_init\n", foo);
	return foo;
	}
single_print_state("just after init", 0);

memset(&globbuf, 0, sizeof(glob_t));
foo = glob(globpattern, 0, NULL, &globbuf);
fprintf(stderr, "globbing '%s' -> %d, %d files found\n",
			globpattern, foo, (int)globbuf.gl_pathc);
if (0 == globbuf.gl_pathc) {
	fprintf(stderr, "%s : no file found, aborting\n", __func__);
	return -1;
	}

for (idx=0; idx<globbuf.gl_pathc; idx++) {

	fname = globbuf.gl_pathv[idx];		/* alias of filename */
	fprintf(stderr, "%s  %6d  %s\r", __func__, idx, fname);

	if (0==image.width && 0==image.height) {
		foo = fimg_create_from_dump(fname, &image);
		}
	else	{
		foo = fimg_load_from_dump(fname, &image);
		}
	if (foo) {
		fprintf(stderr, "get image -> %d\n", foo);
		return -1;
		}

	foo = filterstack_run(chain, &image, 0);
	if (foo) {
		fprintf(stderr, "%s: filterstack run --> %d\n",
				__func__,                 foo);
		return foo;
		}

	foo = single_push_picture(&image);
	if (foo) {
		fprintf(stderr, "erreur %d push picture\n", foo);
		return foo;
		}
	}

fprintf(stderr, "\n");

single_print_state("end of run :)", 0);

fimg_destroy(&image);

return 0;
}
/* ----------------------------------------------------------- */

int help(void)
{
puts("Yolo!");

puts("\t-F\tdefine:the:filter:chain");
puts("\t-g\tinput glob pattern");
puts("\t-i\tinfile.fimg");
puts("\t-L\tlist available filters");
puts("\t-o\toutfile.xxx");
puts("\t-O\t/output/directory");
puts("\t-s\tdo single test");

exit(0);
}
/* ----------------------------------------------------------- */
int experiment(char *fname)
{
int		foo;
FloatImg	image, dest;

fprintf(stderr, "----- EXPERIMENT on '%s' -----\n", fname);

foo = fimg_create_from_dump(fname, &image);
if (foo) {
	fprintf(stderr, "%s: err %d on create_from_dump\n",
			__func__, foo);
	return -1;
	}

foo = fimg_clone(&image, &dest, 0);

foo = poke_sup_rb_g(&image, &dest);
fprintf(stderr, "poke sup rb g --> %d\n", foo);

fimg_export_picture(&dest, "foo.png", 0);

exit(0);		/* back to real world */
}
/* ----------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo, opt;
int		do_xper = 0;
int		do_single = 0;
char		*filterchain = "0";
char		*infile = "mire.fimg";
char		*outfile = PNG;
char		*outdir = "/tmp/x8/";
char		*globstr = "capture/????7.fimg";

fprintf(stderr, "*** %s : compiled by tTh, %s %s\n", __FILE__,
			__DATE__, __TIME__);
fimg_print_version(2);

while ((opt = getopt(argc, argv, "hF:g:i:Lo:O:svx")) != -1) {
	switch(opt) {
		case 'h':	help();			break;
		case 'F':	filterchain = optarg;	break;
		case 'g':	globstr = optarg;	break;
		case 'i':	infile = optarg;	break;
		case 'L':
			list_crapulors("available filters");
			exit(0);
		case 'o':	outfile = optarg;	break;
		case 'O':	outdir  = optarg;	break;
		case 's':	do_single = 1;		break;
		case 'v':	verbosity++;		break;
		case 'x':	do_xper = 1;		break;
		default:	exit(1);
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s : argc=%d, optind=%d\n", argv[0], argc, optind);
#endif

foo = essai_test_export_dir("bla bla");
if (foo) {
	fprintf(stderr, "err %d in essai_test_export_dir\n", foo);
	exit(1);
	}
exit(0);

foo = parse_filter_chain(STK, filterchain);
if (foo) {
	fprintf(stderr, "err %d in parse_filter_chain\n", foo);
	exit(1);
	}


if (do_xper) {
	experiment(infile);
	return 0;
	}
if (do_single) {
	fprintf(stderr, "Globbing '%s'\n", globstr);
	essayer_single(globstr, outdir, STK);
	return 0;
	}

foo = essai_filterstack(infile, outfile);
if (foo) {
	fprintf(stderr, "err %d in essai_filterstack\n", foo);
	exit(1);
	}

return 0;
}

/* ----------------------------------------------------------- */
