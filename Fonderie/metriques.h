/*
 * 		metriques.h
 */

/*   --------------------------------------------------------------   */

int stat_zone(FloatImg *pimg, int geom[4], float v3[3]);


/*		first experiments */

int get_float_metric_a(FloatImg *pimg, float *where);
int get_float_metric_from_file(char *imgname, float *where, int mode);

/*   --------------------------------------------------------------   */
