/*
 *			floatimg.h

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                    Version 2, December 2004

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net>

 Everyone is permitted to copy and distribute verbatim or modified
 copies of this license document, and changing it is allowed as long
 as the name is changed.

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

  0. You just DO WHAT THE FUCK YOU WANT TO.

 *			ugly code from tTh
 *	http://la.buvette.org/photos/cumul/
 *	https://git.tetalab.org/tTh/FloatImg
 */

#define		FIMG_VERSION	(245)
#define		RELEASE_NAME	("noname")
#define		PATCH_LEVEL	("aaaa")


/* XXX add a test for stdint.h / uint32_t XXX */
#include  <stdint.h>


/*
 *		new 11 mars 2022, and a lot of iterations
 *		around the concept of metadata for my work.
 *
 *	we MUST look at packing and endianess problems NOW
 *	and we can think about a fixed size of this datablock
 */
#define MAGIC_MDATA	0xfe007007
typedef struct {
	uint32_t	magic;
	uint32_t	padding;

	struct timeval	timestamp;	// #include  <stdlib.h>
	uint64_t	cpid;		// process id of the creator
	int32_t		count;
	float		fval;
	char		idcam[32];
	int32_t		origin;			// enum ?
	uint32_t	reserved[8];
	} FimgMetaData;


/*
 *	in memory descriptor of a floating image
 *	----------------------------------------
 */
#define MAGIC_FIMG	0x00F11F00
typedef struct {
	uint32_t	magic;
	int		width;
	int		height;
	int		type;
	float		fval;
	int		count;
	float		*R, *G, *B, *A;
	FimgMetaData	mdatas;			// added 20230912
	int		reserved;
	} FloatImg;

/*
 *	fimg file header (short version)
 */
typedef struct {
	char		magic[8];	/* this is not an asciiz ! */
	int32_t		w, h, t;
	/*
	 *  what about the packing ?
	 */
	} FimgFileHead;


#define MAGIC_AREA51	0xA5EA0051
typedef struct {
	uint32_t	magic;
	int		w, h;
	int		x, y;
	int		flags;
	} FimgArea51;

#define FIMG_TYPE_GRAY			1
#define FIMG_TYPE_RGB			3
#define FIMG_TYPE_RGBA			4
#define FIMG_TYPE_RGBZ			99

#define FILE_TYPE_FIMG			1
#define FILE_TYPE_PNM			2
#define FILE_TYPE_PNG			3
#define FILE_TYPE_TGA			4
#define FILE_TYPE_TIFF			5
#define FILE_TYPE_FITS			6
#define FILE_TYPE_BMP			7
#define FILE_TYPE_EXR			8
#define FILE_TYPE_DICOM			9
#define FILE_TYPE_PGM			10

/*	lib/contrast.c			*/
#define CONTRAST_NONE			0
#define CONTRAST_SQRT			1
#define CONTRAST_POW2			2
#define CONTRAST_COS01			3
#define CONTRAST_COS010			4
#define CONTRAST_XPER			5		/* use with caution */
/*
 *	core module
 */
int   fimg_create(FloatImg *fimg, int w, int h, int t);
int   fimg_destroy(FloatImg *fimg);
int   fimg_clone(FloatImg *fimg, FloatImg *newpic, int flags);
int   fimg_copy_data(FloatImg *from, FloatImg *to);
int   fimg_type_is_valid(int type);


int   fimg_print_version(int k);
void  fimg_print_sizeof(void);
void  fimg_printhead(FloatImg *h);
void  fimg_printdims(char *txt, FloatImg *pi);
int   fimg_describe(FloatImg *head, char *txt);
char *fimg_str_type(int type);
int   fimg_plot_rgb (FloatImg *head, int x, int y,
					float r, float g, float b);
int   fimg_get_rgb(FloatImg *head, int x, int y, float *rgb);
int   fimg_put_rgb(FloatImg *head, int x, int y, float *rgb);
int   fimg_clear(FloatImg *fimg);
int   fimg_add_rgb(FloatImg *head, int x, int y, float r, float g, float b);
int   fimg_rgb_constant(FloatImg *head, float r, float g, float b);

/*	--> lib/fimg-compare.c		*/
int fimg_images_not_compatible(FloatImg *a, FloatImg *b);

int fimg_interpolate(FloatImg *s1, FloatImg *s2, FloatImg *d, float coef);

/*	'operats' module 		*/
int fimg_add_3(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_add_2(FloatImg *a, FloatImg *b);			/* B+=A */
int fimg_sub_3(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_mul_3(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_minimum(FloatImg *a, FloatImg *b, FloatImg *d);
int fimg_maximum(FloatImg *a, FloatImg *b, FloatImg *d);


/* ---------------------------------------------------- */
/*	funcs/filtrage.c		*/
typedef struct {
	float		matrix[9];
	float		mult;
	float		offset;
	} FimgFilter3x3;
/*
 *	this module contains bugs...
 */
int fimg_killborders(FloatImg *img);
int fimg_lissage_2x2(FloatImg *img);
int fimg_lissage_3x3(FloatImg *img);
int fimg_show_filter(char *title, FimgFilter3x3 *filtr);
int fimg_filter_3x3(FloatImg *s, FloatImg *d, FimgFilter3x3 *filtr);

int fimg_contour_2x2(FloatImg *psrc, FloatImg *pdst, int reverse);

/* ---------------------------------------------------- */

/*	 module sfx0.c			*/
int fimg_killcolors_a(FloatImg *fimg, float fval);
int fimg_killcolors_b(FloatImg *fimg, float fval);
int fimg_colors_mixer_a(FloatImg *fimg, float fval);

/*	 module sfx1.c			*/
int fimg_highlight_color(FloatImg *src, FloatImg *dst,
					char color, float fval);

/*	 module sfx2.c			*/
int fimg_binarize(FloatImg *pimg, int notused);
int fimg_trinarize(FloatImg *pimg, int notused);

/*	 module sfx3.c			*/
int fimg_make_rndfluffy_lines(FloatImg *src, FloatImg *dst, int rndt);
int fimg_crump_hard(FloatImg *src, FloatImg *dst, float kval, int notused);

/*	 module sfx4.c			*/
int fimg_sfx_triplemul(FloatImg *s, FloatImg *d, int notused);
int fimg_split_level(FloatImg *src, FloatImg *dst, int notused);
int fimg_make_triptyq(FloatImg *src, FloatImg *dst, int notused);

/*	funcs/rotate.c module		*/
int fimg_rotate_90(FloatImg *src, FloatImg *dst, int notused);

int fimg_killrgb_v(FloatImg *src, FloatImg *dst, int k);
int fimg_decomp_rgbz_color(FloatImg *psrc, FloatImg *pdst, int k);
int fimg_decomp_rgbz_gray(FloatImg *psrc, FloatImg *pdst, int k);

int fimg_save_plane_as_dicom(FloatImg *src, char *outname,
						char plane, int flags);

/* universal exporter XXX */
int fimg_export_picture(FloatImg *pic, char *fname, int flags);

/*	PNM files module		*/
int fimg_save_as_pnm(FloatImg *head, char *fname, int flags);
int fimg_save_as_pgm(FloatImg *head, char *fname, int flags);
int fimg_load_from_pnm(char *fname, FloatImg *head, int notused);
int fimg_save_plane_as_pgm(FloatImg *psrc, char *fname, char plane);

double fimg_timer_set(int whot);
double fimg_timer_get(int whot);

/*	--> lib/contrast.c		*/
int fimg_id_contraste(char *name);
int fimg_square_root(FloatImg *s, FloatImg *d, double maxval);
int fimg_power_2(FloatImg *s, FloatImg *d, double maxval);
int fimg_cos_01(FloatImg *s, FloatImg *d, double maxval);
int fimg_cos_010(FloatImg *s, FloatImg *d, double maxval);
int fimg_mix_rgb_gray(FloatImg *img, float mix);

/*	funcs/saturation.c		*/
int     fimg_shift_to_zero(FloatImg *s, FloatImg *d, float coefs[6]);
int     fimg_auto_shift_to_zero(FloatImg *s, FloatImg *d);

/*	funcs/falsecolors.c		*/
int fimg_falsecolors_0(FloatImg *src, FloatImg *dst, int k, float valf);

/*	funcs/fmorpho.c			*/
int fimg_filtre_morpho_0(FloatImg *sfimg, FloatImg *dfimg, int index);

int fimg_auto_thermique(FloatImg *src, FloatImg *dst, int k);

/*	--> funcs/plasmas.c		*/
int fimg_prototype_plasma(FloatImg *img, double time, int type);

/*	--> funcs/pixelize.c
			voir fimg_pixelize_h_rnd() */
int fimg_pixelize_h_0(FloatImg *psrc, FloatImg *pdst, int k);
int fimg_pixelize_h_rnd(FloatImg *psrc, FloatImg *pdst, int largeur);

/* * * * experimental ! */
int fimg_classif_trial(FloatImg *src, FloatImg*dst, float fval, int notused);
int fimg_qsort_rgb_a(FloatImg *psrc, FloatImg *pdst, int notused);
int fimg_qsort_rgb_b(FloatImg *psrc, FloatImg *pdst, int notused);

/*	module funcs/equalize.c		*/
int fimg_equalize_compute(FloatImg *src, void *vptr, float vmax);
int fimg_equalize(FloatImg *src, double vmax);

int fimg_mk_gray_from(FloatImg *src, FloatImg*dst, int k);
int fimg_desaturate(FloatImg *src, FloatImg *dst, int notused);

/*	module funcs/geometry.c		*/
		/* warning, this module is a mess */
int fimg_halfsize_0(FloatImg *src, FloatImg *dst, int notused);
int fimg_halfsize_1(FloatImg *src, FloatImg *dst, int notused);
int fimg_extractor(FloatImg *in, FloatImg *out, FimgArea51 *rect);
int fimg_mirror(FloatImg *src, FloatImg *dst, int notused);

int fimg_incrustator_0(FloatImg *psrc, FloatImg *pdst,
				int xpos, int ypos, int flags);

int fimg_displacement_0(FloatImg *psrc, FloatImg *pdst, int flags);

/*	module funcs/rampes.c		*/
int fimg_hdeg_a(FloatImg *img, double dcoef);
int fimg_vdeg_a(FloatImg *img, double dcoef);
int fimg_do_stripes(FloatImg *img, float fmax, int mode);

/*	FIMG native file module		*/
int   fimg_fileinfos(char *fname, int *datas);
int   fimg_dump_to_file(FloatImg *head, char *fname, int notused);
int fimg_dumpmd_to_file(FloatImg *fi, char *nm, FimgMetaData *pmd, int nu);

int   fimg_load_from_dump(char *fname, FloatImg *where);
int   fimg_create_from_dump(char *fname, FloatImg *head);

/*	FIMG metadata module		*/
int fimg_show_metadata(FimgMetaData *pmd, char *title, int notused);
int fimg_default_metadata(FimgMetaData *pmd, int bla);
int fimg_get_metadata_from_file(char *fname, FimgMetaData *pmd);

/*
 *	FITS, an image fileformat for astronomy
 */
int fimg_save_R_as_fits(FloatImg *src, char *outname, int flags);
int fimg_save_G_as_fits(FloatImg *src, char *outname, int flags);
int fimg_save_B_as_fits(FloatImg *src, char *outname, int flags);
int fimg_save_plane_as_fits(FloatImg *src, char *oname, char plane, int flags);

int fimg_write_as_tiff(FloatImg *src, char *fname, int flags);
int fimg_save_as_exr(FloatImg *src, char *outname, int flags);

/*	mathematics operations 				*/
float fimg_get_plane_maxvalue(FloatImg *psrc, char plane);
float fimg_get_maxvalue(FloatImg *head);
int   fimg_get_minmax_rgb(FloatImg *head, float mmvals[6]);
int   fimg_meanvalues(FloatImg *head, float means[4]);
int   fimg_to_gray(FloatImg *head);
int   fimg_add_cste(FloatImg *fi, float value);
int   fimg_mul_cste(FloatImg *fi, float value);
int   fimg_div_cste(FloatImg *fi, float value);
int   fimg_ajust_from_grab(FloatImg *fi, double maxima, int notused);
int   fimg_absolute(FloatImg *fimg);
void  fimg_drand48(FloatImg *fi, float kmul);
long   fimg_count_negativ(FloatImg *fi);
long   fimg_clamp_negativ(FloatImg *fi);

int fimg_max_of_max(FloatImg *img, float maxes[3]);

/*	various funcs modules		*/
int   fimg_load_from_png(char *filename, FloatImg *fimg);
int   fimg_create_from_png(char *filename, FloatImg *fimg);
int   fimg_save_as_png(FloatImg *src, char *outname, int flags);

int   fimg_save_as_bmp(FloatImg *src, char *outname, int flags);

int   fimg_test_pattern(FloatImg *fimg, int type, double dval);
int   fimg_draw_something(FloatImg *fimg);
int   fimg_mircol_1(FloatImg *dst, float mval);
int   fimg_multirandom(FloatImg *fimg, long nbpass);

/*	file is 'funcs/utils.c'		*/
void fimg_print_minmax(float minmax[6], char *titre);
float *charplane2int(char plane, FloatImg *img);

int parse_WxH(char *str, int *pw, int *ph);
int parse_double(char *str, double *dptr);
int irand2(int offset, int modulo);
int print_rectangle(char *str, FimgArea51 *rect);
int parse_rectangle(char *str, FimgArea51 *r, int notused);
int format_from_extension(char *fname);
char * extension_from_format(int fmt);

int fimg_clear_rectangle(FloatImg *pimg, int rect[4]);



