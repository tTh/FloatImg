/*
 *		qsort_rgb.c
 */

#include   <stdio.h>
#include   <stdlib.h>
#include   <stdint.h>
#include   <string.h>
#include   <math.h>

#include   "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/* nouveau 7 octobre 2020, juste avant sonoptic de la pluie craignos	 */

static int compare_a(const void *p1, const void *p2)
{
return		( *(float *)p1 < *(float *)p2 );
}

int fimg_qsort_rgb_a(FloatImg *psrc, FloatImg *pdst, int notused)
{
int		foo, szimg;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, psrc, pdst, notused);
#endif

if (notused) { fprintf(stderr, "%s notused != 0, WTF ?\n", __func__); }

if (FIMG_TYPE_RGB != psrc->type) {
	fprintf(stderr, "%s: bad src type %d\n", __func__, psrc->type);
	return -7;
	}
if (fimg_images_not_compatible(psrc, pdst)) {
	fprintf(stderr, "%s: bad dst type %d\n", __func__, pdst->type);
	return -8;
	}

if (psrc != pdst) {		/* optimize or futurbug ? */
	foo = fimg_copy_data(psrc, pdst);
	if (foo) {
		fprintf(stderr, "%s: err %d on copy data\n", __func__, foo);
		return foo;
		}
	}

szimg = pdst->width * pdst->height;
// fprintf(stderr, "%s : %d pixels\n", __func__, szimg);

qsort(pdst->R, szimg, sizeof(float), compare_a);
qsort(pdst->G, szimg, sizeof(float), compare_a);
qsort(pdst->B, szimg, sizeof(float), compare_a);

return 0;
}
/* --------------------------------------------------------------------- */
typedef struct {
	float	sum;
	float	r, g, b;
	} pix;

static int compare_b(const void *p1, const void *p2)
{
pix	*s1, *s2;
s1 = (pix *)p1;
s2 = (pix *)p2;
return		( s1->sum < s2->sum );
}

int fimg_qsort_rgb_b(FloatImg *psrc, FloatImg *pdst, int notused)
{
int		x, y, szimg;
pix		*ppix, *ptr;
float		rgb[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, psrc, pdst, notused);
#endif

if (notused) { fprintf(stderr, "%s notused != 0, WTF ?\n", __func__); }

if (FIMG_TYPE_RGB != psrc->type) {
	fprintf(stderr, "%s: bad src type %d\n", __func__, psrc->type);
	return -7;
	}
if (fimg_images_not_compatible(psrc, pdst)) {
	fprintf(stderr, "%s: bad dst type %d\n", __func__, pdst->type);
	return -8;
	}

szimg = pdst->width * pdst->height;
// fprintf(stderr, "%s : %d pixels\n", __func__, szimg);

ppix = calloc(szimg, sizeof(pix));

ptr = ppix;				/* mobile pointer */
for (y=0; y<psrc->height; y++) {
	for (x=0; x<psrc->width; x++) {

		fimg_get_rgb(psrc, x, y, rgb);

		ptr->sum = rgb[0] + rgb[1] + rgb[2];
		ptr->r = rgb[0];
		ptr->g = rgb[1];
		ptr->b = rgb[2];

		ptr++;			/* next pixel */
		}
	}

qsort(ppix, szimg, sizeof(pix), compare_b);

ptr = ppix;				/* mobile pointer */
for (y=0; y<psrc->height; y++) {
	for (x=0; x<psrc->width; x++) {

		rgb[0] = ptr->r;
		rgb[1] = ptr->g;
		rgb[2] = ptr->b;
		fimg_put_rgb(pdst, x, y, rgb);

		ptr++;			/* next pixel */
		}
	}

free(ppix);

return 0;
}
/* --------------------------------------------------------------------- */
