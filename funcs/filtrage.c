/*
 *		Floating filters are all WIP !
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <string.h>
#include  <fcntl.h>
#include  <float.h>

#include  "../floatimg.h"

extern int		verbosity;

/* -------------------------------------------------------------------- */
int fimg_show_filter(char *title, FimgFilter3x3 *filtr)
{
float		*M;			/* alias of filter matrix */
int		idx;
float		sum, value;

if (title)	fprintf(stderr, "--------- %s ---------\n", title);

M = filtr->matrix;			/* aliasing here */

fprintf(stderr, "%8.3f   %8.3f   %8.3f\n", M[0], M[1], M[2]);
fprintf(stderr, "%8.3f   %8.3f   %8.3f\n", M[3], M[4], M[5]);
fprintf(stderr, "%8.3f   %8.3f   %8.3f\n", M[6], M[7], M[8]);

sum = 0.0;
for (idx=0; idx<9; idx++)	sum += M[idx];
fprintf(stderr, "        sum:  %8.3f\n", sum);
fprintf(stderr, "       mult:  %8.3f\n", filtr->mult);
fprintf(stderr, "     offset:  %8.3f\n", filtr->offset);

value = (sum * filtr->mult) + filtr->offset;
fprintf(stderr, "      value:  %8.3f\n", value);

return 0;
}
/* -------------------------------------------------------------------- */
int fimg_filter_3x3(FloatImg *src, FloatImg *dst, FimgFilter3x3 *filtr)
{
int		x, y, w, h, of;
float		*pr, *pg, *pb;		/* alias for src pix filds */
float		*M;			/* alias of filter matrix */
double		dval;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p   %p )\n", __func__, src, dst, filtr);
#endif

if (src->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s: src type %d invalid\n", __func__, src->type);
	return -99;
	}
if (dst->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s: dst type %d invalid\n", __func__, dst->type);
	return -99;
	}
if (fimg_images_not_compatible(src, dst)) {
	fprintf(stderr, "%s: src & dst not compatibles\n", __func__);
	return -98;
	}

if (verbosity > 1) {
	fimg_show_filter((char *)__func__, filtr);
	}

/*	aliasing some vars for cleaner code */
pr = src->R;	pg = src->G;	pb = src->B;
w = src->width;	h = src->height;
M = filtr->matrix;

for (y=1; y < h-1; y++) {
	for (x=1; x < w-1; x++) {
		of = x + (y * w);

	dval =	M[0] * pr[of-(w+1)]	+
		M[1] * pr[of-w]		+
		M[2] * pr[of-(w-1)]	+
		M[3] * pr[of-1]		+
		M[4] * pr[of]		+
		M[5] * pr[of+1]		+
		M[6] * pr[of+(w+1)]	+
		M[7] * pr[of+w]		+
		M[8] * pr[of+(w-1)]	;

	dst->R[of] = dval + filtr->offset;

	dval =	M[0] * pg[of-(w+1)]	+
		M[1] * pg[of-w]		+
		M[2] * pg[of-(w-1)]	+
		M[3] * pg[of-1]		+
		M[4] * pg[of]		+
		M[5] * pg[of+1]		+
		M[6] * pg[of+(w+1)]	+
		M[7] * pg[of+w]		+
		M[8] * pg[of+(w-1)]	;

	dst->G[of] = dval + filtr->offset;

	dval =	M[0] * pb[of-(w+1)]	+
		M[1] * pb[of-w]		+
		M[2] * pb[of-(w-1)]	+
		M[3] * pb[of-1]		+
		M[4] * pb[of]		+
		M[5] * pb[of+1]		+
		M[6] * pb[of+(w+1)]	+
		M[7] * pb[of+w]		+
		M[8] * pb[of+(w-1)]	;

	dst->B[of] = dval + filtr->offset;
	}
}

return 0;
}
/* -------------------------------------------------------------------- */
/*
 *	this is the more shifting hack on the block.
 */
static int fimg_lissage_2x2_a(FloatImg *img)
{
int		x, y, offset;
float		cr, cg, cb;
float		*pr, *pg, *pb;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, img);
fprintf(stderr,"     type %d size %dx%d\n", img->type,
					img->width, img->height);
#endif

if (img->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s : type %d invalide\n", __func__, img->type);
	return -99;
	}

pr = img->R;	pg = img->G;	pb = img->B;

for (y=1; y < img->height-1; y++) {
	for (x=1; x < img->width-1; x++) {
		offset = x + (y * img->width);

	cr =	pr[offset]		+	pr[offset+1]	+
		pr[offset+img->width]	+	pr[offset+img->width+1];

	cg =	pg[offset]		+	pg[offset+1]	+
		pg[offset+img->width]	+	pg[offset+img->width+1];

	cb =	pb[offset]		+	pb[offset+1]	+
		pb[offset+img->width]	+	pb[offset+img->width+1];

	pr[offset] = cr / 4.0;
	pg[offset] = cg / 4.0;
	pb[offset] = cb / 4.0;
	}
}

return 0;
}
/* -------------------------------------------------------------------- */
int fimg_killborders(FloatImg *img)
{
int		idx, h, w, o;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, img);
fprintf(stderr,"     type %d size %dx%d\n", img->type,
					img->width, img->height);
#endif

if (img->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s : type %d invalide\n", __func__, img->type);
	return -99;
	}

h = img->height;	w = img->width;

for (idx=0; idx<h; idx++) {

#define FAST 1
#if FAST
	img->R[idx*w] = 0.0;
	img->G[idx*w] = 0.0;
	img->B[idx*w] = 0.0;
	img->R[(idx*w)+w-1] = 0.0;
	img->G[(idx*w)+w-1] = 0.0;
	img->B[(idx*w)+w-1] = 0.0;
#else
	fimg_plot_rgb(img, 0, idx, 0.0, 0.0, 0.0);
	fimg_plot_rgb(img, w-1, idx, 0.0, 0.0, 0.0);
#endif
	}

o = w * (h - 1);
for (idx=0; idx<w; idx++) {
#if FAST
	img->R[idx] = 0.0;
	img->G[idx] = 0.0;
	img->B[idx] = 0.0;
	img->R[idx+o] = 0.0;
	img->G[idx+o] = 0.0;
	img->B[idx+o] = 0.0;
#else
	fimg_plot_rgb(img, idx, 0, 0.0, 0.0, 0.0);
	fimg_plot_rgb(img, idx, h-1, 0.0, 0.0, 0.0);
#endif
	}

return 0;
}
/* -------------------------------------------------------------------- */
int fimg_lissage_2x2(FloatImg *img)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, img);
#endif

foo = fimg_lissage_2x2_a(img);
if (foo) {
	fprintf(stderr, "%s: fail %d\n", __func__, foo);
	return foo;
	}

fimg_killborders(img);

return foo;
}
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
/*
 *	XXX inplace filtering is a BAD IDEA
 */
int fimg_lissage_3x3(FloatImg *img)
{
int		foo;
FloatImg	tmp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p )\n", __func__, img);
#endif

static FimgFilter3x3   lowpass = {
                {
                1.0,    2.0,    1.0,
                2.0,    4.0,    2.0,
                1.0,    2.0,    1.0,
                },
        16.0,   0.0
        };

foo = fimg_clone(img, &tmp, 1);
if (foo) {
	fprintf(stderr, "%s: clone -> %d\n", __func__, foo);
	abort();
	}
foo = fimg_filter_3x3(&tmp, img, &lowpass);
if (foo) {
	fprintf(stderr, "%s: lowpass -> %d\n", __func__, foo);
	abort();
	}

foo = fimg_copy_data(&tmp, img);
if (foo) {
	fprintf(stderr, "%s: copy data -> %d\n", __func__, foo);
	abort();
	}

foo = fimg_destroy(&tmp);
if (foo) {
	fprintf(stderr, "%s: destroy -> %d\n", __func__, foo);
	abort();
	}

fimg_killborders(img);

return 0;
}
/* -------------------------------------------------------------------- */

