/*
 *		FLOATIMG - a kluge from tTh
 */


#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

extern int		verbosity;

/*
 *		a place for moving here Fonderie effects
 */
/* -------------------------------------------------------------- */
int fimg_binarize(FloatImg *pimg, int notused)
{
float		mm[6], mR, mG, mB;
int		foo, size;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, pimg, notused);
#endif

if (notused) {
	fprintf(stderr, "notused was %d, must be 0 in %s\n",
				notused, 		__func__);
	}

foo = fimg_get_minmax_rgb(pimg, mm);
mR = (mm[1] - mm[0]) / 2.0;
mG = (mm[3] - mm[2]) / 2.0;
mB = (mm[5] - mm[4]) / 2.0;

if (verbosity > 1)
	fprintf(stderr, "%s:  %f  %f  %f\n", __func__, mR, mG, mB);

size = pimg->width * pimg->height;

for (foo=0; foo<size; foo++) {
	if (pimg->R[foo] < mR)	pimg->R[foo] = mm[0];
	else			pimg->R[foo] = mm[1];
	if (pimg->G[foo] < mG)	pimg->G[foo] = mm[2];
	else			pimg->G[foo] = mm[3];
	if (pimg->B[foo] < mB)	pimg->B[foo] = mm[4];
	else			pimg->B[foo] = mm[5];
	}

return 0;
}
/* -------------------------------------------------------------- */

int fimg_trinarize(FloatImg *pimg, int notused)
{
float		mm[6], mRa, mGa, mBa, mRb, mGb, mBb;
float		*fptr;
int		foo, size;

if (notused) {
	fprintf(stderr, "notused was %d, must be 0 in %s\n",
				notused, 		__func__);
	}

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, pimg, notused);
#endif

foo = fimg_get_minmax_rgb(pimg, mm);
mRa = (mm[1] - mm[0]) * 0.33333;
mGa = (mm[3] - mm[2]) * 0.33333;
mBa = (mm[5] - mm[4]) * 0.33333;
mRb = (mm[1] - mm[0]) * 0.66666;
mGb = (mm[3] - mm[2]) * 0.66666;
mBb = (mm[5] - mm[4]) * 0.66666;

size = pimg->width * pimg->height;

for (foo=0; foo<size; foo++) {

	fptr = pimg->R;
	if (fptr[foo] < mRa || fptr[foo] > mRb)
		fptr[foo] = mm[0];
	else
		fptr[foo] = mm[1];

	fptr = pimg->G;
	if (fptr[foo] < mGa || fptr[foo] > mGb)
		fptr[foo] = mm[2];
	else
		fptr[foo] = mm[3];

	fptr = pimg->B;
	if (fptr[foo] < mBa || fptr[foo] > mBb)
		fptr[foo] = mm[4];
	else
		fptr[foo] = mm[5];

	}

return 0;
}
/* -------------------------------------------------------------- */
/*
 *			ici, il manque le quaternize :)
 */
/* -------------------------------------------------------------- */
