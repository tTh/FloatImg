/*
 *		interface FloatImg <-> libpnm(3)
 */

#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include <string.h>

#include  <pam.h>        

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
static void print_struct_pam(struct pam *ppam, char *text)
{
printf("\ttext                %s\n", text);
printf("\tsize                %d\n", ppam->size);

printf("\tformat              %d 0x%04x\n", ppam->format, ppam->format);
printf("\tplainformat         %d\n",	ppam->plainformat);
printf("\twidth & height      %d %d\n", ppam->width, ppam->height);
printf("\tdepth               %d\n", ppam->depth);
printf("\tmaxval              %lu\n", ppam->maxval);
}
/* --------------------------------------------------------------------- */

int fimg_pnm_infos(char *fname)
{
struct pam		inpam;
FILE			*fp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, fname);
#endif

printf("    --- infos for '%s' ------------\n", fname);

if (NULL==(fp=fopen(fname, "r"))) {
	perror(fname);
	exit(1);
	}

pnm_readpaminit(fp, &inpam, sizeof(inpam));
print_struct_pam(&inpam, fname);

fclose(fp);

return 0;
}
/* --------------------------------------------------------------------- */
