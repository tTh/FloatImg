#!/bin/bash

src=/dev/shm/foo.fimg
out=out.fimg
device=/dev/video2

maxi=59
W="320"
H="240"
grabopt=" -s ${W}x${H} -vv -u -d $device -p 0 -n 30
0 -c none "

mkdir /tmp/V
rm /tmp/V/*

G=$(printf "%dx%d+0+0" $W $H)

for foo in $(seq 0 $maxi)
do

	echo ; echo

	grabvidseq -$grabopt -o $src

	fval=$(echo "$foo / $maxi * 13.56636" | bc -l)
	echo ; echo $foo ' => ' $fval

	./t -vv -k $fval -o $out plasma $src

	# fimgstats $out

	dst=$(printf "/tmp/V/%03d.png" $foo)
	echo $dst
	montage $src $out -tile 1x2 -geometry $G $dst

	sleep 55

done

convert -delay 10 /tmp/V/*.png foo.gif

rm /tmp/V/*

