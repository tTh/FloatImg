/*
 *	FloatImg library from tTh - really ugly code inside
 */

#include  <stdio.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

/* -------------------------------------------------------------- */
/*	global vars exported from main
 */
extern	int	verbosity;

/* -------------------------------------------------------------- */
/*
 *	parameter mix is between 0.0 and 1.0 but other
 *	values give sometime good vibrations.
 */
int fimg_mix_rgb_gray(FloatImg *img, float mix)
{
int		x, y, p;
float		gr;

if (FIMG_TYPE_RGB != img->type) {
	fprintf(stderr, "%s bad type\n", __func__);
	return -6;
	}

for (y=0; y<img->height; y++) {
	p = y * img->width;		/* first pixel of the row */
	for (x=0; x<img->width; x++) {

		gr = (img->R[p] + img->G[p] + img->R[p]) / 3.0;

		img->R[p] = ((gr * mix) + (img->R[p] * (1.0-mix))) / 2.0;
		img->G[p] = ((gr * mix) + (img->G[p] * (1.0-mix))) / 2.0;
		img->B[p] = ((gr * mix) + (img->B[p] * (1.0-mix))) / 2.0;
		p++;			/* next pixel in the row */
		}
	}

return 0;
}
/* -------------------------------------------------------------- */
/*
 *	The third parameter was a six value array with min and max
 *	values maybe computed by the 'fimg_get_minmax_rgb' function.
 */
int     fimg_shift_to_zero(FloatImg *s, FloatImg *d, float coefs[6])
{
int             sz, idx;

if (FIMG_TYPE_RGB != s->type) {
	fprintf(stderr, "%s bad type\n", __func__);
	return -6;
	}

sz = s->width * s->height;
for (idx=0; idx<sz; idx++) {
	d->R[idx] = s->R[idx] - coefs[0];
	d->G[idx] = s->G[idx] - coefs[2];
	d->B[idx] = s->B[idx] - coefs[4];
	}

return 0;
}
/* -------------------------------------------------------------- */
/*
 *    I think that this function is fully buggy, and need
 *    more explanations.
 */
int  fimg_auto_shift_to_zero(FloatImg *src, FloatImg *dst)
{
float		coefs[6];
int		foo;
float		minima = 1e7;		/* magic value ? */

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, src, dst);
#endif

if (FIMG_TYPE_RGB != src->type) {
	fprintf(stderr, "%s: bad image type %d\n", __func__, src->type);
	return -6;
	}

foo = fimg_get_minmax_rgb(src, coefs);
if (foo) {
	fprintf(stderr, "%s: err %d get minmax\n", __func__, foo);
	return foo;
	}

/* crude hack for now */
if (coefs[0] < minima)	minima = coefs[0];
if (coefs[2] < minima)	minima = coefs[2];
if (coefs[4] < minima)	minima = coefs[4];

coefs[0] = coefs[2] = coefs[4] = minima;

foo = fimg_shift_to_zero(src, dst, coefs);
if (foo) {
	fprintf(stderr, "%s WTF? %d\n", __func__, foo);
	return foo;
	}

return 0;
}
/* -------------------------------------------------------------- */

