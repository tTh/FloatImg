/*
 *		FLOATIMG - a kluge from tTh
 *	effets spéciaux bizarres sur les couleurs.
 *	nouveau pour un truc chelou avec Maëva
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

/*			WARNING
		some crapy code cuted & pasted here			*/

/* --------------------------------------------------------------------- */
static void highlight_red(FloatImg *src, FloatImg *dst, float fval)
{
int		sz, idx;

sz = src->width * src->height;
for (idx=0; idx<sz; idx++) {
	dst->G[idx] = src->G[idx];
	dst->B[idx] = src->B[idx];
	if ( 	(src->G[idx] < src->R[idx])	&&
		(src->B[idx] < src->R[idx])	) {
		dst->R[idx] = src->R[idx] * fval;
		}
	else	{
		dst->R[idx] = src->R[idx];
		}
	}
}
/* --------------------------------------------------------------------- */
static void highlight_green(FloatImg *src, FloatImg *dst, float fval)
{
int		sz, idx;

sz = src->width * src->height;
for (idx=0; idx<sz; idx++) {
	dst->R[idx] = src->R[idx];
	dst->B[idx] = src->B[idx];
	if ( 	(src->R[idx] < src->R[idx])	&&
		(src->B[idx] < src->R[idx])	) {
		dst->G[idx] = src->G[idx] * fval;
		}
	else	{
		dst->G[idx] = src->G[idx];
		}
	}
}
/* --------------------------------------------------------------------- */
static void highlight_blue(FloatImg *src, FloatImg *dst, float fval)
{
int		sz, idx;

sz = src->width * src->height;
for (idx=0; idx<sz; idx++) {
	dst->G[idx] = src->G[idx];
	dst->R[idx] = src->R[idx];
	if ( 	(src->G[idx] < src->B[idx])	&&
		(src->R[idx] < src->B[idx])	) {
		dst->B[idx] = src->B[idx] * fval;
		}
	else	{
		dst->B[idx] = src->B[idx];
		}
	}
}
/* --------------------------------------------------------------------- */
int fimg_highlight_color(FloatImg *src, FloatImg *dst, char color, float fval)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p [%c] %f )\n", __func__,
			src, dst, color, fval);
#endif

if (FIMG_TYPE_RGB != src->type) {
	fprintf(stderr, "%s: bad src type %d on %p\n", __func__,
					src->type, src);
	return -8;
	}
if (fimg_images_not_compatible(src, dst)) {
	fprintf(stderr, "oh fuck in %s\n", __func__);
	return -9;
	}

switch (color) {

	case 'r': case 'R':
		highlight_red(src, dst, fval);			break;
	case 'g': case 'G':
		highlight_green(src, dst, fval);		break;
	case 'b': case 'B':
		highlight_blue(src, dst, fval);			break;

	default:
		fprintf(stderr, "%s: '%c' is invalid\n", __func__, color);
		return -11;
		break;			/* nottreached */
	}

return 0;
}
/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
