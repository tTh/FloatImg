/*
 *	DECOMPOSITION RGB
 */

#include  <stdio.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

extern int		verbosity;

/* == ---------------------------------------------------- == */

/*	some dirty macros				*/

#define pixidx(fi,x,y) (((y)*fi->width)+(x))

#define getRpix(fi,x,y) (fi->R[ pixidx(fi,(x),(y)) ])
#define getGpix(fi,x,y) (fi->G[ pixidx(fi,(x),(y)) ])
#define getBpix(fi,x,y) (fi->B[ pixidx(fi,(x),(y)) ])

/*	A lot of strange and usefull parenthesis	*/

/* == ---------------------------------------------------- == */
#if DEBUG_LEVEL
static float compute_z_value(float r, float g, float b)
{
double		dval;

return 42.0;
}
#endif
/* == ---------------------------------------------------- == */

int fimg_decomp_rgbz_color(FloatImg *psrc, FloatImg *pdst, int k)
{
int		x, y, x2, y2;
int		w2, h2;
float		cumul, vgray;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__,
			psrc,  pdst, k);
#endif

/*
 *	XXX useless message ?
 */
if (k) { fprintf(stderr, "k=%d in %s\n", k, __func__); }

fimg_clear(pdst);
w2 = psrc->width/2;	h2 = psrc->height/2;

for (y=0; y<h2; y++)
	{
	y2 = y * 2;
	for (x=0; x<w2; x++) {
		x2 = x * 2;

		cumul = getRpix(psrc, x2, y2) +
			getRpix(psrc, x2, y2+1) +
			getRpix(psrc, x2+1, y2) +
			getRpix(psrc, x2+1, y2+1);
		cumul /= 4, vgray = cumul;
		pdst->R[pixidx(pdst,x,y)] = cumul;
		pdst->R[pixidx(pdst,x+w2,y+h2)] = cumul;

		cumul = getGpix(psrc, x2, y2) +
			getGpix(psrc, x2, y2+1) +
			getGpix(psrc, x2+1, y2) +
			getGpix(psrc, x2+1, y2+1);
		cumul /= 4, vgray += cumul;
		pdst->G[pixidx(pdst,x+w2,y)] = cumul;
		pdst->G[pixidx(pdst,x+w2,y+h2)] = cumul;

		cumul = getBpix(psrc, x2, y2) +
			getBpix(psrc, x2, y2+1) +
			getBpix(psrc, x2+1, y2) +
			getBpix(psrc, x2+1, y2+1);
		cumul /= 4, vgray += cumul;
		pdst->B[pixidx(pdst,x,y+h2)] = cumul;
		pdst->B[pixidx(pdst,x+w2,y+h2)] = cumul;

		}

	}


return 0;
}

/* == ---------------------------------------------------- == */

		/* PUTAIN LE COPIER/COLLÉ DE BATARD !!! */

int fimg_decomp_rgbz_gray(FloatImg *psrc, FloatImg *pdst, int k)
{
int		x, y, x2, y2;
int		w2, h2, idx;
float		cumul, vgray;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__,
			psrc,  pdst, k);
#endif

/*
 *	XXX useless message ?
 */
if (k) { fprintf(stderr, "k=%d in %s\n", k, __func__); }

fimg_clear(pdst);
w2 = psrc->width/2;	h2 = psrc->height/2;

for (y=0; y<h2; y++)
	{
	y2 = y * 2;
	for (x=0; x<w2; x++) {
		x2 = x * 2;

		cumul = getRpix(psrc, x2, y2) +
			getRpix(psrc, x2, y2+1) +
			getRpix(psrc, x2+1, y2) +
			getRpix(psrc, x2+1, y2+1);
		cumul /= 4, vgray = cumul;
		idx = pixidx(pdst,x,y);
		pdst->R[idx] = pdst->G[idx] = pdst->B[idx] = cumul;

		cumul = getGpix(psrc, x2, y2) +
			getGpix(psrc, x2, y2+1) +
			getGpix(psrc, x2+1, y2) +
			getGpix(psrc, x2+1, y2+1);
		cumul /= 4, vgray += cumul;
		idx = pixidx(pdst,x+w2,y);
		pdst->R[idx] = pdst->G[idx] = pdst->B[idx] = cumul;

		cumul = getBpix(psrc, x2, y2) +
			getBpix(psrc, x2, y2+1) +
			getBpix(psrc, x2+1, y2) +
			getBpix(psrc, x2+1, y2+1);
		cumul /= 4, vgray += cumul;
		idx = pixidx(pdst,x,y+h2);
		pdst->R[idx] = pdst->G[idx] = pdst->B[idx] = cumul;

		idx = pixidx(pdst,x+w2,y+h2);
		pdst->R[idx] = pdst->G[idx] = 		\
				pdst->B[idx] =  vgray / 3.0;
		}

	}


return 0;
}

/* == ---------------------------------------------------- == */
