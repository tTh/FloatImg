/*
 *		classif.c
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include  <stdint.h>
#include   <math.h>

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/* nouveau 2 octobre 2020, juste avant sonoptic de la pluie craignos	 */

int fimg_classif_trial(FloatImg *psrc, FloatImg *pdst, float fval, int flags)
{
float		minmax[6], delta[3], baryc[3];
float		range, dist, rgb[3], dr, dg, db;
int		x, y, on, off;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %f %d )\n", __func__,
					psrc, pdst, fval, notused);
#endif

if (flags) { fprintf(stderr, "flags: 0x%04x in %s\n", flags, __func__); }

if (FIMG_TYPE_RGB != psrc->type) {
	fprintf(stderr, "%s: bad src type %d\n", __func__, psrc->type);
	return -7;
	}
if (fimg_images_not_compatible(psrc, pdst)) {
	fprintf(stderr, "%s: bad dst type %d\n", __func__, pdst->type);
	return -8;
	}

/* calculer les amplitudes RGB de l'image source */
fimg_get_minmax_rgb(psrc, minmax);
delta[0] = minmax[1] - minmax[0];
delta[1] = minmax[3] - minmax[2];
delta[2] = minmax[5] - minmax[4];

/* chercher le plus petit des deltas */
range = delta[0]; 	if (delta[1]<range) range=delta[1];
			if (delta[2]<range) range=delta[2];

/* convertir le diametre en rayon (magic inside) */
range *= fval;

if (verbosity > 1) fprintf(stderr, "deltas : %f  %f  %f  /  %f\n",
					delta[0], delta[1], delta[2], range);


/* calcul du "barycentre" chromatique */
baryc[0] = (minmax[1] + minmax[0]) / 2;
baryc[1] = (minmax[3] + minmax[2]) / 2;
baryc[2] = (minmax[5] + minmax[4]) / 2;
if (verbosity > 1) fprintf(stderr, "barycs : %f  %f  %f\n",
					baryc[0], baryc[1], baryc[2]);

on = off = 0;

for (y=0; y<psrc->height; y++) {
	for (x=0; x<psrc->width; x++) {

		fimg_get_rgb(psrc, x, y, rgb);

		/* calcul de la distance chromatique */
		dr = rgb[0] - baryc[0];
		dg = rgb[1] - baryc[1];
		db = rgb[2] - baryc[2];
		dist = sqrtf( (dr*dr) + (dg*dg) + (db*db) );

#if DEBUG_LEVEL
	if (x==y) fprintf(stderr, "%4d  %4d   %f\n", x, y, dist);
#endif

		/*   action !!!   */
		if (dist > range) {
			/* make our pixel gray-level */
			rgb[0] = rgb[1] = rgb[2] = 0.0;
				// (rgb[0] + rgb[1] + rgb[2]) / 3.0; 
			on++;
			}
		else	{
			/* the easy part : do nothing */
			off++;
			}

		fimg_put_rgb(pdst, x, y, rgb);

		/* MUST BE MORE CREATIVE HERE !!! */
		}
	}

if (verbosity > 1)	fprintf(stderr, "on %d off %d\n", on, off);

return 0;
}

/* --------------------------------------------------------------------- */


