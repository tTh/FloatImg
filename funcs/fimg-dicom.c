/*
      Digital Imaging and Communications in Medicine

 nouveau Fri 26 Nov 2021 11:12:44 PM CET - allée de Dinan

 */

#include  <stdio.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

extern int verbosity;		/* must be declared around main() */

/* --------------------------------------------------------------------- */
int fimg_save_plane_as_dicom(FloatImg *src, char *outname,
						char plane, int flags)
{
float		*planeptr;

fprintf(stderr, ">>> %s ( %p %s %c %d )\n", __func__, src, outname, plane, flags);

planeptr = charplane2int(plane, src);		/* please explain XXX */
fprintf(stderr, "planeptr is %p\n", planeptr);

if (verbosity) {
	fimg_describe(src, outname);
	}

return -1;
}
/* --------------------------------------------------------------------- */
