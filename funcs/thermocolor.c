/*
 *		FLOATIMG
 *	fabriquer une pseudo-image thermique
 *
 * new: Wed Mar 20 16:55:09 UTC 2024 dans ma maison du Gers
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <string.h>
#include  <stdint.h>

#include  "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/*
 *	parameter k in not used, but may be in the futur.
 */
int fimg_auto_thermique(FloatImg *src, FloatImg *dst, int k)
{
// int		x, y, off;
int		idx, surface;

float		seuil, gray;
double		graymean;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %p  %d )\n", __func__, src, dst, k);
#endif

if (0!=k) {
	fprintf(stderr, "%s: k must be 0, was %d\n", __func__, k);
	}

surface = src->width * src->height;
// fprintf(stderr, "surface = %d\n", surface);

graymean = 0.0;
for (idx=0; idx<surface; idx++) {
	gray = src->R[idx] + src->G[idx] + src->B[idx];
	graymean += (double)gray;
	}
seuil = (float) (graymean / surface);
// fprintf(stderr, "graymean = %f   seuil = %f\n", graymean, seuil);

/*
 *	please, explain this number brotching
 */
for (idx=0; idx<surface; idx++) {
	gray = src->R[idx] + src->G[idx] + src->B[idx];
	if (gray > seuil) {
		dst->R[idx] = gray - seuil;
		dst->B[idx] = seuil / 8.0;
		}
	else	{
		dst->G[idx] = gray;
		dst->B[idx] = seuil / 8.0;
		}
	}

return 0;
}


/* --------------------------------------------------------------------- */
