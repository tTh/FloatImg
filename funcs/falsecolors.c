/*
 *	FloatImg library from tTh - really ugly code inside
 *
 *		F A L S E   C O L O R S
 *						or fake colors ?
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <sys/time.h>
#include  <math.h>
#include  "../floatimg.h"

/* -------------------------------------------------------------- */
/*	this is a global vars exported from main */
extern	int	verbosity;

/* -------------------------------------------------------------- */
/*			TRUCS A VOIR

f(x,y) = (((y & x) * (y - x)) % ((21 & x) * (y | 8))) ^ (~((x & 7) | (x % x)))


*/
/* -------------------------------------------------------------- */
/* nouveau 18 mai 2022 */
/* please explain the meaning of 'valf' parameter */
int fimg_falsecolors_0(FloatImg *src, FloatImg *dst, int k, float valf)
{
int		x, y, offset;
float		r, g, b, gray, maxv;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %d  %f )\n", __func__,
					src, dst, k, valf);
#endif

if (k) {
	fprintf(stderr, "%s : %d %f\n", __func__, k, valf);
	}

/* check validity of parameters */
if (FIMG_TYPE_RGB != dst->type) {
	fprintf(stderr, "in %s, picz at %p is not valid\n",
			__func__, dst);
	abort();
		/* BLAM! */
	}


maxv = fimg_get_plane_maxvalue(src, 'r');
fprintf(stderr, "%s: maxv of red plane = %f\n", __func__, maxv);

/* enter big loop */
offset = 0;
for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {

		gray = src->R[offset];

		if (gray < maxv/2.0) {
			r = gray * 2.0;
			g = 0.0;
			}
		else 	{
			r = 0.0;
			g = gray * 2.0;
			}
		b = fmodf(gray*8.0, maxv);

		dst->R[offset] = r;
		dst->G[offset] = g;
		dst->B[offset] = b;

		/* and GOTO next pixel */
		offset++;
		}
	}

return 0;
}
/* -------------------------------------------------------------- */
