/*
 *	tests des fonctions diverses - prototypes
	see also: t.c & tests.c
 */

int essai_plasma(char *infile, char *outfile, int ikoef, float fkoef);
int essai_miroir(char *inf, char *outf, int flags);
int essai_killrgb(char *inf, char *outf);
int essai_decomprgb_color(char *inf, char *outf);
int essai_decomprgb_gray(char *inf, char *outf);
int essai_rndfluffy(char *infile, char *outfile, int k);	/* sfx3.c */
int essai_split_level(char *inf, char *outf, int flags);

int essai_displacement(char *infile, char *outfile);
int essai_qsort_rgb(char *infile, char *outfile);
int essai_equalize(char *infile);
int essai_ecriture_fits(char *outname);
int essai_rotate(char *infile);
int essai_filtrage_2x2(char *infile);
int essai_filtrage_3x3(char *infile);
int essai_sfx0(char *infile);
int essai_mire(char *infile, int wtf);
int essai_ecriture_png(char *infile);
int essai_ecriture_tiff(char *infile);
int fimg_essai_hsv(char *infile);
int essai_classif(char *infile, char *outfile, float fvalue);
int essai_contour_2x2(char *filename, char *outfile);
int essai_geometrie(char *infile, int notused);
int essai_detect_type(void);
int fimg_essai_histo(FloatImg *src, char *outpic, int k); /* histogram.c */
int essai_histogramme(char *fname, int k);
int essai_0_fausses_couleurs(char *dstfile, int type);
int essai_thermocol(char *infile, char *outfile);
int essai_triptyq(char *infile, char *outfile);

int essai_lecture_png(char *fname, char *outfile, int notused);

int essai_highlights(char *inf, char *outf, int ikoef, float fkoef);
int essai_openexr(char *inf, char *outf, int flags);
int essai_fmorpho_0(char *infile, char *basefname, int k);

int essai_pixelize(char *infile, char *outfile);

int essai_rectangle(char *outf, int k);

int essai_dicom(char *inf, char *outf, int k);
