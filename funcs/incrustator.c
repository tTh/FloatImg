/*
 *		incrustator VERY experimental
 *						KRKRK
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <stdlib.h>

#include  "../floatimg.h"

// XXX #include  "incrustator.h"

extern int 		verbosity;

/* ---------------------------------------------------------------- */
static int check_boundaries(FloatImg *from, FloatImg *to, FimgArea51 *a51)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, from, to, a51);
#endif

if (verbosity > 1) {
	fimg_printdims("from", from);
	fimg_printdims("to  ", to);
	}

/* just a small molly-guard */
if ( (a51->w < 0) || (a51->h < 0) ) {
	fprintf(stderr, "%s: fubar on %p\n", __func__, a51);
	abort();		/* FY Bro ! */
	}

return 0;
}
/* ---------------------------------------------------------------- */
/* XXX
static int move_pixels(FloatImg *from, FloatImg *to,
				FimgArea51 *a51, int flags)
{
#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p 0x%04x )\n", __func__,
				from, to, a51, flags);
#endif

return -1;
}
*/
/* ---------------------------------------------------------------- */
/*
 *	See also:    fimg_extractor() in geometry.c
 */
int fimg_incrustator_0(FloatImg *psrc, FloatImg *pdst,
				int xpos, int ypos, int flags)
{
int		y, srcpos, dstpos, szl;
int		foo;

FimgArea51	area;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d %d 0x%04X\n", __func__, psrc, pdst,
							xpos, ypos, flags);
#endif

if (flags) { fprintf(stderr, "flags: 0x%04x in %s\n", flags, __func__); }

if (verbosity > 1) {
	fimg_describe(psrc, "source");
	fimg_describe(pdst, "destination");
	}

/* check boudaries */
area.x = xpos;		area.y = ypos;
area.w = psrc->width;	area.h = psrc->height;
foo = check_boundaries(psrc, pdst, &area);
if (foo) {
	fprintf(stderr, "Boudary error %d in %s:%d\n", foo, __func__, __LINE__);
	return foo;
	}

if (	(xpos < 0)  || (xpos > pdst->width  - psrc->width)  ||
	(ypos < 0)  || (ypos > pdst->height - psrc->height) ) {
	fprintf(stderr, "%s: boudary error, x=%d y=%d\n", __func__,
					    xpos, ypos);
	return -2;
	}

/* move all the data by looping over lines */
srcpos = 0;
dstpos = (ypos * pdst->width) + xpos;
szl = psrc->width * sizeof(float);
for (y=0; y<psrc->height; y++) {
	// fprintf(stderr, " %7d   %7d   %7d\n", y, srcpos, dstpos);

	memcpy(pdst->R + dstpos, psrc->R + srcpos, szl);
	memcpy(pdst->G + dstpos, psrc->G + srcpos, szl);
	memcpy(pdst->B + dstpos, psrc->B + srcpos, szl);

	srcpos += psrc->width;
	dstpos += pdst->width;
	}

return 0;
}
/* ---------------------------------------------------------------- */
