# Fonctions

Plein de fonctions qu'il serait bon de documenter :)


## Contours

Détecter des contours est une activité respectable. Mais difficile.
Une recherche dans la littérature s'impose.

## Exporter

`fimg_exporter` est une méta-fonction qui va sauvegarder (dans la mesure de ses conséquences)
une image en fonction de l'extension du nom de fichier fourni.

### PNG

__Attention__ : la bibliothèque `pnglite` actuellement utilisée pour lire
les fichiers PNG n'accepte que certains types de fichiers.
Et en particulier, elle brotche sur ceux produits par ImageMagick !

### FITS

Ce [format de fichier](https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/c_user/node1.html)
est utilisé en astronomie.
Son support est actuellement minimaliste.


### DICOM

https://en.wikipedia.org/wiki/DICOM et ça semble bien compliqué,
donc ça reste pour le moment au stade de vague projet :(

## Sfx

Effets spéciaux divers. Ils sont répartis dans plusieurs fichiers
(`sfx[0-4].c`) sans raison de classement apparente.

Certains peuvent être directement utilisés
avec l'outil [../tools/fimgfx.c](fimgfx).
Leur paramétrage est trop sommaire.

La fonction `fimg_pixelize_h_rnd` est issue d'une idée qui m'est venue
dans la roulotte de TerreBlanque. Elle a besoin de recherches sur la
dynamique temporelle, et d'une FSM à trois états.

## Dithering

Work in progress...

Une difficulté dans ce domaine, c'est la *floattitude* des pixels,
sur laquelle il est difficile d'appliquer des operations binaires,
comme le XOR.
