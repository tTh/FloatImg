/*
 *	FloatImg : some dithering experiments
 */

#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include <string.h>
#include   <math.h>

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
int fimg_dither_0(FloatImg *psrc, FloatImg *pdst, int flags)
{
int		x, y;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p 0x%04x )\n", __func__, psrc, pdst, flags);
#endif

if (flags) { fprintf(stderr, "flags: 0x%04x in %s\n", flags, __func__); }

if (fimg_images_not_compatible(psrc, pdst)) {
	fprintf(stderr, "%s: shit happen\n", __func__);
	return -2;
	}

for (y=0; y<psrc->height; y++) {

	for (x=0; x<psrc->width; x++)
		{

		/* PLEASE DO SOMETHING HERE */

		}
	}

return -1;
}
/* --------------------------------------------------------------------- */

