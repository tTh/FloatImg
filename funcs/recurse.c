/*
                RECURSION 'QUADTREE' SUR LES IMAGES
                -----------------------------------
*/

#include   <stdio.h>
#include   <math.h>
#include  <stdint.h>
#include  <sys/time.h>
#include  <stdlib.h>

#include   "../floatimg.h"

/* -------------------------------------------------------------------- */
/*		may be we need some private variables ? 		*/
/* --------------------------------------------------------------------	*/
/* nouveau 29 avril 2021, pendant un autre masque-flamme coronavidique	*/

int fimg_recursion_proto(FloatImg *src, FloatImg *dst, int notused)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, src, dst, notused);
#endif

if (notused) {
	fprintf(stderr, "%s parameter notused is %d, must be 0\n", 
			__func__, notused);
	return -2;
	}

fprintf(stderr, "!!!!!! %s is a wip !!!!!\n", __func__);

if (fimg_images_not_compatible(src, dst)) {
	fprintf(stderr, "%s: compatibility  fatalerror\n", __func__);
	exit(1);
	}
/*
 *	bon, maintenant, il faut plonger dans du code du
 *	siecle dernier et l'adapter a ce nouveau contexte
 */

return -1;
}
/* -------------------------------------------------------------------- */
/* -------------------------------------------------------------------- */
