/*
 *		Detection de contours
 */

#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include <string.h>
#include   <math.h>

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/* nouveau 4 octobre 2020, juste avant sonoptic de la pluie craignos	 */

int fimg_contour_2x2(FloatImg *psrc, FloatImg *pdst, int reverse)
{
float		avg[4];
int		foo, x, y, q;
float		v1, v2;

int     tbl[] = 			/* deep magic inside */
        {
        0,      1,      1,      1,
        1,      1,      0,      1,
        1,      0,      1,      1,
        1,      1,      1,      0
        };				/* please explain */

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, psrc, pdst, reverse);
#endif

if (FIMG_TYPE_RGB != psrc->type) {
	fprintf(stderr, "%s: bad src type %d\n", __func__, psrc->type);
	return -7;
	}
if (fimg_images_not_compatible(psrc, pdst)) {
	fprintf(stderr, "%s: bad dst type %d\n", __func__, pdst->type);
	return -8;
	}

if (reverse) {
	v1 = 0.0;	v2 = 1.0;
	}
else	{
	v1 = 1.0;	v2 = 0.0;
	}

foo = fimg_meanvalues(psrc, avg);
if (foo) {
	fprintf(stderr, "%s: err %d on fimg_meanvalues\n", __func__, foo);
	return foo;
	}
if (verbosity > 1) {
	fprintf(stderr, "%s: %f %f %f\n", __func__, avg[0], avg[1], avg[2]);
	}

#define RP(ix, iy)   ( psrc->R[((iy)*psrc->width)+(ix)] < avg[0] )
#define GP(ix, iy)   ( psrc->G[((iy)*psrc->width)+(ix)] < avg[1] )
#define BP(ix, iy)   ( psrc->B[((iy)*psrc->width)+(ix)] < avg[2] )

for (y=0; y<psrc->height-1; y++) {

	for (x=0; x<psrc->width-1; x++) {

		q = (	( RP(x, y)	<< 3 )	|
			( RP(x+1, y)	<< 2 )	|
			( RP(x, y+1)	<< 1 )	|
			( RP(x+1, y+1)	     )	);
		pdst->R[(y*psrc->width)+x] = tbl[q] ? v1 : v2 ;

		q = (	( GP(x,  y)	<< 3 )	|
			( GP(x+1, y)	<< 2 )	|
			( GP(x, y+1)	<< 1 )	|
			( GP(x+1, y+1)	     )	);
		pdst->G[(y*psrc->width)+x] = tbl[q] ? v1 : v2 ;

		q = (	( BP(x, y)	<< 3 )	|
			( BP(x+1, y)	<< 2 )	|
			( BP(x, y+1)	<< 1 )	|
			( BP(x+1, y+1)	     )	);
		pdst->B[(y*psrc->width)+x] = tbl[q] ? v1 : v2 ;

		}

	}

/* kill potential NaN values in last row or last column */
				fimg_killborders(pdst);

return 0;
}
/* --------------------------------------------------------------------- */
