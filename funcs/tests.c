/*
 *			FLOATIMG
 *			--------

 *	tests des fonctions diverses - subroutines
	see also: t.c
 */

#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include <string.h>
#include  <pam.h>

#undef 		DEBUG_LEVEL
#define		DEBUG_LEVEL	1

#include "../floatimg.h"
#include  "tests.h"

extern int		verbosity;

#define W 1024
#define H 768

/* --------------------------------------------------------------------- */
/* new Mon Apr  1 15:35:57 UTC 2024     -> sfx4.c			 */
int essai_triptyq(char *infile, char *outfile)
{
FloatImg	src, dst;
int		idx, foo;

fprintf(stderr, ">>> %s ( %s %s )\n", __func__, infile, outfile);

foo = fimg_create_from_dump(infile, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		infile);
	return foo;
	}
fimg_clone(&src, &dst, 1);

foo = fimg_make_triptyq(&src, &dst, 0);
if (0 != foo) {
	fprintf(stderr, "%s: make_triptyq -> %d\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (0 != foo) {
	fprintf(stderr, "%s: err %d exporting to '%s'\n", __func__,
				foo, 		outfile);
	return foo;
	}

return 0;
}

/* --------------------------------------------------------------------- */
/*
 *	nouveau 30 octobre 2022     --> fmorpho.c
 */
int essai_fmorpho_0(char *infile, char *basefname, int k)
{
FloatImg	src, dst;
int		idx, foo;
char		fname[100];

fprintf(stderr, ">>> %s ( '%s'  '%s'  %d )\n", __func__, infile, basefname, k);

if (k) {
	fprintf(stderr, "%s k=%d must be 0\n", __func__, k);
	return -6;
	}

if (NULL == infile ) {
	foo = fimg_create(&src, W, H, FIMG_TYPE_RGB);
	if (foo) {
		fprintf(stderr, "%s: create -> %d\n",__func__, foo);
		return foo;
		}
	foo = fimg_test_pattern(&src, 0, 254.99);
	if (foo) {
		fprintf(stderr, "%s: test_pattern -> %d\n",__func__, foo);
		return foo;
		}
	}
else	{
	foo = fimg_create_from_dump(infile, &src);
	if (foo) {
		fprintf(stderr, "%s: error %d loading '%s'\n", __func__, foo, infile);
		return foo;
		}
	}

fimg_save_as_pnm(&src, "foo.pnm", 0);

foo = fimg_clone(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s: clone -> %d\n",__func__, foo);
	return foo;
	}

for (idx=0; idx<9; idx++) {
	sprintf(fname, "%s%04d.pnm", basefname, idx);
	fprintf(stderr, "      ---> %s\n", fname);
	foo = fimg_filtre_morpho_0(&src, &dst, idx);
	if (foo) {
		fprintf(stderr, "%s: got a %d at round %d\n", __func__, foo, idx);
		return foo;
		}
	fimg_save_as_pnm(&dst, fname, 0);
	}

fimg_destroy(&src);	fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */
/* nouveau 18 mai 2022 */
int essai_0_fausses_couleurs(char *dstfile, int type)
{
FloatImg	src, dst;
int		foo;

fprintf(stderr, "\nEssais fausses couleurs (type %d) -> '%s'\n", type, dstfile);

foo = fimg_create(&src, W, H, FIMG_TYPE_RGB);
foo = fimg_hdeg_a(&src, 12e3);
// fimg_export_picture(&src, "debug.pnm", 0);
foo = fimg_clone(&src, &dst, 0);
foo = fimg_clear(&dst);

foo = fimg_falsecolors_0(&src, &dst, type, 3.141592654);
fprintf(stderr, "false colors 0 --> %d\n", foo);

foo = fimg_export_picture(&dst, dstfile, 0);
fimg_destroy(&src);	fimg_destroy(&dst);

return -1;
}
/* --------------------------------------------------------------------- */
int essai_dicom(char *inf, char *outf, int k)
{
int		foo;
FloatImg	img;

fprintf(stderr, ">>> %s ( %s %s %d )\n", __func__, inf, outf, k);

foo = fimg_create(&img, 320, 240, FIMG_TYPE_RGB);
fimg_drand48(&img, 1.0);

foo = fimg_save_plane_as_dicom(&img, outf, 'R', 0);
fprintf(stderr, "dicom: save a plane --> %d\n", foo);

return -1;
}
/* --------------------------------------------------------------------- */
int essai_rectangle(char *outf, int k)
{
FloatImg	img;
int		foo;
int		rect[4];

if (0 != k) {
	fprintf(stderr, "in %s, k muste be 0, was %d\n", __func__, k);
	}

foo = fimg_create(&img, 320, 240, FIMG_TYPE_RGB);

fimg_drand48(&img, 1.0);

rect[0] = 100;		rect[1] = 100;
rect[2] = 32;		rect[3] = 16;

foo = fimg_clear_rectangle(&img, rect);

foo = fimg_export_picture(&img, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

fimg_destroy(&img);
return 0;
}
/* --------------------------------------------------------------------- */
int essai_killrgb(char *inf, char *outf)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, ">>> %s ( %s %s )\n", __func__, inf, outf);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}
fimg_clone(&src, &dst, 1);

foo = fimg_killrgb_v(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s:%s(): fail %d line %d\n",
			__FILE__, __func__, foo, __LINE__);
	return foo;
	}

foo = fimg_export_picture(&dst, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */
int essai_decomprgb_gray(char *inf, char *outf)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, ">>> %s ( %s %s )\n", __func__, inf, outf);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}
fimg_clone(&src, &dst, 1);

foo = fimg_decomp_rgbz_gray(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s:%s(): fail %d line %d\n",
			__FILE__, __func__, foo, __LINE__);
	return foo;
	}

foo = fimg_export_picture(&dst, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

fimg_destroy(&src);   fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_decomprgb_color(char *inf, char *outf)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, ">>> %s ( %s %s )\n", __func__, inf, outf);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}
fimg_clone(&src, &dst, 1);

foo = fimg_decomp_rgbz_color(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s:%s(): fail %d line %d\n",
			__FILE__, __func__, foo, __LINE__);
	return foo;
	}

foo = fimg_export_picture(&dst, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

fimg_destroy(&src);   fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */

int fimg_recursion_proto(FloatImg *src, FloatImg *dst, int notused);

int essai_recursion(char *inf, char *outf, int flags)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, ">>> %s ( '%s' '%s' 0x%04X )\n", __func__,
				inf, outf, flags);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}

fimg_clone(&src, &dst, 0);

foo = fimg_recursion_proto(&src, &dst, flags);
if (foo) {
	fprintf(stderr, "%s: fail %d\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

return -1;
}
/* --------------------------------------------------------------------- */
int essai_miroir(char *inf, char *outf, int flags)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, ">>> %s ( '%s' '%s' 0x%X )\n", __func__,
				inf, outf, flags);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}

fimg_clone(&src, &dst, 0);

/*	run the crappy code */
foo = fimg_mirror(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "err %d in fimg_mirror\n", foo);
	return -6;
	}

foo = fimg_export_picture(&dst, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */
/* new  Sun Feb 12 10:34:06 CET 2023 
 */
int essai_rndfluffy(char *infile, char *outfile, int k)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, ">>> %s ( '%s'  '%s'  %d )\n", __func__,
			infile, outfile, k);

foo = fimg_create_from_dump(infile, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		infile);
	return foo;
	}
fprintf(stderr, "    %s loaded.\n", infile);

fimg_clone(&src, &dst, 0);
fprintf(stderr, "    %p cloned.\n", &src);

foo = fimg_make_rndfluffy_lines(&src, &dst, 80);
if (foo) {
	fprintf(stderr, "err %d while making fluffy\n", foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s: err %d exporting %s\n", __func__, foo, outfile);
	return foo;
	}
fimg_destroy(&src);	fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_split_level(char *inf, char *outf, int flags)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, ">>> %s ( '%s' '%s' 0x%X )\n", __func__,
				inf, outf, flags);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}
fimg_clone(&src, &dst, 0);
foo = fimg_split_level(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "err %d in split_level\n", foo);
	return -6;
	}
foo = fimg_export_picture(&dst, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */
/* nouveau 21 mars 2021 - rue d'Aragon */
int essai_openexr(char *inf, char *outf, int flags)
{
FloatImg	src;
int		foo;

fprintf(stderr, ">>> %s ( '%s' '%s' 0x%X )\n", __func__,
				inf, outf, flags);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}
// fprintf(stderr, "image loaded at %p\n", &src);
fimg_describe(&src, "for save EXR test");

foo = fimg_save_as_exr(&src, outf, flags);

fimg_destroy(&src);

return -2;
}
/* --------------------------------------------------------------------- */
/* nouveau 20 mars 2021 - rue d'Aragon */
int essai_highlights(char *inf, char *outf, int ikoef, float fkoef)
{
FloatImg	src, dst;
int		foo;

fprintf(stderr, ">>> %s ( '%s' '%s' %d %g )\n", __func__,
				inf, outf, ikoef, fkoef);

foo = fimg_create_from_dump(inf, &src);
if (0 != foo) {
	fprintf(stderr, "%s: err %d loading image '%s'\n", __func__,
				foo, 		inf);
	return foo;
	}

fimg_clone(&src, &dst, 0);

foo = fimg_highlight_color(&src, &dst, 'R', fkoef);
if (foo) {
	fprintf(stderr, "%s: err %d ?\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outf, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */

int essai_plasma(char *infile, char *outfile, int ikoef, float fkoef)
{
FloatImg	src, dst;
int		foo;

fprintf(stderr, ">>> %s ( '%s' '%s' %d %g )\n", __func__,
				infile, outfile, ikoef, fkoef);

/* if infile is loadable, use it for background */
foo = fimg_create_from_dump(infile, &src);
if (0 == foo) {
	fprintf(stderr, "%s: image '%s' loaded\n", __func__, infile);
	}
else	{
	/* make a fancy synthetic picture */
	foo = fimg_create(&src, 800, 600, FIMG_TYPE_RGB);
	}

fimg_printhead(&src);
fimg_clone(&src, &dst, 1);

foo = fimg_prototype_plasma(&dst, fkoef, 0);
if (foo)  {
	fprintf(stderr, "%s: err %d on plasma proto\n", __func__, foo);
	return -88;
	}

fimg_mul_3(&src, &dst, &dst);

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

return -1;
}
/* --------------------------------------------------------------------- */
/* nouveau 24 octobre 2020, pendant le masque-flamme coronavidique       */

int essai_displacement(char *infile, char *outfile)
{
int		foo;
FloatImg	src, dst;

fprintf(stderr, "%s : loading %s\n", __func__, infile);
foo = fimg_create_from_dump(infile, &src);
if (foo) {
	fprintf(stderr, "%s: error loading '%s'\n", __func__, infile);
	return foo;
	}

fimg_clone(&src, &dst, 1);

foo = fimg_displacement_0(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s: err %d in disp map 0\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *	nouveau 7 octobre 2020 pendant sonoptic
 *
 *	inspiration:  Olivier Baudu
 */

int essai_qsort_rgb(char *infile, char *outfile)
{
FloatImg	src, dst;
int		foo;

if (NULL != infile) {
	fprintf(stderr, "%s : loading %s\n", __func__, infile);
	foo = fimg_create_from_dump(infile, &src);
	if (foo) {
		fprintf(stderr, "%s: error loading '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s : NOT INPUT FILE, FUBAR\n", __func__);
	abort();
	}

fimg_clone(&src, &dst, 1);

foo = fimg_qsort_rgb_b(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s: err %d in qsort_rgb\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

fimg_destroy(&src);	fimg_destroy(&dst);

return 0;
}

/* --------------------------------------------------------------------- */
/*
 *	nouveau 5 octobre 2020 pendant sonoptic
 */

int essai_contour_2x2(char *infile, char *outfile)
{
FloatImg	src, dst;
int		foo;

if (NULL != infile) {
	fprintf(stderr, "%s : loading %s\n", __func__, infile);
	foo = fimg_create_from_dump(infile, &src);
	if (foo) {
		fprintf(stderr, "%s: error loading '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s : NOT INPUT FILE, FUBAR\n", __func__);
	abort();
	}

fimg_clone(&src, &dst, 1);

foo = fimg_contour_2x2(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s: err %d in contour_2x2\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

fimg_destroy(&src);	fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *	nouveau 5 octobre 2020 pendant sonoptic
 */
int essai_classif(char *infile, char *outfile, float fvalue)
{
FloatImg	src, dst;
int		foo;

if (NULL != infile) {
	fprintf(stderr, "%s : loading %s\n", __func__, infile);
	foo = fimg_create_from_dump(infile, &src);
	if (foo) {
		fprintf(stderr, "%s: error loading '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s : NOT INPUT FILE, FUBAR\n", __func__);
	abort();
	}

fimg_clone(&src, &dst, 1);

fprintf(stderr, "%s : fvalue is %f\n", __func__, fvalue);

foo = fimg_classif_trial(&src, &dst, fvalue, 0);
if (foo) {
	fprintf(stderr, "%s: err %d in classif_trial\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result\n", __func__, foo);
	return foo;
	}

fimg_destroy(&src);	fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */
/* nouveau 19 aout 2020, le matin avant la canicule */

int essai_ecriture_tiff(char *outname)
{
int		foo;
FloatImg	picz;

fimg_create(&picz, 800, 600, FIMG_TYPE_RGB);
fimg_test_pattern(&picz, 0, 22222);

foo = fimg_write_as_tiff(&picz, outname, 0);
if (foo) {
	fprintf(stderr, "%s got a %d\n", __func__, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */
/*	essai de fichiers FITS (astronomie)				*/
int essai_ecriture_fits(char *outname)
{
FloatImg	src;
int		foo;

fprintf(stderr, "%s is creating the picz\n", __func__);
fimg_create(&src, 512, 512, FIMG_TYPE_RGB);
fimg_test_pattern(&src, 0, 255.0);

foo = fimg_save_R_as_fits(&src, outname, 0);

fprintf(stderr, "saving '%s' to fits --> %d\n", outname, foo);

return -1;
}
/* --------------------------------------------------------------------- */
/*
 *	egalisation dynamique approximative
 *	#coronamaison  Thu 09 Apr 2020 03:37:10 PM CEST
 */
int essai_equalize(char *infile)
{
FloatImg	src;
int		foo;

if (NULL != infile) {
	fprintf(stderr, "%s: loading %s\n", __func__, infile);
	foo = fimg_create_from_dump(infile, &src);
	if (foo) {
		fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s : NOT INPUT FILE, FUBAR\n", __func__);
	abort();
	}

/*
 *		XXX need more work on this function !
 */
foo = fimg_equalize_compute(&src, NULL, 666.666);
fprintf(stderr, "equalize compute --> %d\n", foo);

fimg_destroy(&src);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_rotate(char *infile)
{
FloatImg	src, dst;
int		foo;

if (NULL != infile) {
	fprintf(stderr, "%s: loading %s\n", __func__, infile);
	foo = fimg_create_from_dump(infile, &src);
	if (foo) {
		fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s : NOT INPUT FILE, FUBAR\n", __func__);
	abort();
	}

fimg_save_as_png(&src, "test.png", 0);

foo = fimg_rotate_90(&src, &dst, 0);
fprintf(stderr, "rotate 90 -> %d\n", foo);

foo = fimg_export_picture(&dst, "rotated90.png", 0);
foo = fimg_export_picture(&dst, "rotated90.pnm", 0);

fimg_destroy(&src);

return -1;
}
/* --------------------------------------------------------------------- */
int essai_filtrage_3x3(char *infile)
{
FloatImg	src, dst;
int		foo;			/// , idx;
// char		buffer[100];

FimgFilter3x3	filter_a =  {

	{	1.0,	1.0,	1.0,
		1.0,   -3.0,	1.0,
		1.0,	1.0,	1.0 },
		9.0,	0.0

	};

FimgFilter3x3	filter_b =  {

	{	-2.0,	-1.0,	0.0,
		-1.0,    3.0,	1.0,
		0.0,	 1.0,	2.0 },
		8.0,	0.0

	};

FimgFilter3x3	filter_c = {
		{
		2.0,	1.0,	0.0,
		1.0,	0.0,	-1.0,
		0.0,	-1.0,	-2.0,
		},
	1.0,	8.0
	};

if (NULL != infile) {
	fprintf(stderr, "%s: loading %s\n", __func__, infile);
	foo = fimg_create_from_dump(infile, &src);
	if (foo) {
		fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s is creating the picz\n", __func__);
	fimg_create(&src, 640, 480, FIMG_TYPE_RGB);
	fimg_test_pattern(&src, 0, 255.0);
	}

// fimg_save_as_png(&src, "test.png", 0);
foo = fimg_count_negativ(&src);
fprintf(stderr, "%s: source have %d negs\n", __func__, foo);

foo = fimg_clone(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "%s: err clone %p\n", __func__, &src);
	return -44;
	}

fimg_filter_3x3(&src, &dst, &filter_a);
foo = fimg_clamp_negativ(&dst);
if (foo) {
	fprintf(stderr, "A clamped %d negative pixels\n", foo);
	}
foo = fimg_save_as_png(&dst, "f3x3a.png", 0);
// foo = fimg_save_as_pnm(&dst, "f3x3a.pnm", 0);

fimg_filter_3x3(&src, &dst, &filter_b);
foo = fimg_clamp_negativ(&dst);
if (foo) {
	fprintf(stderr, "B clamped %d negative pixels\n", foo);
	}
foo = fimg_save_as_png(&dst, "f3x3b.png", 0);
// foo = fimg_save_as_pnm(&dst, "f3x3a.pnm", 0);

fimg_filter_3x3(&src, &dst, &filter_c);
foo = fimg_clamp_negativ(&dst);
if (foo) {
	fprintf(stderr, "C clamped %d negative pixels\n", foo);
	}
foo = fimg_save_as_png(&dst, "f3x3b.png", 0);
// foo = fimg_save_as_pnm(&dst, "f3x3a.pnm", 0);

fimg_destroy(&src);	fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_filtrage_2x2(char *infile)
{
FloatImg	fimg;
int		foo, idx;
char		buffer[100];

if (NULL != infile) {
	fprintf(stderr, "%s: loading %s\n", __func__, infile);
	foo = fimg_create_from_dump(infile, &fimg);
	if (foo) {
		fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fprintf(stderr, "%s is creating the picz\n", __func__);
	fimg_create(&fimg, 512, 512, FIMG_TYPE_RGB);
	fimg_draw_something(&fimg);
	}

foo = fimg_save_as_pnm(&fimg, "source.pnm", 0);

/*
 *	running multiple filters so you can
 *	watch the up-left shift :)
 */
for (idx=0; idx<5; idx++) {
	foo = fimg_lissage_2x2(&fimg);
	sprintf(buffer, "filter%03d.png", idx);
	foo = fimg_save_as_png(&fimg, buffer, 0);
	if (verbosity) {
		fprintf(stderr, "%s %d\n", buffer, foo);
		}
	}

fimg_destroy(&fimg);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_geometrie(char *infile, int notused)
{
FloatImg	fimg, result;
int		foo;

if (0 != notused) {
	fprintf(stderr, "in %s, k must be 0, was %d\n", __func__, notused);
	}

if (NULL != infile) {
	fprintf(stderr, "loading %s\n", infile);
	foo = fimg_create_from_dump(infile, &fimg);
	if (foo) {
		fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fimg_create(&fimg, 512, 512, FIMG_TYPE_RGB);
	fimg_draw_something(&fimg);
	}

// foo = fimg_save_as_pnm(&fimg, "source.pnm", 0);

memset(&result, 0, sizeof(FloatImg));

foo = fimg_halfsize_0(&fimg, &result, 0);
fprintf(stderr, "retour halfsize 0 -> %d\n", foo);
if (foo) {
	return -2;
	}
if (verbosity) fimg_describe(&result, "result after halfsize 0");
foo = fimg_save_as_pnm(&result, "halfsize0.pnm", 0);

fimg_destroy(&result);
foo = fimg_halfsize_1(&fimg, &result, 0);
fprintf(stderr, "retour halfsize 1 -> %d\n", foo);
if (foo) {
	return -2;
	}
if (verbosity) fimg_describe(&result, "result after halfsize 1");
foo = fimg_save_as_pnm(&result, "halfsize1.pnm", 0);

/* hop, un peu de nettoyage */
fimg_destroy(&result);	fimg_destroy(&fimg);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_sfx0(char *infile)
{
FloatImg	fimg;
int		foo;

if (NULL != infile) {
	fprintf(stderr, "loading %s\n", infile);
	foo = fimg_create_from_dump(infile, &fimg);
	if (foo) {
		fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
		return foo;
		}
	}
else	{
	fimg_create(&fimg, 512, 512, FIMG_TYPE_RGB);
	fimg_draw_something(&fimg);
	}

foo = fimg_save_as_pnm(&fimg, "something.pnm", 0);
if (foo) {
	fprintf(stderr, "%s: err save %d\n", __func__, foo);
	return -6;
	}
foo = fimg_killcolors_a(&fimg, 0.0);
foo = fimg_save_as_pnm(&fimg, "colorskilled-a.pnm", 0);
if (foo) {
	fprintf(stderr, "%s: err save %d\n", __func__, foo);
	return -6;
	}

foo = fimg_killcolors_b(&fimg, 0.0);
foo = fimg_save_as_pnm(&fimg, "colorskilled-b.pnm", 0);
if (foo) {
	fprintf(stderr, "%s: err save %d\n", __func__, foo);
	return -6;
	}

fimg_destroy(&fimg);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_parse_double(void)
{
int		foo;
double		dval;
char		*str;

str = "12.34";	dval = 0.0;
foo = parse_double(str, &dval);
printf("%-10s -> %3d   %g\n", str, foo, dval);

str = "12e4";	dval = 0.0;
foo = parse_double(str, &dval);
printf("%-10s -> %3d   %g\n", str, foo, dval);

str = "5s";	dval = 0.0;
foo = parse_double(str, &dval);
printf("%-10s -> %3d   %g\n", str, foo, dval);

str = "PORN";	dval = 0.0;
foo = parse_double(str, &dval);
printf("%-10s -> %3d   %g\n", str, foo, dval);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_detect_type(void)
{
int		foo;
char		*fname;

foo = format_from_extension(fname="foo.fimg");
printf("%-10s %3d\n", fname, foo);

foo = format_from_extension(fname="foo.pnm");
printf("%-10s %3d\n", fname, foo);

foo = format_from_extension(fname="foo.png");
printf("%-10s %3d\n", fname, foo);

foo = format_from_extension(fname="foo.tiff");
printf("%-10s %3d\n", fname, foo);

foo = format_from_extension(fname="foo.fits");
printf("%-10s %3d\n", fname, foo);

foo = format_from_extension(fname="foo.xyzzy");
printf("%-10s %3d\n", fname, foo);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_mire(char *outname, int notused)
{
FloatImg	fimg;
int		re, foo;

if (0 != notused) {
	fprintf(stderr, "in %s, k must be 0, was %d\n", __func__, notused);
	}

fimg_create(&fimg, 1280, 960, FIMG_TYPE_RGB);

re = fimg_test_pattern(&fimg, 9, 1.0);
if (re) {
	fprintf(stderr, "fimg_test_pattern -> %d\n", re);
	}
foo = fimg_export_picture(&fimg, outname, 0);
fprintf(stderr, "in %s, export give a %d value\n", __func__, foo);

return 0;
}
/* --------------------------------------------------------------------- */
int essai_rampes(void)
{
FloatImg	fimg;
int		foo;

fimg_create(&fimg, 640, 480, FIMG_TYPE_RGB);

#define V ((double)3.141592654)

foo = fimg_hdeg_a(&fimg,  V);
fprintf(stderr, "make h deg -> %d\n", foo);
foo = fimg_save_as_pnm(&fimg, "hdeg.pnm", 0);
fprintf(stderr, "%s: save as pnm -> %d\n", __func__, foo);


foo = fimg_vdeg_a(&fimg,  V);
fprintf(stderr, "make h deg -> %d\n", foo);
foo = fimg_save_as_pnm(&fimg, "vdeg_a.pnm", 0);
fprintf(stderr, "%s: save as pnm -> %d\n", __func__, foo);

foo = fimg_vdeg_a(&fimg, -V);
fprintf(stderr, "make h deg -> %d\n", foo);
foo = fimg_save_as_pnm(&fimg, "vdeg_b.pnm", 0);
fprintf(stderr, "%s: save as pnm -> %d\n", __func__, foo);

#undef V

return 0;
}
/* --------------------------------------------------------------------- */
int essai_lecture_png(char *fname, char *outfile, int notused)
{
FloatImg	fimg;
int		foo;

fprintf(stderr, ">>> %s ( '%s' %d  )\n", __func__, fname, notused);

memset(&fimg, 0, sizeof(FloatImg));
foo = fimg_create_from_png(fname, &fimg);
if (foo) {
	fprintf(stderr, "%s: createfrom -> %d\n", __func__, foo);
	return foo;
	}
fimg_describe(&fimg, "created from png");

foo = fimg_export_picture(&fimg, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result to %s\n", __func__,
				foo, outfile);
	return foo;
	}
return 0;
}
/* --------------------------------------------------------------------- */
int essai_ecriture_png(char *fname)
{
FloatImg	fimg;
int		foo;

fimg_create(&fimg, 800, 600, FIMG_TYPE_RGB);

fimg_draw_something(&fimg);

if (verbosity) {
	foo = fimg_save_as_pnm(&fimg, "quux.pnm", 0);
	fprintf(stderr, "%s: saved as pnm -> %d\n", __func__, foo);
	}

foo = fimg_save_as_png(&fimg, fname, 0);
fprintf(stderr, "save as png -> %d\n", foo);

fimg_destroy(&fimg);

return 0;
}
/* --------------------------------------------------------------------- */

int fimg_essai_hsv(char *fname);			/* hsv.c */


int essai_histogramme(char *fname, int k)
{
FloatImg	fimg;
int		foo;

fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, fname, k);

foo = fimg_create_from_dump(fname, &fimg);
if (foo) {
	fprintf(stderr, "%s: err load '%s'\n", __func__, fname);
	return foo;
	}

foo = fimg_essai_histo(&fimg, "out.png", k);
if (foo) {
	fprintf(stderr, "essai_histo -> error %d\n", foo);
	return foo;
	}

fimg_destroy(&fimg);

fprintf(stderr, "\\o/ end of %s\n", __func__);

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *	dans la roulotte de terreblanque
 */
int essai_pixelize(char *infile, char *outfile)
{
FloatImg	src, dst;
int		foo;

fprintf(stderr, ">>> %s ( '%s' '%s' )\n", __func__, infile, outfile);

memset(&src, 0, sizeof(FloatImg));
foo = fimg_create_from_dump(infile, &src);
if (foo) {
	fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
	return foo;
	}

memset(&dst, 0, sizeof(FloatImg));
foo = fimg_clone(&src, &dst, 0);
if (foo) return -888;

foo = fimg_pixelize_h_0(&src, &dst, 8);
if (foo) {
	fprintf(stderr, "in %s, pixelize give us a %d\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result to %s\n", __func__,
				foo, outfile);
	return foo;
	}

fimg_destroy(&src);
fimg_destroy(&dst);

return 0;
}
/* --------------------------------------------------------------------- */
/* dans ma nouvelle maison du Gers, le 20 mars 2024 */
int essai_thermocol(char *infile, char *outfile)
{
FloatImg	src, dst;
int		foo;

fprintf(stderr, ">>> %s ( '%s' '%s' )\n", __func__, infile, outfile);

memset(&src, 0, sizeof(FloatImg));
foo = fimg_create_from_dump(infile, &src);
if (foo) {
	fprintf(stderr, "%s: err load '%s'\n", __func__, infile);
	return foo;
	}

memset(&dst, 0, sizeof(FloatImg));
foo = fimg_clone(&src, &dst, 0);
if (foo) return -888;

foo = fimg_auto_thermique(&src, &dst, 0);

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "%s : err %d saving result to %s\n", __func__,
				foo, outfile);
	return foo;
	}

fimg_destroy(&src);
fimg_destroy(&dst);

return 793;
}
/* --------------------------------------------------------------------- */
