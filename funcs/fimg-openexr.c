/*
 *		Lecture/ecriture des images EXR
 *		-------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include <string.h>

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
int fimg_save_as_exr(FloatImg *src, char *outname, int flags)
{
// #if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' 0x%X )\n", __func__, src, outname, flags);
// #endif

if (FIMG_TYPE_RGB != src->type) {
	fprintf(stderr, "%s: src bad type %d\n", __func__, src->type);
	return -2;
	}



return -2;
}
/* --------------------------------------------------------------------- */
