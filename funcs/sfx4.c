/*
 *		FLOATIMG - a kluge from tTh
 *		---------------------------
 *
 *	some strange effects on floating pictures.
 */


#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

extern int		verbosity;

/* -------------------------------------------------------------- */
int fimg_sfx_triplemul(FloatImg *src, FloatImg *dst, int notused)
{
int		x, y, foo;
float		in[3], out[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p 0x%04x )\n", __func__, src, dst, notused);
#endif

if (notused) {
	fprintf(stderr, "notused was %d, must be 0 in %s\n",
				notused, 		__func__);
	}

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		foo =  fimg_get_rgb(src, x, y, in);
		if (foo)	return foo;
		out[0] = in[1] * in[2];
		out[1] = in[0] * in[2];
		out[2] = in[0] * in[1];
		// fprintf(stderr, "%9f %9f %9f\n", out[0], out[1], out[2]);
		foo =  fimg_put_rgb(dst, x, y, out);
		if (foo)	return foo;
		}
	}

return 0;
}
/* -------------------------------------------------------------- */
/*
 * see also:
	sfx3.c:fimg_crump_hard()
*/
int fimg_split_level(FloatImg *src, FloatImg *dst, int notused)
{
float		means[4];
// float		in[3];
int		foo, idx, surface;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p 0x%04x )\n", __func__, src, dst, notused);
#endif

if (notused) {
	fprintf(stderr, "notused was %d, must be 0 in %s\n",
				notused, 		__func__);
	}

foo = fimg_meanvalues(src, means);
if (foo) {
	return -66;
	}

surface = src->width*src->height;
for(idx=0; idx<surface; idx++) {
	if (src->R[idx]<means[0])   dst->R[idx]=src->R[idx]*2.0;
	else     dst->R[idx]=(src->R[idx]-means[0])*2.0;
	if (src->G[idx]<means[1])   dst->G[idx]=src->G[idx]*2.0;
	else     dst->G[idx]=(src->G[idx]-means[1])*2.0;
	if (src->B[idx]<means[2])   dst->B[idx]=src->B[idx]*2.0;
	else     dst->B[idx]=(src->B[idx]-means[2])*2.0;
	}

return 0;
}
/* -------------------------------------------------------------- */
int fimg_make_triptyq(FloatImg *src, FloatImg *dst, int notused)
{
int		x, y, sh, foo, ow;
float		r, g, b, in[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p 0x%04x )\n", __func__, src, dst, notused);
#endif

if (fimg_images_not_compatible(src, dst)) {
	fprintf(stderr, "%s : compatibility error\n", __func__);
	return -8;
	}

fimg_clear(dst);

ow = src->width / 3;
for (y=0; y<src->height; y++) {
	for (x=0; x<src->width/3; x++) {
		r = g = b = 0.0;
		for (sh=0; sh<3; sh++) {
			foo =  fimg_get_rgb(src, (x*3)+sh, y, in);
			r+=in[0], g+=in[1], b+=in[2];
			}
		for (sh=0; sh<3; sh++) {
			fimg_plot_rgb(dst, x,        y,   r, 0, 0);
			fimg_plot_rgb(dst, x+ow,     y,   0, g, 0);
			fimg_plot_rgb(dst, x+(2*ow), y,   0, 0, b);
			}
		}
	}

return 0;
}
/* -------------------------------------------------------------- */
