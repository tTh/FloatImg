/*
 *		KILL RGB !
 *
 *	nouveau TerreBlanque 4 octobre 2021
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <math.h>

#include  "../floatimg.h"
#include  "tests.h"

/* --------------------------------------------------------------------- */

int fimg_killrgb_v(FloatImg *src, FloatImg *dst, int k)
{
int		line, col;
int		ir;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %p  %d )\n", __func__, src, dst, k);
#endif

if (k) { fprintf(stderr, "in %s k was %d\n", __func__, k); }

fimg_clear(dst);
ir = 0;

for (line=0; line<src->height; line++) {
	// fprintf(stderr, "%s line %d\n", __func__, line);
	for (col=0; col<(src->width-3); col+=3) {

		dst->R[ir  ] = src->R[ir];	ir++; 
		dst->G[ir+1] = src->G[ir];	ir++; 
		dst->B[ir+2] = src->B[ir];	ir++; 

		}
	}

return 0;
}

/* --------------------------------------------------------------------- */
