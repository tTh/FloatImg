/*
 *		FLOATIMG
 *	import/export to/from FITS files 

   https://heasarc.gsfc.nasa.gov/docs/software/fitsio/c/c_user/node1.html

 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  <fitsio.h>

#include  "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
int fimg_save_plane_as_fits(FloatImg *src, char *outname,
						char plane, int flags)
{
fitsfile	*fptr;       		/* pointer to the FITS file */
int		status, sz;
int		bitpix = FLOAT_IMG;
float		*pplane;
long		naxis = 2;
long		naxes[2];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' %d )\n", __func__, src, outname, flags);
#endif

if (flags) { fprintf(stderr, "flags: 0x%04x in %s\n", flags, __func__); }

status = 0;

switch (plane) {
	case 'r':	case 'R':
		pplane = src->R;		break;
	case 'g':	case 'G':
		pplane = src->G;		break;
	case 'b':	case 'B':
		pplane = src->B;		break;
	default:
		return -66;
	}

remove(outname);               /* Delete old file if it already exists */
if (fits_create_file(&fptr, outname, &status)) {
	fits_report_error(stderr, status);
	return -9;
	}

naxes[0] = src->width;		naxes[1] = src->height;
if (verbosity > 1) fimg_describe(src, "to be saved as FITS");

if ( fits_create_img(fptr,  bitpix, naxis, naxes, &status) ) {
	fits_report_error(stderr, status);
	return -10;
	}

sz = naxes[0]*naxes[1];
if ( fits_write_img(fptr, TFLOAT, 1, sz, pplane, &status) ) {
	fits_report_error(stderr, status);
	return -10;
	}

if ( fits_close_file(fptr, &status) ) {
	fits_report_error(stderr, status);
	return -9;
	}

return 0;
}
/* --------------------------------------------------------------------- */
int fimg_save_R_as_fits(FloatImg *src, char *outname, int flags)
{
int		retv;
retv = fimg_save_plane_as_fits(src, outname, 'r', flags);
return retv;
}
/* --------------------------------------------------------------------- */
int fimg_save_G_as_fits(FloatImg *src, char *outname, int flags)
{
int		retv;
retv = fimg_save_plane_as_fits(src, outname, 'g', flags);
return retv;
}
/* --------------------------------------------------------------------- */
int fimg_save_B_as_fits(FloatImg *src, char *outname, int flags)
{
int		retv;
retv = fimg_save_plane_as_fits(src, outname, 'b', flags);
return retv;
}

/************************************************************
 *****    MAGIC CODE FROM OUTERSPACE ?

	function 'writeimage' from :
https://heasarc.gsfc.nasa.gov/docs/software/fitsio/cexamples/cookbook.c

float		**array;
array = calloc(src->height, sizeof(float *));
array[0] = src->R;
#define REVERSE 1
for( idx=0; idx<naxes[1]; idx++ ) {
#if REVERSE
	k = naxes[1] - idx - 1;
#else
	k = idx;
#endif
	array[idx] = src->R + (k*naxes[0]);
	fprintf(stderr, " %6d   %6d   %p\n", idx, k, array[idx]);
	}
**************************************************************/
