/*
 *		FLOATIMG
 *	rampes diverses, trucs etranges 
 */

#include  <stdio.h>
#include  <stdint.h>
#include  <stdlib.h>
#include  <sys/time.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */
int fimg_hdeg_a(FloatImg *img, double dcoef)
{
int		x, y;
float		value;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %f )\n", __func__, img, dcoef);
#endif

if (FIMG_TYPE_RGB != img->type) {
	fprintf(stderr, "%s bad type\n", __func__);
	return -6;
	}
for (x=0; x<img->width; x++) {
	value = (float)x / (float)img->width;
	value *= dcoef;
	for (y=0; y<img->height; y++) {
		fimg_plot_rgb(img, x, y, value, value, value);
		}
	}

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *    To have the black at the bottom, use a negative dcoef
 */
int fimg_vdeg_a(FloatImg *img, double dcoef)
{
int		x, y;
float		value;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %f )\n", __func__, img, dcoef);
#endif

if (FIMG_TYPE_RGB != img->type) {
	fprintf(stderr, "%s bad type\n", __func__);
	return -6;
	}
for (x=0; x<img->width; x++) {
	value = (float)x / (float)img->width;
	value *= dcoef;
	for (y=0; y<img->height; y++) {
		fimg_plot_rgb(img, x, y, value, value, value);
		}
	}

return 0;
}
/* --------------------------------------------------------------------- */
/*				nouveau 19 septembre 2022
 *
 *	6 octobre 2023 : rajout du parametre 'fmax'
 */

int fimg_do_stripes(FloatImg *img, float fmax, int mode)
{
int		x, y, quad;
float		*ligne;
float		fr, fg, fb;

fprintf(stderr, ">>> %s ( %p %f %d )\n", __func__, img, fmax, mode);

/*
 *	allocate and fill a lookup table
 */
if (NULL==(ligne=malloc(img->width*sizeof(float)))) {
	fprintf(stderr, "%s: malloc fail\n", __func__);
	exit(1);
	}
for (x=0; x<img->width; x++) {
	ligne[x] = (float)x / (float)img->width;
	ligne[x] *= fmax;
	}

/*
 *	build the pixels
 */
for (y=0; y<img->height; y++)
	{
	quad = (y*4) / img->height ;
	for (x=0; x<img->width; x++) {
		fr = fg = fb = ligne[x];
		switch(quad) {
			case 0:		fg = fb = 0;		break;
			case 1:		fr = fb = 0;		break;
			case 2:		fr = fg = 0;		break;
			case 3:					break;
			default:	abort();		break;
			}
		fimg_plot_rgb(img, x, y, fr, fg, fb);
		}
	}

free(ligne);

return 0;
}
/* --------------------------------------------------------------------- */
