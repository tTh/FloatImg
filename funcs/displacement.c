/*
 *		displacement.c
 */

#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include <string.h>
#include   <math.h>

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/* nouveau 24 octobre 2020, pendant le masque-flamme coronavidique       */

int fimg_displacement_0(FloatImg *psrc, FloatImg *pdst, int flags)
{
int		x, y, foo;
float		minmax[6];
float		rgb[3];
float		dltr, dltg, dltb;	/* delta des minmax */
float		dispx, dispy;
int		dstx, dsty;
int		in, out;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p 0x%04x )\n", __func__, psrc, pdst, flags);
#endif

if (FIMG_TYPE_RGB != psrc->type) {
	fprintf(stderr, "%s: bad src type %d\n", __func__, psrc->type);
	return -7;
	}
if (fimg_images_not_compatible(psrc, pdst)) {
	fprintf(stderr, "%s: bad dst type %d\n", __func__, pdst->type);
	return -8;
	}

foo = fimg_get_minmax_rgb(psrc, minmax);
if (foo) {
	fprintf(stderr, "%s : err %d on get minmax\n", __func__, foo);
	return foo;
	}
if (verbosity) {
	fimg_print_minmax(minmax, (char *)__func__);
	}
// dltr = minmax[1] - minmax[0];
dltg = minmax[3] - minmax[2];
dltb = minmax[5] - minmax[4];

in = out = 0;
/*
 *		BE WARNED !
 *	This code doesn't work as expected, so I have to
 *	rewrite it, maybe when the pandemic is closed...
 */
for (y=0; y<psrc->height; y++) {

	for (x=0; x<psrc->width; x++) {

		fimg_get_rgb(psrc, x, y, rgb);

		dispx = (float)x + (rgb[1]/dltg * 20.0);
		dispy = (float)y + (rgb[2]/dltb * 20.0);
		dstx = (int)roundf(dispx - 10.0);
		dsty = (int)roundf(dispy - 10.0);

		if (		(dstx < 0) || (dsty < 0)	||
				(dstx >= psrc->width)		||
				(dsty >= psrc->height)		)
			{
			/* OUT OF DESTINATION PICTURE */
			out++;
			}
		else	{
			if (flags & 1) {
				/* going monochrome */
				rgb[1] = rgb[2] = rgb[0];
				}
			fimg_put_rgb(pdst, dstx, dsty, rgb);
			in++;
			}
		}
	}

if (verbosity) fprintf(stderr, "%s -> in %d out %d\n", __func__, in, out);

return 0;
}
/* --------------------------------------------------------------------- */


