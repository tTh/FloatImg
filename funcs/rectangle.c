/*
 *			R E C T A N G L E
 *		This is an eternal WIP, sorry...
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <string.h>

#include  <math.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */
int fimg_clear_rectangle(FloatImg *pi, int coo[4])
{
int		line, off;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, pi, coo);
#endif

	/* please add boudary checks */

for (line=0; line<coo[3]; line++) {
	off = (line+coo[1])*pi->width + coo[0];
	// fprintf(stderr, "line %3d   off %8d\n", line, off);
	/* Kaboum ! */
	memset(pi->R + off, 0, coo[2]*sizeof(float));
	memset(pi->G + off, 0, coo[2]*sizeof(float));
	memset(pi->B + off, 0, coo[2]*sizeof(float));
	}

return -1;
}
/* --------------------------------------------------------------------- */

