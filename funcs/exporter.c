/*
 *		exporter.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <string.h>

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/*
 *	multi-magic 'save file' function.
 */
int fimg_export_picture(FloatImg *pic, char *fname, int flags)
{
int		filetype;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' 0x%X )\n", __func__,
				pic, fname, flags);
#endif

if (flags) { fprintf(stderr, "flags: 0x%04x in %s\n", flags, __func__); }

filetype = format_from_extension(fname);
if (verbosity > 1) {
	fprintf(stderr, "file %s have type %d\n", fname, filetype);
	}

switch(filetype) {
	case FILE_TYPE_FIMG:
		foo = fimg_dump_to_file(pic, fname, 0);
		break;
	case FILE_TYPE_PNM:
		foo = fimg_save_as_pnm(pic, fname, 0);
		break;
	case FILE_TYPE_PNG:
		foo = fimg_save_as_png(pic, fname, 0);
		break;
	case FILE_TYPE_TGA:
		fprintf(stderr, "%s: FILE_TYPE_TGA not implemented\n",
				__func__);
		foo = -666;
		break;
	case FILE_TYPE_TIFF:
		foo = fimg_write_as_tiff(pic, fname, 0);
		break;
	case FILE_TYPE_FITS:
		foo = fimg_save_R_as_fits(pic, fname, 0);
		break;
	case FILE_TYPE_BMP:
		fprintf(stderr, "%s: file type BMP not implemented\n", __func__);
		foo = -666;
		break;
	case FILE_TYPE_EXR:
		fprintf(stderr, "%s: file type EXR experimental\n", __func__);
		foo = fimg_save_as_exr(pic, fname, 0);
		break;
	case FILE_TYPE_PGM:
		fprintf(stderr, "XXX %s EXPERIMENT!\n", __func__);
		foo = fimg_save_as_pgm(pic, fname, 0);
		break;
	default:
		foo = -1789;
		break;
	}

if (foo) {
	fprintf(stderr, "%s: exporting '%s' -> %d\n", __func__,
					fname, foo);
	/* que faire maintenant ? */
	}

return foo;
}
/* --------------------------------------------------------------------- */

