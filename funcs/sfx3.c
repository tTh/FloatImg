/*
 *		FLOATIMG - a kluge from tTh
 *		---------------------------
 *
 *	some strange effects on floating pictures, made in
 *	the batcave of "Le Bib", in Montpellier.
 */


#include  <stdio.h>
#include  <string.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

extern int		verbosity;

/* -------------------------------------------------------------- */
/* new    Sun 29 Jan 2023 10:01:39 PM CET

*/
int fimg_make_rndfluffy_lines(FloatImg *src, FloatImg *dst, int rndt)
{
int		x, y, ol;
float		accu;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%p'  '%p'  %d )\n", __func__,
				src, dst, rndt);
#endif

if (fimg_images_not_compatible(src, dst)) {
	/* be hard for the lamers */
	fprintf(stderr, "compatibility OUPS in %s\n", __func__);
	abort();
	}

for (y=0; y<src->height; y++) {
	ol = y * src->width;
	if (rndt < (rand() % 100)) {
		accu = 0.0;
		for (x=0; x<src->width; x++)	accu += src->R[ol + x];
		accu /= (float)src->width;
		for (x=0; x<src->width; x++)	dst->R[ol + x] = accu;
		}
	else	{
		for (x=0; x<src->width; x++) {
			dst->R[ol+x] = src->R[ol+x];
			dst->G[ol+x] = src->G[ol+x];
			dst->B[ol+x] = src->B[ol+x];
			}
		}
	}

return 0;
}
/* -------------------------------------------------------------- */
int fimg_crump_hard(FloatImg *src, FloatImg *dst, float kval, int notused)
{
float		halfval;
float		rgb[3];
int		x, y, foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %f 0x%04x )\n", __func__,
				src, dst, kval, notused);
#endif

if (notused) {
	fprintf(stderr, "%s: notused was %d, must be 0\n",
			__func__, notused);
	}

halfval = fimg_get_maxvalue(src) / 2.0;
if (verbosity > 1) {
	fprintf(stderr, "%s: kval=%f & halfval=%f\n", __func__,
				kval,	halfval);
	}

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		foo =  fimg_get_rgb(src, x, y, rgb);
		if (foo)	return foo;

		if (rgb[0] > halfval)		rgb[0] /= 2.0;
		if (rgb[1] > halfval)		rgb[1] /= 2.0;
		if (rgb[2] > halfval)		rgb[2] /= 2.0;

		foo =  fimg_put_rgb(dst, x, y, rgb);
		if (foo)	return foo;
		}
	}

return 0;
}
/* -------------------------------------------------------------- */

