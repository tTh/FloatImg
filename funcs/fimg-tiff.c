/*
 *		FLOATIMG
 *	import/export to/from TIFF files 
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <tiffio.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */

extern int		verbosity;

/* --------------------------------------------------------------------- */
int fimg_write_as_tiff(FloatImg *src, char *fname, int flags)
{
TIFF			*tiff;
unsigned short		*linebuff, *ptr;
int			x, y, idx, foo;
char			ligne[100];
double			maximum, fk;

if (flags) fprintf(stderr, "in %s, flags are 0x%04x, must be 0\n",
				__func__,	flags);

		/* bon, tout cela semble bien tortueux ! */

if (FIMG_TYPE_RGB != src->type) {
	fprintf(stderr, "%s: src bad type %d\n", __func__, src->type);
	return -2;
	}

linebuff = calloc(src->width, 3*sizeof(unsigned short));
if (NULL==linebuff) {
	fprintf(stderr, "%s: fatal memory error\n", __func__);
	return -7;
	}

maximum = (double)fimg_get_maxvalue(src);
fk = maximum / 65535.0;
if (verbosity) {
	fprintf(stderr, "%s : maxv %f fk %f\n", __func__, maximum, fk);
	}

tiff = TIFFOpen(fname, "w");
if (NULL==tiff) {
	fprintf(stderr, "erreur TIFFOpen\n");
	free(linebuff);
	return -6;
	}

TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH,  src->width);
TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, src->height);

TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL,  3);	// RGB
TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE,   16);	// 0->65535

TIFFSetField(tiff, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG);
TIFFSetField(tiff, TIFFTAG_PHOTOMETRIC,  PHOTOMETRIC_RGB);

sprintf(ligne, "lib FloatImg v %d by tTh", FIMG_VERSION);
TIFFSetField(tiff, TIFFTAG_SOFTWARE, ligne);

foo = src->width * 3;
foo = TIFFDefaultStripSize(tiff, foo);
if (verbosity) fprintf(stderr, "default strip size %d\n", foo);

TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, foo);

for (y=0; y<src->height; y++) {
	ptr = linebuff;
	idx = y * src->width;

	for (x=0; x<src->width; x++) {
		*ptr++ = (unsigned short) (src->R[idx] / fk);
		*ptr++ = (unsigned short) (src->G[idx] / fk);
		*ptr++ = (unsigned short) (src->B[idx] / fk);
		idx++;
		}

	TIFFWriteScanline(tiff, linebuff, y, 0);
	idx += src->width;
	}

TIFFClose(tiff);

free(linebuff);

return 0;
}
/* --------------------------------------------------------------------- */
