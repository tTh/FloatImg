/*
  *			FLOATIMG
 *			--------

 *			F M O R P H O
 *
 * nouveau 30 septembre 2022  / integration 28 octobre 2022
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- !*/
static struct
	{
	int	x, y;
	} deltas[] =
{	{ -1,	-1, },
	{  0,	-1, },
	{  1,	-1, },
	{ -1,	 0, },
	{  0,	 0, },
	{  1,	 0, },
	{ -1,	 1, },
	{  0,	 1, },
	{  1,	 1 }
};

typedef struct
	{
	int		x, y;			// not used
	float		r, g, b;
	float		fgris;
	int		rang;
	} fpixel;

static fpixel	pixels[9];

/* --------------------------------------------------------------------- !*/
static int comparaison_fgris(const void *A, const void *B)
{
return ((fpixel *)A)->fgris > ((fpixel *)B)->fgris;
}
/* --------------------------------------------------------------------- !*/
/*
 *		this is a klugy approch, sorry.
 */
int fimg_filtre_morpho_0(FloatImg *sfimg, FloatImg *dfimg, int index)
{
int		xs, ys, loop9;
int		xp, yp;
float		rgb[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p   %d )\n", __func__, sfimg, dfimg, index);
#endif

if ( (index<0) || (index>8)) {
	fprintf(stderr, " %s: bad index %d\n", __func__, index);
#if MUST_ABORT
	fflush(stderr);		abort();
#endif
	return -1;
	}

fimg_clear(dfimg);

for (ys=1; ys<sfimg->height-1; ys++) {
	for (xs=1; xs<sfimg->width-1; xs++) {
		for (loop9=0; loop9<9; loop9++) {
			xp = xs + deltas[loop9].x;
			yp = ys + deltas[loop9].y;
			fimg_get_rgb(sfimg, xp, yp, rgb);
			pixels[loop9].fgris = rgb[0] + rgb[1] + rgb[2];
			pixels[loop9].rang  = loop9;
			}
		qsort(&pixels, 9, sizeof(fpixel), comparaison_fgris);
		rgb[0] = rgb[1] = rgb[2] = pixels[index].fgris;
		fimg_put_rgb(dfimg, xs, ys, rgb);
		}
	}

return 0;
}
/* --------------------------------------------------------------------- !*/
