/*
 *		FLOATIMG
 *	egalisation dynamique approximative
 *	#coronamaison  Thu 09 Apr 2020 03:37:10 PM CEST
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <sys/time.h>

#include  "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/*				new func: Wed 14 Sep 2022 11:28:04 AM CEST
*/
int fimg_equalize(FloatImg *src, double vmax)
{
float			mm[6];
double			maxi, coef;
int			foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p  %f )\n", __func__, src, vmax);
#endif

memset(mm, 0, 6*sizeof(float));
foo = fimg_get_minmax_rgb(src, mm);
if (foo) {
	fprintf(stderr, "%s: err %d get_minmax\n", __func__, foo);
	return foo;
	}
maxi = mm[1] > mm[3] ? (double)mm[1] : (double)mm[3];
maxi = maxi  > mm[5] ?         maxi  : (double)mm[5];
coef = vmax / maxi;

if (verbosity) {
	fprintf(stderr, "maximums  %.3f %.3f %.3f    %.3f\n",
				mm[1], mm[3], mm[5], maxi);
	fprintf(stderr, "vmax %f maxi %f multcoef = %g\n", vmax, maxi, coef);
	}

foo = fimg_mul_cste(src, (float)coef);
if (foo) {
	fprintf(stderr, "%s: err %d mul_cste\n", __func__, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *
 *	- wtf is this "void *vptr" thing ?
 */
int fimg_equalize_compute(FloatImg *src, void *vptr, float vmax)
{
float			minmax[6];
int			foo;
float			dr, dg, db;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %f )\n", __func__, src, vptr, vmax);
#endif

memset(minmax, 0, 6*sizeof(float));

foo = fimg_get_minmax_rgb(src, minmax);
if (foo) {
	fprintf(stderr, "err %d get minmax in %s\n", foo, __func__);
	return foo;
	}

fprintf(stderr, "vptr is %p   vmax is %f\n", vptr, vmax);

dr = minmax[1] - minmax[0];
dg = minmax[3] - minmax[2];
db = minmax[5] - minmax[4];

printf("Rmin  %12.4g   max  %12.4g  delta  %12.4g\n", minmax[0], minmax[1], dr);
printf("Gmin  %12.4g   max  %12.4g  delta  %12.4g\n", minmax[2], minmax[3], dg);
printf("Bmin  %12.4g   max  %12.4g  delta  %12.4g\n", minmax[4], minmax[5], db);

if ( (minmax[0]<0.0) || (minmax[2]<0.0) || (minmax[4]<0.0) ) {
	fprintf(stderr, "%s: negative value ?\n", __func__);
	return -4;
	}

fprintf(stderr, "deltas  %12.4g  %12.4g  %12.4g\n", dr, dg, db);

return 0;
}
/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */

