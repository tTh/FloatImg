/*
 *	tests des fonctions diverses - main file
	see also: tests.c
 */

#include <stdio.h>
#include <stdlib.h>
#include  <stdint.h>
#include <string.h>
#include  <pam.h>

#undef 		DEBUG_LEVEL
#define		DEBUG_LEVEL	1

#include "../floatimg.h"
#include  "tests.h"

int		verbosity;
float		global_fvalue;

/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
enum nCmd { Equalize=1, Rotate, Sfx0, F3x3, MIRE, Wfits, Wpng, Wtiff,
			Histo, Hsv, Classif, Ctr2x2, Qsortrgb,
			Displace, ReadPNG, Plasmas, Hilight, OpenEXR,
			Geometrie, FileType, Mirror, KillRGB,
			Pixelize,SplitLevel, DecompRgbz, DecompRgbg,
			Rectangle, Dicom, Fakolor0, Fakolor3,
			Fmorpho0, Fluffy, ThermoCol, TriPtyq  };
typedef struct {
	char	*name;
	int	Cmd;
	} Command;

Command commands[] = {
	{ "equalize",	Equalize	},
	{ "rotate",	Rotate		},
	{ "sfx0",	Sfx0		},
	{ "f3x3",	F3x3		},
	{ "mire",	MIRE		},
	{ "wfits",	Wfits		},
	{ "wpng",	Wpng		},
	{ "wtiff",	Wtiff		},
	{ "histo",	Histo		},
	{ "hsv",	Hsv		},
	{ "classif",	Classif		},
	{ "ctr2x2",	Ctr2x2		},
	{ "qsortrgb",	Qsortrgb	},
	{ "displace",	Displace	},
	{ "readpng",	ReadPNG		},
	{ "plasma",	Plasmas		},
	{ "hilight",	Hilight		},
	{ "openexr",	OpenEXR		},
	{ "geometrie",	Geometrie,	},
	{ "filetype",	FileType	},
	{ "mirror",	Mirror		},
	{ "killrgb",	KillRGB		},
	{ "pixelize",	Pixelize	},
	{ "spltlvl",	SplitLevel	},
	{ "decomprgbz",	DecompRgbz	},
	{ "decomprgbg",	DecompRgbg	},
	{ "rectangle",  Rectangle	},
	{ "dicom",      Dicom		},
	{ "fakolor0",	Fakolor0	},
	{ "fakolor3",	Fakolor3	},
	{ "fmorpho0",	Fmorpho0	},
	{ "fluffy",	Fluffy		},
	{ "thermocol",	ThermoCol	},
	{ "triptyq",    TriPtyq		},
	{ NULL,		0		}
	} ;

/* --------------------------------------------------------------------- */
int lookup_cmd(char *cmdtxt)
{
Command *pcmd;

pcmd = commands;
while (pcmd->name) {
	if (!strcmp(pcmd->name, cmdtxt)) return pcmd->Cmd;
	pcmd++;
	}

return -1;
}
/* --------------------------------------------------------------------- */
void list_tests(void)
{
Command *pcmd = commands;
while (pcmd->name) {
	printf("%s\n", pcmd->name);
	pcmd++;
	}
exit(0);
}
/* --------------------------------------------------------------------- */
void help(int k)
{
Command *pcmd;

fprintf(stderr, "usage:\n\t./t [options] command [filename]\n");

fprintf(stderr, "options:\n");
fprintf(stderr, "\t-k 1.414\tset float value\n");
fprintf(stderr, "\t-l\t\tlist tests\n");
fprintf(stderr, "\t-o \t\toutfile\n");
fprintf(stderr, "\t-v \t\tincrease verbosity\n");

if (verbosity) {
	fprintf(stderr, "commands:\n");
	pcmd = commands;
	while (pcmd->name) {
		fprintf(stderr, "\t%-15s   %d\n", pcmd->name, pcmd->Cmd);
		pcmd++;
		}
	}

fprintf(stderr, "\ncompiled on "__DATE__" at "__TIME__"\n");

if (k)  fimg_print_version(k);

exit(0);
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;
char		*filename, *command, *outfile;

fprintf(stderr, "++++++++ test des fonctions pid=%d\n", getpid());
fprintf(stderr, "++++++++ compiled "__DATE__" at " __TIME__ "\n");

global_fvalue	= 1.0;
outfile		= "out.pnm";
command		= "none";
filename	= "in.fimg";

while ((opt = getopt(argc, argv, "hk:lo:p:v")) != -1) {
	// fprintf(stderr, "opt = %c\n", opt);
	switch(opt) {
		case 'h':	help(0);			break;
		case 'k':	global_fvalue = atof(optarg);	break;
		case 'l':	list_tests();			break;
		case 'o':	outfile = optarg;		break;
		case 'v':	verbosity++;			break;
		}
	}

// fprintf(stderr, "argc %d   optind %d\n", argc, optind);

switch (argc-optind) {
	case 1:			/* only command */
		command  = argv[optind];
		break;
	case 2:
		command  = argv[optind];
		filename = argv[optind+1];
		break;
	default:
		fprintf(stderr, "%s: bad command line ?\n", argv[0]);
		help(1);
		break;
	}

if (verbosity) {
	fprintf(stderr, "++++++++ %s : running command '%s' on '%s'\n",
				argv[0], command, filename);
	fprintf(stderr, "global fvalue : %f\n", global_fvalue);
	}

opt = lookup_cmd(command);
// fprintf(stderr, "lookup '%s' --> %d\n", command, opt);

switch(opt) {
	case Equalize:
		foo = essai_equalize(filename);		break;
	case Sfx0:
		foo = essai_sfx0(filename);		break;
	case F3x3:
		foo = essai_filtrage_3x3(filename);	break;
	case MIRE:
		foo = essai_mire(filename, 0);
		break;
	case Wfits:
		foo =  essai_ecriture_fits("out.fits");
		break;
	case Wpng:
		foo = essai_ecriture_png("out.png");
		break;
	case Wtiff:
		foo = essai_ecriture_tiff("out.tiff");
		break;
	case Histo:
		foo = essai_histogramme(filename, 98765);
		break;
	case Hsv:
		// not ready for primtime
		// foo = fimg_essai_hsv(filename);
		foo = 0;
		break;
	case Classif:
		foo = essai_classif(filename, outfile, global_fvalue);
		break;
	case Ctr2x2:
		foo = essai_contour_2x2(filename, outfile);
		break;
	case Qsortrgb:
		foo = essai_qsort_rgb(filename, outfile);
		break;
	case Displace:
		foo = essai_displacement(filename, outfile);
		break;
	case ReadPNG:
		// not ready for primetime		
		// foo = essai_lecture_png("in.png", outfile, 0);
		foo = 0;
		break;
	case Plasmas:
		foo = essai_plasma(filename, outfile, 1, global_fvalue);
		fprintf(stderr, "we are all plasmafields\n");
		break;
	case Rotate:
		fprintf(stderr, "rotate not implemented (%d)\n", rand());
		foo = 0;
		break;
	case Hilight:
		foo = essai_highlights(filename, outfile, 0, global_fvalue);
		break;
	case OpenEXR:
		foo = essai_openexr(filename, outfile, 0x55);
		break;
	case Geometrie:
		foo = essai_geometrie(filename, 0);
		break;
	case FileType:
		foo = essai_detect_type();
		break;
	case Mirror:
		foo = essai_miroir(filename, outfile, 0);
		break;
	case KillRGB:
		foo = essai_killrgb(filename, outfile);
		break;
	case Pixelize:
		foo = essai_pixelize(filename, outfile);
		break;
	case SplitLevel:
		foo = essai_split_level(filename, outfile, 0);
		break;
	case DecompRgbz:
		foo = essai_decomprgb_color(filename, outfile);
		break;
	case DecompRgbg:
		foo = essai_decomprgb_gray(filename, outfile);
		break;
	case Rectangle:
		essai_rectangle(outfile, 0);
		break;
	case Dicom:
		foo = essai_dicom(filename, outfile, 0);
		break;
	case Fakolor0:
		foo = essai_0_fausses_couleurs(outfile, 0);
		break;
	case Fmorpho0:
		foo = essai_fmorpho_0(filename, "/tmp/fmorpho", 0);
		break;
	case Fluffy:
		foo = essai_rndfluffy(filename, outfile, 0);
		break;
	case ThermoCol:
		foo = essai_thermocol(filename, outfile);
		fprintf(stderr, "retour thermocolor = %d\n", foo);
		break;
	case TriPtyq:
		foo = essai_triptyq(filename, outfile);
		fprintf(stderr, "retour triptyq = %d\n", foo);
		break;

	default:
		fprintf(stderr, "'%s' is a bad command\n", command);
		exit(1);

	}

if (foo) {
	fprintf(stderr, "******* essai --> %d\n\n", foo);
	}

fprintf(stderr, "++++++++++++ end of '%s' pid %d\n", command, getpid());
return 0;
}
/* --------------------------------------------------------------------- */
