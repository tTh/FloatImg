/*
			PLASMAS

	Inspiration Reep : https://blog.314r.net/2021/01/10/plasma/

*/

#include  <stdio.h>
#include  <math.h>
#include  <stdint.h>
#include  <sys/time.h>

#include "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/* --------------------------------------------------------------------- */
int fimg_prototype_plasma(FloatImg *img, double time, int type)
{
int		x, y;
float		rgb[3];
double		dx, dy;

fprintf(stderr, ">>> %s ( %p %.3f %d )\n", __func__,
				img, time, type);

for (y=0; y<img->height; y++) {
	dy = ((double)y/(double)img->height) - 0.5000;

	for (x=0; x<img->width; x++) {
		dx = ((double)x/(double)img->width) - 0.5000;

		rgb[0] = sin(dx*10 + time + dy) + 1.0;
		rgb[1] = sin(dx*12 + time + dy) + 1.0;
		rgb[2] = sin(dx*14 + time + dy) + 1.0;
		fimg_put_rgb(img, x, y, rgb);
		}
	}

return 0;
}
/* --------------------------------------------------------------------- */
