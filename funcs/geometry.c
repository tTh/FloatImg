/*
 *		FLOATIMG
 *	distorsions géométriques - coredumping ?
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <string.h>

#include  "../floatimg.h"

extern int		verbosity;

/* --------------------------------------------------------------------- */
/*
 *	really crude function, need more work...
 */
int fimg_halfsize_0(FloatImg *src, FloatImg *dst, int notused)
{
int		wd, hd;
int		foo, x, y;
float		pixel[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__,
			src, dst, notused);
#endif

if (notused)	fprintf(stderr, "notused %d in %s\n", notused, __func__);

			/* no magic check here ? */
if (dst->width || dst->height) {
	fprintf(stderr, "*** %s: image at %p not empty\n", __func__, dst);
	fimg_describe(dst, "destination halfsize 0");
	return -2;
	}

wd = src->width / 2;	hd = src->height / 2;
foo = fimg_create(dst, wd, hd, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s: err create %d\n", __func__, foo);
	return -3;
	}

for (y=0; y<hd; y++) {
	for (x=0; x<wd; x++) {
		foo = fimg_get_rgb(src, x*2, y*2, pixel);
		if (foo) {
			fprintf(stderr, "%s: err get %d\n", __func__, foo);
			abort();
			}
		foo = fimg_plot_rgb(dst, x, y, pixel[0], pixel[1], pixel[2]);
		if (foo) {
			fprintf(stderr, "%s: err plot %d\n", __func__, foo);
			abort();
			}
		}

	}
return 0;
}
/* --------------------------------------------------------------------- */
int fimg_halfsize_1(FloatImg *src, FloatImg *dst, int notused)
{
int		wd, hd;
int		foo, x, y, x2, y2;
float		ac;

if (notused)	fprintf(stderr, "notused %d in %s\n", notused, __func__);

if (dst->width || dst->height) {
	fprintf(stderr, "*** %s: image at %p not empty\n", __func__, dst);
	fimg_describe(dst, "destination halfsize 1");
	return -2;
	}

wd = src->width / 2;	hd = src->height / 2;
if ( (foo = fimg_create(dst, wd, hd, FIMG_TYPE_RGB)) ) {
	fprintf(stderr, "%s: err create %d\n", __func__, foo);
	return -3;
	}

#define WS  (src->width)
#define WD  (dst->width)

for (y=0; y<hd; y++) {
	y2 = y * 2;
	for (x=0; x<wd; x++) {
		x2 = x * 2;

		ac = src->R[(y2*WS)+x2]      + src->R[(y2*WS)+x2+1] +
		     src->R[((1+y2)*WS)+x2]  + src->R[((1+y2)*WS)+x2+1];
		dst->R[y*WD +x] = ac / 4.0;

		ac = src->G[(y2*WS)+x2]      + src->G[(y2*WS)+x2+1] +
		     src->G[((1+y2)*WS)+x2]  + src->G[((1+y2)*WS)+x2+1];
		dst->G[y*WD +x] = ac / 4.0;

		ac = src->B[(y2*WS)+x2]      + src->B[(y2*WS)+x2+1] +
		     src->B[((1+y2)*WS)+x2]  + src->B[((1+y2)*WS)+x2+1];
		dst->B[y*WD +x] = ac / 4.0;

		}
	}

#undef WS
#undef WD

return 0;
}
/* --------------------------------------------------------------------- */
/*
 *	this function can be optimized with direct pixel copy
 */
int fimg_extractor(FloatImg *in, FloatImg *out, FimgArea51 *rect)
{
int		xs, ys, xd, yd;
int		count;
float		rgb[3];

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, in, out, rect);
#endif

if (verbosity > 1) {
	fimg_describe(in,  "extractor: source");
	fimg_describe(out, "extractor: destination");
	}

/*
 *	some sanity controls, please ! XXX
 */

count = 0;
for (yd=0; yd<rect->h; yd++) {
	ys = yd + rect->y;
	if ((ys<0) || (ys>=in->height)) continue;
	for (xd=0; xd<rect->w; xd++) {
		xs = xd + rect->x;
		if ((xs<0) || (xs>=in->width)) continue;
		fimg_get_rgb(in,  xs, ys, rgb);
		fimg_put_rgb(out, xd, yd, rgb);
		count++;
		}
	}

if (verbosity > 1) fprintf(stderr, "%s: %d pix moved\n", __func__, count);

return 0;
}
/* --------------------------------------------------------------------- */
/*    ho, btw, you can have a locck at 'incrustator.c' :)                */
/* --------------------------------------------------------------------- */
int fimg_mirror(FloatImg *src, FloatImg *dst, int notused)
{
float		*fptr;
int		line, col, offl;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p 0x%04x )\n", __func__,
				src, dst, notused);
#endif

if (notused)	fprintf(stderr, "notused %d in %s\n", notused, __func__);

if (fimg_images_not_compatible(src, dst)) {
	fprintf(stderr, "bad karma in %s\n", __func__);
	return -9;
	}

if (NULL == (fptr=alloca(src->width*sizeof(float)))) {
	fprintf(stderr, "%s: no mem available\n", __func__);
#if MUST_ABORT
	abort();
#endif
	return -11;
	}

for (line=0; line<src->height; line++) {
	offl = line * src->width;
	for (col=0; col<src->width; col++) 
			fptr[(src->width-1) - col] = src->R[offl+col];
	memcpy(dst->R+offl, fptr, src->width*sizeof(float));
	for (col=0; col<src->width; col++) 
			fptr[(src->width-1) - col] = src->G[offl+col];
	memcpy(dst->G+offl, fptr, src->width*sizeof(float));
	for (col=0; col<src->width; col++) 
			fptr[(src->width-1) - col] = src->B[offl+col];
	memcpy(dst->B+offl, fptr, src->width*sizeof(float));
	}

return 0;
}
/* --------------------------------------------------------------------- */

