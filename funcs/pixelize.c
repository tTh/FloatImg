/*
 *		P I X E L I Z E
 */
#include  <stdio.h>
#include  <stdint.h>
#include  <stdlib.h>

#include  "../floatimg.h"

extern int		verbosity;

/*   --------------------------------------------------------------   */
/* nouveau 10 octobre 2021 dans la roulotte de Terreblanque */

#define LARGEUR  16

int fimg_pixelize_h_0(FloatImg *psrc, FloatImg *pdst, int largeur)
{
int		line, col, loop, idx;
float		cr, cg, cb;		/* cumuls */

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %d )\n", __func__, psrc, pdst, largeur);
#endif

if (fimg_images_not_compatible(psrc, pdst)) {
	fprintf(stderr, "%s: err compatibility\n", __func__);
	return -8;
	}

switch(largeur) {
	case 8:	 case 16: case 32:
		break;
	default:
		fprintf(stderr, "%s: bad width %d\n", __func__, largeur);
		return -77;
	}

for (line=0; line<psrc->height; line++) {
	for (col=0; col<psrc->width; col+=largeur) {
		cr = cg = cb = 0.0;
		idx = line * psrc->width + col;
		for (loop=0; loop<largeur; loop++) {
			cr += psrc->R[idx+loop];
			cg += psrc->G[idx+loop];
			cb += psrc->B[idx+loop];
			}
		for (loop=0; loop<largeur; loop++) {
			pdst->R[idx+loop] = cr / (float)largeur;
			pdst->G[idx+loop] = cg / (float)largeur;
			pdst->B[idx+loop] = cb / (float)largeur;
			}
		}
	}

return 0;
}
/*   --------------------------------------------------------------   */
/*
 *	un essai dans la roulotte :)
 *	11 oct 2021 : premier jet, essai concluant, mais necessite
 *		du travail sur les rand() pour etre plus 'noisy'
 *
 */
int fimg_pixelize_h_rnd(FloatImg *psrc, FloatImg *pdst, int largeur)
{
static int	count = 0;
static int	flag  = 0;
int		foo;

/* may be a mollyguard on 'largeur' parameter ? */

if (0==count) {
	if (flag) {
		count = irand2(5, 10);
		flag = ! flag;
		}
	else	{
		count = irand2(20, 40);
		flag = ! flag;
		}
	if (verbosity) {
		fprintf(stderr, "%s c=%d f=%c\n", __func__,
					count, flag?'T':'F');
		}
	}

if (verbosity) {
	fprintf(stderr, "%s: count=%d flag=%d\n", __func__, count, flag);
	}

foo = fimg_pixelize_h_0(psrc, pdst, flag ? largeur : 32);
if (foo) {
	fprintf(stderr, "pixelize_h_0 give err %d  in %s\n", foo, __func__);
	}

count--;		/* nice trick bro */

return 0;
}
/*   --------------------------------------------------------------   */
