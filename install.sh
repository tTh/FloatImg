#!/bin/bash -v

set -e ; set -u

cp	libfloatimg.a 	/usr/local/lib
cp	floatimg.h 	/usr/local/include

cp	tools/mkfimg tools/fimg2pnm tools/fimgops 	\
	tools/fimg2png tools/fimg2tiff tools/fimg2fits	\
	tools/png2fimg tools/fimgstats tools/fimgfx	\
	tools/cumulfimgs tools/fimg2text		\
	tools/fimghalfsize				\
	tools/fimgmetadata tools/fimgfilters		\
	tools/fimgextract				\
	/usr/local/bin

cp	v4l2/grabvidseq v4l2/video-infos		\
	/usr/local/bin

cp	Fonderie/interpolator Fonderie/fonderie		\
	Fonderie/singlepass				\
	/usr/local/bin

A="tools/addtga2fimg"
if [ -r $A ] ; then
	cp $A /usr/local/bin
fi
