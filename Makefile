####################################################
#          Before running make, you must have      #
#          a look to the 'build.sh' script !       #
####################################################

COPT = -Wall -Wextra -fpic -g  -no-pie -DDEBUG_LEVEL=0
LDOPT = libfloatimg.a -g -lm 

all:	essai

#---------------------------------------------------------------

essai:		essai.c libfloatimg.a floatimg.h Makefile
	gcc $(COPT) $< $(LDOPT) -lpnglite -lz -o $@

install:
	@echo "=== Use the 'install.sh' script ==="

#---------------------------------------------------------------

TOTAR = *.[ch] Makefile *.sh *.md 			\
	doc/the*.tex doc/mk*.sh	doc/*.txt		\
	funcs/*.[ch] funcs/Makefile			\
	tools/*.[ch] tools/*.sh tools/README.md tools/Makefile \
	v4l2/*.[ch] v4l2/Makefile			\
	scripts/*.sh scripts/README.md			\
	lib/*.[ch] lib/Makefile

lines:          $(TOTAR)
	@wc $(TOTAR) | sort -n

tarball:        $(TOTAR)
	date > tarball
	ls $(TOTAR) | sed 's/^/FloatImg\//'  > MANIFEST
	( cd .. ; tar zcvf floatimg.tar.gz `cat FloatImg/MANIFEST` )

#---------------------------------------------------------------
