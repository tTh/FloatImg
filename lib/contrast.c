/*
 *		contrast.c - part of libfloatimg
 *		--------------------------------
 *
 *	you can see some use in 'tools/fimgfx.c', so you can thing
 *	about the 'maxval' parameter. It was touchy :)
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <string.h>
#include  <math.h>

#include  "../floatimg.h"

extern int		verbosity;

/* ---------------------------------------------------------------- */
int fimg_id_contraste(char *str)
{

if (!strcmp(str, "none")) 	return CONTRAST_NONE;
if (!strcmp(str, "sqrt")) 	return CONTRAST_SQRT;
if (!strcmp(str, "pow2")) 	return CONTRAST_POW2;
if (!strcmp(str, "cos01")) 	return CONTRAST_COS01;
if (!strcmp(str, "cos010")) 	return CONTRAST_COS010;
if (!strcmp(str, "xper")) 	return CONTRAST_XPER;

#if DEBUG_LEVEL
fprintf(stderr, "%s: unknow contrast name '%s'\n", __func__, str);
#endif

return -1;
}
/* ---------------------------------------------------------------- */
/*
 *	if the second parameter is NULL, operate 'in-place'
 */
int fimg_square_root(FloatImg *s, FloatImg *d, double maxval)
{
int		nbre, idx;
double		dval;

if (s->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s: type %d invalide\n",
				__func__, s->type);
	return -4;
	}

if (NULL==d) { d = s; }
else	{
	if (d->type != FIMG_TYPE_RGB) {
		fprintf(stderr, "%s: dst type %d invalide\n",
					__func__, d->type);
		return -4;
		}
	}

nbre = s->width * s->height;

for (idx=0; idx<nbre; idx++) {
	dval = s->R[idx] / maxval;
	d->R[idx] = maxval * sqrt(dval);
	dval = s->G[idx] / maxval;
	d->G[idx] = maxval * sqrt(dval);
	dval = s->B[idx] / maxval;
	d->B[idx] = maxval * sqrt(dval);
	}

return 0;
}
/* ---------------------------------------------------------------- */
int fimg_power_2(FloatImg *s, FloatImg *d, double maxval)
{
int		nbre, idx;
double		dval;

if (s->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s: src type %d invalide\n",
				__func__, s->type);
	return -4;
	}

if (NULL==d) { d = s; }
else	{
	if (d->type != FIMG_TYPE_RGB) {
		fprintf(stderr, "%s: dst type %d invalide\n",
					__func__, d->type);
		return -4;
		}
	}

nbre = s->width * s->height;

for (idx=0; idx<nbre; idx++) {
	dval = s->R[idx] / maxval;
	d->R[idx] = maxval * dval * dval;
	dval = s->G[idx] / maxval;
	d->G[idx] = maxval * dval * dval;
	dval = s->B[idx] / maxval;
	d->B[idx] = maxval * dval * dval;
	}

return 0;
}
/* ---------------------------------------------------------------- */
/*
		#macro Cos_01( X )
		  (0.5-0.5*cos( 3.141592654 * X))
		#end
 */
int fimg_cos_01(FloatImg *s, FloatImg *d, double maxval)
{
int		nbre, idx;
double		dval;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p  %g )\n", __func__, s, d, maxval);
#endif

if (s->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s: type %d invalide\n",
				__func__, s->type);
	return -4;
	}

if (NULL==d) { d = s; }			/* In place */
else	{
	if (d->type != FIMG_TYPE_RGB) {
		fprintf(stderr, "%s: dst type %d invalide\n",
					__func__, d->type);
		return -4;
		}
	}

nbre = s->width * s->height;

for (idx=0; idx<nbre; idx++) {
	dval = s->R[idx] / maxval;
	d->R[idx] = maxval * (0.5 - 0.5 * cos(3.141592654*dval));
	dval = s->G[idx] / maxval;
	d->G[idx] = maxval * (0.5 - 0.5 * cos(3.141592654*dval));
	dval = s->B[idx] / maxval;
	d->B[idx] = maxval * (0.5 - 0.5 * cos(3.141592654*dval));
	}

return 0;
}
/* ---------------------------------------------------------------- */
int fimg_cos_010(FloatImg *s, FloatImg *d, double maxval)
{
int		nbre, idx;
double		dval;

if (s->type != FIMG_TYPE_RGB) {
	fprintf(stderr, "%s : type %d invalide\n",
				__func__, s->type);
	return -4;
	}

if (NULL==d) { d = s; }
else	{
	if (d->type != FIMG_TYPE_RGB) {
		fprintf(stderr, "%s : dst type %d invalide\n",
					__func__, d->type);
		return -4;
		}
	}

nbre = s->width * s->height;

for (idx=0; idx<nbre; idx++) {
	dval = s->R[idx] / maxval;
	d->R[idx] = maxval * (0.5 - 0.5 * cos(2*3.141592654*dval));
	dval = s->G[idx] / maxval;
	d->G[idx] = maxval * (0.5 - 0.5 * cos(2*3.141592654*dval));
	dval = s->B[idx] / maxval;
	d->B[idx] = maxval * (0.5 - 0.5 * cos(2*3.141592654*dval));
	}

return 0;
}

/* ---------------------------------------------------------------- */

