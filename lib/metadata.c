/*
 *              metadata.c
 *		----------
 *
 *	This is not a stable version. You must expect problems,
 *	and the first one was time_t 32 vs. 64 bits.
 *
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <time.h>
#include	<sys/time.h>
#include  <string.h>

#include  "../floatimg.h"

extern int verbosity;           /* must be declared around main() */

/* ---------------------------------------------------------------- */
int fimg_show_metadata(FimgMetaData *pmd, char *title, int notused)
{
int		foo;
double		doubletime;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' 0x%08x )\n", __func__,
				pmd, title, notused);
#endif

if (notused) {
	fprintf(stderr, "notused was %d, must be 0 in %s\n",
				notused, 		__func__);
	}
if (NULL != title) {
	fprintf(stderr, "======== metadata for %s\n", title);
	}
if (verbosity) {
	fprintf(stderr, "sizeof(metadata)     = %ld\n",    \
				sizeof(FimgMetaData));
	fprintf(stderr, "    Magic [%08x]\n", pmd->magic);
	}

/* SHOW TIMESTAMP HERE */
fprintf(stderr, "secs from epoch      = %ld\n", pmd->timestamp.tv_sec);
fprintf(stderr, "date & time          = %s",   ctime(&pmd->timestamp.tv_sec));
doubletime = (double)pmd->timestamp.tv_sec +			\
		(double)pmd->timestamp.tv_usec / 1e6;
fprintf(stderr, "dtime of day         = %12.3f\n", doubletime);
fprintf(stderr, "creator pid          = %ld\n", pmd->cpid);
fprintf(stderr, "float value          = %.3f\n", pmd->fval);
fprintf(stderr, "counter              = %d\n", pmd->count);
fprintf(stderr, "id camera            = '%s'\n", pmd->idcam);
fprintf(stderr, "origin               = 0x%x\n", pmd->origin);

fputs("reserved words are:\n      ", stderr);
for (foo=0; foo<8; foo++) {
	fprintf(stderr, "     0x%08x", pmd->reserved[foo]);
	if (3 == foo) fputs("\n      ", stderr);
	}
fputc('\n', stderr);
return 0;
}
/* ---------------------------------------------------------------- */
/*
 *	those values may be loaded from a config file ?
 */
int fimg_default_metadata(FimgMetaData *pmd, int bla)
{
int			foo;
struct timeval		tvl;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d )\n", __func__, pmd, bla);
#endif

memset(pmd, 0, sizeof(FimgMetaData));

/* set timestamp here ? */
/* CHALLENGE ACCEPTED */
memset(&tvl, 0, sizeof(struct timeval));
foo = gettimeofday(&tvl, NULL);
if (foo) {
	/* error on get time of day ? */
	perror("omg");
	}
else	{
	if (verbosity > 1) {
		fprintf(stderr, "%s : set TimeOfDay to  %12ld %8ld\n", \
					__func__, tvl.tv_sec, tvl.tv_usec);
		}
	memcpy(&(pmd->timestamp), &tvl, sizeof(struct timeval));
	}

pmd->magic	   = MAGIC_MDATA;
pmd->cpid          = getpid();		// we are the creator, no ?
pmd->count         = 1;			// mmmm...
pmd->fval          = 255.0;		// Ok
strcpy(pmd->idcam, "<noname camera>");
pmd->origin        = 0xdeadbeef;	// classic joke, sorry
pmd->reserved[0]   = bla;
pmd->reserved[7]   = 0x55445544;	// magic number is a crime

return 0;
}
/* ---------------------------------------------------------------- */
int fimg_get_metadata_from_file(char *fname, FimgMetaData *pmd)
{
FILE			*fp;
FimgFileHead		filehead;
FimgMetaData		metadata;
int			foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p )\n", __func__, fname, pmd);
#endif

if (NULL==(fp=fopen(fname, "r"))) {
	perror(fname);
	return -1;
	}

foo = fread(&filehead, sizeof(FimgFileHead), 1, fp);
if (1 != foo) {
	fprintf(stderr, "short read (%d) on %s (head)\n", foo, fname);
	fclose(fp);
	return -2;
	}

if (memcmp(filehead.magic, "FIMG", 4)) {
	fprintf(stderr, "'%s' is not a fimg file.\n", fname);
	fclose(fp);
	return -3;
	}

if ('a' != filehead.magic[4]) {
	fprintf(stderr, "file '%s' have no metadata.\n", fname);
	fclose(fp);
	return -4;
	}

foo = fread(&metadata, sizeof(FimgMetaData), 1, fp);
if (1 != foo) {
	fprintf(stderr, "short read on %s (metadata)\n", fname);
	fclose(fp);
	return -5;
	}

fclose(fp);		/* got all needed datas */

if (MAGIC_MDATA != metadata.magic) {
	fprintf(stderr, "%s: magic was %08X, wtf?\n", __func__,
					metadata.magic);
	return -4;
	}


memcpy(pmd, &metadata, sizeof(FimgMetaData));

return 0;
}
/* ---------------------------------------------------------------- */
