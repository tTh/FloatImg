/*
 *		OPERATORS
 *
 *	operations entre des images. 
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <string.h>
#include  <math.h>

#include  "../floatimg.h"

extern int verbosity;		/* must be declared around main() */

/* ---------------------------------------------------------------- */
/*
 * 		A + B -> D
 */
int fimg_add_3(FloatImg *a, FloatImg *b, FloatImg *d)
{
int		idx, nbpixels;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, a, b, d);
#endif

if (FIMG_TYPE_RGB != a->type ||
	FIMG_TYPE_RGB != b->type ||
	FIMG_TYPE_RGB != d->type) {
	fprintf(stderr, "%s : got a bad type fimg\n", __func__);
	return -8;
	} 

nbpixels = a->width * a->height;

for (idx=0; idx<nbpixels; idx++) {
	d->R[idx] = a->R[idx] + b->R[idx];
	d->G[idx] = a->G[idx] + b->G[idx];
	d->B[idx] = a->B[idx] + b->B[idx];
	}

return 0;
}
/* ---------------------------------------------------------------- */
/*
 * 		B += A		may be faster than fimg_add_3 ?
 */
int fimg_add_2(FloatImg *a, FloatImg *b)
{
int		idx, nbpixels;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, a, b);
#endif

if (FIMG_TYPE_RGB != a->type || FIMG_TYPE_RGB != b->type) {
	fprintf(stderr, "%s : got a bad type fimg\n", __func__);
	return -8;
	}

nbpixels = a->width * a->height;

for (idx=0; idx<nbpixels; idx++) {
	b->R[idx] += a->R[idx];
	b->G[idx] += a->G[idx];
	b->B[idx] += a->B[idx];
	}

return 0;
}
/* ---------------------------------------------------------------- */
/*
 *		A - B -> D
 */
int fimg_sub_3(FloatImg *a, FloatImg *b, FloatImg *d)
{
int		idx, nbpixels;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, a, b, d);
#endif

if (	FIMG_TYPE_RGB != a->type  ||
	FIMG_TYPE_RGB != b->type  ||
	FIMG_TYPE_RGB != d->type) {
	fprintf(stderr, "%s : got a bad type fimg\n", __func__);
	return -8;
	}

nbpixels = a->width * a->height;

/*		maybe we can speedup this loop for
 *		avoiding the cache strashing ?
 */
for (idx=0; idx<nbpixels; idx++) {
	d->R[idx] = fabs(a->R[idx] - b->R[idx]);
	d->G[idx] = fabs(a->G[idx] - b->G[idx]);
	d->B[idx] = fabs(a->B[idx] - b->B[idx]);
	}

return 0;
}
/* ---------------------------------------------------------------- */
/*
 * 		B *= A		may be faster than fimg_mul_3 ?
 */
int fimg_mul_2(FloatImg *a, FloatImg *b)
{
int		idx, nbpixels;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, a, b);
#endif

if (FIMG_TYPE_RGB != a->type || FIMG_TYPE_RGB != b->type) {
	fprintf(stderr, "%s : got a bad type fimg\n", __func__);
	return -8;
	}

nbpixels = a->width * a->height;

for (idx=0; idx<nbpixels; idx++) {
	b->R[idx] *= a->R[idx];
	b->G[idx] *= a->G[idx];
	b->B[idx] *= a->B[idx];
	}

return 0;
}
/* ---------------------------------------------------------------- */
/*
 *		A * B -> D
 */
int fimg_mul_3(FloatImg *a, FloatImg *b, FloatImg *d)
{
int		idx, nbpixels;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, a, b, d);
#endif

if (FIMG_TYPE_RGB != a->type || FIMG_TYPE_RGB != b->type ||
		FIMG_TYPE_RGB != d->type) {
	fprintf(stderr, "%s : got a bad type fimg\n", __func__);
	return -8;
	} 

nbpixels = a->width * a->height;

for (idx=0; idx<nbpixels; idx++) {
	d->R[idx] = a->R[idx] * b->R[idx];
	d->G[idx] = a->G[idx] * b->G[idx];
	d->B[idx] = a->B[idx] * b->B[idx];
	}

return 0;
}
/* ---------------------------------------------------------------- */
int fimg_minimum(FloatImg *a, FloatImg *b, FloatImg *d)
{
int		idx, nbiter;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, a, b, d);
#endif

if (FIMG_TYPE_RGB != a->type || FIMG_TYPE_RGB != b->type ||
		FIMG_TYPE_RGB != d->type) {
	fprintf(stderr, "%s : got a bad type fimg\n", __func__);
	return -8;
	}

nbiter = a->width * a->height;

for (idx=0; idx<nbiter; idx++) {
	if (a->R[idx] < b->R[idx])	d->R[idx] = a->R[idx];
	else				d->R[idx] = b->R[idx];
	if (a->G[idx] < b->G[idx])	d->G[idx] = a->G[idx];
	else				d->G[idx] = b->G[idx];
	if (a->B[idx] < b->B[idx])	d->B[idx] = a->B[idx];
	else				d->B[idx] = b->B[idx];
	}

return 0;
}
/* ---------------------------------------------------------------- */
int fimg_maximum(FloatImg *a, FloatImg *b, FloatImg *d)
{
int		idx, nbiter;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p )\n", __func__, a, b, d);
#endif

if (FIMG_TYPE_RGB != a->type || FIMG_TYPE_RGB != b->type ||
		FIMG_TYPE_RGB != d->type) {
	fprintf(stderr, "%s : got a bad type fimg\n", __func__);
	return -8;
	}

nbiter = a->width * a->height ;

for (idx=0; idx<nbiter; idx++) {
	if (a->R[idx] > b->R[idx])	d->R[idx] = a->R[idx];
	else				d->R[idx] = b->R[idx];
	if (a->G[idx] > b->G[idx])	d->G[idx] = a->G[idx];
	else				d->G[idx] = b->G[idx];
	if (a->B[idx] > b->B[idx])	d->B[idx] = a->B[idx];
	else				d->B[idx] = b->B[idx];
	}

return 0;
}
/* ---------------------------------------------------------------- */
