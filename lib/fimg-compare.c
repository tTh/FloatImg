/*
 *		fimg-compare.c
 *
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  "string.h"

#include  "../floatimg.h"

extern int verbosity;		/* must be declared around main() */

/* ---------------------------------------------------------------- */
/*
 *	return 0 if images are compatibles
 */
int fimg_images_not_compatible(FloatImg *a, FloatImg *b)
{
#if DEBUG_LEVEL > 1
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, a, b);
#endif

if (a->type != b->type) {
	if (verbosity) fprintf(stderr, "%p %p != type\n", a, b);
	return -10;
	}

if (a->width != b->width) {
	if (verbosity) fprintf(stderr, "%p %p != width\n", a, b);
	return -11;
	}

if (a->height != b->height) {
	if (verbosity) fprintf(stderr, "%p %p != height\n", a, b);
	return -12;
	}

return 0;
}
/* ---------------------------------------------------------------- */
