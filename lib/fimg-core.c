/*
 *		fimg-core.c
 *
 *
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  "string.h"

#include  "../floatimg.h"

extern int verbosity;		/* must be declared around main() */

/* ---------------------------------------------------------------- */
int fimg_type_is_valid(int type)
{
switch (type) {
 	case FIMG_TYPE_GRAY:
	case FIMG_TYPE_RGB:
	case FIMG_TYPE_RGBA:
	case FIMG_TYPE_RGBZ:		return 1;
	}
return 0;
}
/* --------------------------------------------------------------------- */
char *fimg_str_type(int type)
{
switch (type) {
 	case FIMG_TYPE_GRAY:		return "gray";
	case FIMG_TYPE_RGB:		return "rgb";
	case FIMG_TYPE_RGBA:		return "rgba";
	case FIMG_TYPE_RGBZ:		return "rgbz";
	}
return "???";
}
/* --------------------------------------------------------------------- */
int fimg_print_version(int k)
{
fprintf(stderr, "*** FloatImg library, v%d '%s' (%s, %s)\n",
		FIMG_VERSION, RELEASE_NAME, __DATE__, __TIME__);

if (51 == k) {
	puts("+------------------------+");
	puts("|  Pastis is coming soon |");
	puts("+------------------------+");
	}

return 0;
}
/* --------------------------------------------------------------------- */
void fimg_print_sizeof(void)
{
fprintf(stderr, "   sz FloatImg =   %lu\n", sizeof(FloatImg));
fprintf(stderr, "   sz filehead =   %lu\n", sizeof(FimgFileHead));
fprintf(stderr, "   sz metadata =   %lu\n", sizeof(FimgMetaData));
fprintf(stderr, "   sz filter   =   %lu\n", sizeof(FimgFilter3x3));
}
/* --------------------------------------------------------------------- */
void fimg_printdims(char *txt, FloatImg *pi)
{
fprintf(stderr, "# %s   %dx%d\n", txt, pi->width, pi->height);
}
/* --------------------------------------------------------------------- */
void fimg_printhead(FloatImg *h)
{
printf("%5d  %5d  %2d  %p %p %p %p\n", h->width, h->height, h->type,
			h->R, h->G, h->B, h->A);
}
/* --------------------------------------------------------------------- */
int fimg_describe(FloatImg *head, char *txt)
{
printf("----- '%s' at %p -----\n", txt, head);

if( ! fimg_type_is_valid(head->type) ) {
	fprintf(stderr, "*** %s *** type %d invalid *** %s ***\n",
				__func__, head->type, txt);
	return -1;
	}

printf("    type         %d   %s\n", head->type, fimg_str_type(head->type));
printf("    dims         %d x %d\n", head->width, head->height);
printf("    fval/count   %f  %d\n", head->fval, head->count);

printf("    pixels@      %p  %p  %p %p\n",
			head->R, head->G, head->B, head->A);

return 0;
}
/* ---------------------------------------------------------------- */
static float *plane_alloc(int size)
{
float		*fptr;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %d )\n", __func__, size);
#endif

fptr = calloc(size, sizeof(float));
if (NULL==fptr) {
	fprintf(stderr, "no more memory available, ABEND\n");
	abort();
	}
return fptr;
}
/* ---------------------------------------------------------------- */
/*
 *	values for the parameter 'type' are defined in 'floatimg.h'
 */
int fimg_create(FloatImg *fimg, int w, int h, int type)
{
int		size;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %d %d %d )\n", __func__, fimg, w, h, type);
#endif

if ( ! fimg_type_is_valid(type) ) {
	fprintf(stderr, "%s: type %d invalid\n", __func__, type);
	return -2;
	}

/*
 *	what to do if we've got a descriptor for an image
 *	already allocated ? and how to check that ?
*/
memset(fimg, 0, sizeof(FloatImg));

size    = w * h;
fimg->width = w;	fimg->height = h;
fimg->type  = type;

/* the red channel is allway allocated */
fimg->R = (float *)plane_alloc(size);

if (FIMG_TYPE_RGB == type) {
	fimg->G = (float *)plane_alloc(size);
	fimg->B = (float *)plane_alloc(size);
	}

if (FIMG_TYPE_RGBA == type) {
	fimg->G = (float *)plane_alloc(size);
	fimg->B = (float *)plane_alloc(size);
	fimg->A = (float *)plane_alloc(size);
	}

return 0;
}

/* --------------------------------------------------------------------- */
int fimg_destroy(FloatImg *fimg)
{

#if DEBUG_LEVEL
fprintf(stderr, ">>> %-25s ( %p )\n", __func__, fimg);
#endif

if (NULL == fimg) {
	fprintf(stderr, "%s : descriptor address is null\n", __func__);
	return -1;
	}

if ( ! fimg_type_is_valid(fimg->type) ) {
	fprintf(stderr, "%s : type %d invalid\n", __func__,
					fimg->type);
	return -2;
	}
if (NULL == fimg->R) {
	fprintf(stderr, "%s : %p already freed ?\n", __func__, fimg);
	return -3;
	}

free(fimg->R);
if (FIMG_TYPE_RGB == fimg->type) {
	free(fimg->G);
	free(fimg->B);
	}
memset(fimg, 0, sizeof(FloatImg));

return 0;
}
/* --------------------------------------------------------------------- */
int fimg_clone(FloatImg *old, FloatImg *new, int flags)
{
int		foo, size;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p 0x%x )\n", __func__, old, new, flags);
#endif

if ( ! fimg_type_is_valid(old->type) ) {
	fprintf(stderr, "invalid type %d in %s\n", old->type, __func__);
	return -2;
	}

memset(new, 0, sizeof(FloatImg));

foo = fimg_create(new, old->width, old->height, old->type);
if (foo) {
	fprintf(stderr, "error %d in %s\n", foo, __func__);
	return -3;
	}

if (flags & 0x01) {			/* copy pixels values */
	size = old->width * old->height * sizeof(float);
	memcpy(new->R, old->R, size);
	memcpy(new->G, old->G, size);
	memcpy(new->B, old->B, size);
	}

return 0;
}
/* --------------------------------------------------------------------- */
int fimg_copy_data(FloatImg *from, FloatImg *to)
{
int		size;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, from, to);
#endif

foo = fimg_images_not_compatible(from, to);
if (foo) {
	fprintf(stderr, "%s: pics not compatible (%d)\n", __func__, foo);
	return foo;
	}

size = from->width * from->height * sizeof(float);
memcpy(to->R, from->R, size);
memcpy(to->G, from->G, size);
memcpy(to->B, from->B, size);

return 0;
}
/* --------------------------------------------------------------------- */
int fimg_clear(FloatImg *fimg)
{
int		size;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %-25s ( %p )\n", __func__, fimg);
#endif
if ( ! fimg_type_is_valid(fimg->type) ) {
	fprintf(stderr, "invalid type %d in %s\n", fimg->type, __func__);
	return -2;
	}

size = fimg->width * fimg->height * sizeof(float);
memset(fimg->R, 0, size);
memset(fimg->G, 0, size);
memset(fimg->B, 0, size);

return 0;
}
/* --------------------------------------------------------------------- */
int fimg_rgb_constant(FloatImg *head, float r, float g, float b)
{
int		idx, size;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %-25s ( %p %f %f %f )\n", __func__, head,
				r, g, b);
#endif

if (head->type != FIMG_TYPE_RGB) {
	return -21;
	}

size = head->width * head->height;

for (idx=0; idx<size; idx++) {
	head->R[idx] = r;
	head->G[idx] = g;
	head->B[idx] = b;
	}

return 0;
}
/* --------------------------------------------------------------------- */

int fimg_plot_rgb (FloatImg *head, int x, int y, 
					float r, float g, float b)
{
int	offset;

if (head->type < 3) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%s : type %d is bad.\n", __func__, head->type);
#endif
	return -1;
	}

offset = x + (y * head->width);

#if DEBUG_LEVEL > 1
fprintf(stderr, ">>> %s ( %p %d %d %f )\n", __func__, head, x, y, gray);
fprintf(stderr, "    offset %d\n", offset);
#endif

head->R[offset] = r;
head->G[offset] = g;
head->B[offset] = b;

return 0;
}
/* ---------------------------------------------------------------- */
int fimg_add_rgb(FloatImg *head, int x, int y, float r, float g, float b)
{
int	offset;

if (head->type != FIMG_TYPE_RGB) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%s : type %d is bad.\n", __func__, head->type);
#endif
	return -1;
	}

offset = x + (y * head->width);
head->R[offset] += r;
head->G[offset] += g;
head->B[offset] += b;
return 0;
}

/* --------------------------------------------------------------------- */
/* nouveau 12 fevrier 2020 */
int fimg_get_rgb(FloatImg *head, int x, int y, float *prgb)
{
int	offset;

if (head->type != FIMG_TYPE_RGB) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%s : type %d is bad.\n", __func__, head->type);
#endif
	return -1;
	}

offset = x + (y * head->width);
prgb[0] = head->R[offset];
prgb[1] = head->G[offset];
prgb[2] = head->B[offset];

return 0;
}
/* --------------------------------------------------------------------- */
/* nouveau 24 mars 2020    -    coronacoding */
int fimg_put_rgb(FloatImg *head, int x, int y, float *prgb)
{
int	offset;

if (head->type != FIMG_TYPE_RGB) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "%s : type %d is bad.\n", __func__, head->type);
#endif
	return -1;
	}

offset = x + (y * head->width);
head->R[offset] = prgb[0];
head->G[offset] = prgb[1];
head->B[offset] = prgb[2];

return 0;
}
/* --------------------------------------------------------------------- */

