/*
 *			interpolate.c
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <string.h>
#include  <math.h>

#include  "../floatimg.h"

extern int		verbosity;

/* ---------------------------------------------------------------- */
static int gray_interpolate(FloatImg *s1, FloatImg *s2, FloatImg *d, float coef)
{
int		picsize, idx;

picsize = d->width * d->height;
for (idx=0; idx<picsize; idx++) {
	d->R[idx] = (coef * s1->R[idx]) + ((1.0-coef) * s2->R[idx]);
	}

return 0;
}
/* ---------------------------------------------------------------- */
static int rgb_interpolate(FloatImg *s1, FloatImg *s2, FloatImg *d, float coef)
{
int		picsize, idx;

picsize = d->width * d->height;
for (idx=0; idx<picsize; idx++) {
	d->R[idx] = (coef * s1->R[idx]) + ((1.0-coef) * s2->R[idx]);
	d->G[idx] = (coef * s1->G[idx]) + ((1.0-coef) * s2->G[idx]);
	d->B[idx] = (coef * s1->B[idx]) + ((1.0-coef) * s2->B[idx]);
	}

return 0;
}
/* ---------------------------------------------------------------- */
int fimg_interpolate(FloatImg *s1, FloatImg *s2, FloatImg *d, float coef)
{
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p %p %f )\n", __func__,
				s1, s2, d, coef);
#endif

/* sanity check here ? or may be ask the
   caller to do it himself ?
*/
if ( (coef < 0.0) || (coef >= 1.0) ){
	fprintf(stderr, "%s: coef %f out of range\n", __func__, coef);
	return -8;
	}

foo = fimg_images_not_compatible(s1, s2);
if (foo) {
	fprintf(stderr, "compat s1 s2 -> %d\n", foo);
	return foo;
	}

foo = fimg_images_not_compatible(s1, d);
if (foo) {
	fprintf(stderr, "compat s1 d  -> %d\n", foo);
	return foo;
	}

switch (s1->type) {
	case FIMG_TYPE_GRAY:
		gray_interpolate (s1, s2, d, coef);	break;
	case FIMG_TYPE_RGB:
		rgb_interpolate  (s1, s2, d, coef);	break;
	case FIMG_TYPE_RGBA:
		fprintf(stderr, "WTF?\n");
		return -18;
	default:
		fprintf(stderr, "%s, %d is a bad type\n", __func__, s1->type);
		return -18;
	}

return 0;
}
/* ---------------------------------------------------------------- */
