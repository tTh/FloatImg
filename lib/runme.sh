#!/bin/bash

# --------------------------------------------------------------------
function grab
{
../v4l2/grabvidseq -s 960x720 -n 100 -p 0.193	\
			-vv -c none			\
			-o original.fimg
}
# --------------------------------------------------------------------

make t && ./t

../tools/fimgops original.fimg cos_01.fimg mini minimum.fimg
../tools/fimgops original.fimg cos_01.fimg maxi maximum.fimg

for picz in original power2 squareroot cos_01 minimum maximum
do

	echo "_______________________" ${picz}

	# ../tools/fimgstats -v ${picz}.fimg
	../tools/fimg2pnm  ${picz}.fimg ${picz}.pnm

	convert -pointsize 48			\
		-fill black -annotate +14+40 "${picz}" 			\
		-fill white -annotate +16+42 "${picz}" 			\
		${picz}.pnm ${picz}.png

	rm ${picz}.pnm

done

convert -delay 150 *.png foo.gif

