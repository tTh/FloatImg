# Floatimg, the base/core library

## Data structures

Image's pixels (floating point number, a `float` for C)
are ordonned as an array of lines of single
componant values. Every componant is in a separate array,
who are *glued* together by an in-memory image descriptor.

```
#define MAGIC_FIMG	0x00F11F00
typedef struct {
  uint32_t	      magic;
  int		      width;
  int		      height;
  int		      type;
  float           fval;
  int		      count;
  float	          *R, *G, *B, *A;
  FimgMetaData    mdatas;			// added 20230912
  int		      reserved;
	} FloatImg;
```
For the current state of this struc, look at the main
[header file](../floatimg.h), located at the root of the project.

