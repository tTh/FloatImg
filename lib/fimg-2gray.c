/*
 *		fimg-2gray.c
 *
 *	This is a tricky job...
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  "string.h"

#include  "../floatimg.h"

extern int verbosity;		/* must be declared around main() */

/* --------------------------------------------------------------------- */
/*
 *	floating resultat img MUST be allocated before calling this func.
 *	and MUST be of type 'gray image'.
 */
int fimg_mk_gray_from(FloatImg *src, FloatImg *dst, int k)
{
float		kr, kg, kb, kdiv;
int		nbb, foo;

if (k)	{
	/* some random funky values ...  */
	kr = 0.789;	kg = 0.523;	kb = 0.349;
	}
else	{
	/* ... and canonic random values */
	kr = kg = kb = 1.0;
	}
kdiv = kr + kg + kb;

/* we must check the validity of our parameters */
if (FIMG_TYPE_RGB != src->type) {
	fprintf(stderr, "%s : bad src type %d on %p\n", __func__, 
				src->type, src);
	return -8;
	}

if (FIMG_TYPE_GRAY != dst->type) {
	fprintf(stderr, "%s : bad dst type %d on %p\n", __func__, 
				dst->type, dst);
	/* may be we can convert dst picture on the fly ? */
	return -9;
	}

/* entering the main processing loop */
nbb = src->width * src->height;
for (foo=0; foo<nbb; foo++) {
	dst->R[foo] = ( (src->R[foo] * kr) +
			(src->G[foo] * kg) +
			(src->B[foo] * kb) ) /
				kdiv;
	}

return 0;
}
/* --------------------------------------------------------------------- */
/* this function can work 'in place'					 */
int fimg_desaturate(FloatImg *src, FloatImg *dst, int notused)
{
int		foo, nbb;

/* we must check the validity of our parameters */
if (notused) {
	fprintf(stderr, "notused was %d, must be 0 in %s\n",
				notused, 		__func__);
	}
if (FIMG_TYPE_RGB != src->type || FIMG_TYPE_RGB != dst->type) {
	fprintf(stderr, "%s : bad image type\n", __func__);
	return -18;
	}

/* entering the main processing loop */
nbb = src->width * src->height;
for (foo=0; foo<nbb; foo++) {
	dst->R[foo] = dst->G[foo] = dst->B[foo] =
			(src->R[foo] + src->G[foo] + src->B[foo]) / 3.0;
	}

return 0;
}
/* --------------------------------------------------------------------- */




