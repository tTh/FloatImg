# Traitement des images en virgule flottante.

C'est d"abord un ensemble de fonctions pour traiter des images avec une énorme dynamique
sur les niveaux de pixels. C'est aussi quelques outils pour traiter ces images.
Et c'est enfin plusieurs embryons de logiciel destiné à faire des photos floues,
voire même des [films flous](Fonderie/).

![horloge floue](http://la.buvette.org/photos/cumul/horloge.png "horloge floue")

Il y a une [description](http://la.buvette.org/photos/cumul/) bien plus 
pas trop longue pour les curieux, et un début de
[documentation](http://la.buvette.org/photos/cumul/the_floatimg_hack.pdf)
pour les codeurs.
Le service après-vente est (plus ou moins bien) assuré sur
la [mailing list](https://lists.tetalab.org/mailman/listinfo/tetalab) et/ou
le canal IRC #tetalab sur le réseau de 
[Libera.Chat](https://libera.chat/)...

Par ailleurs, d'autres expérimentations sont
[en cours](http://la.buvette.org/photos/cumul/fonderie/vidz.html#interpolator)
sur le traitement et l'assemblage de ces images floues dans le but de faire
des films flous. 

## *Show us the code !*

Il y a plusieurs répertoires contenant le code source, en voici
la liste :

  * [lib](lib/README.md) : le noyau du système, fonctions de base.
  * [funcs](funcs/README.md) : fonctions de support : export, traitement
    filtrage, effets...
  * [tools](tools/README.md) : utilitaires divers, outils de debug et gadgets.
  * [v4l2](v4l2/README.md): gestion des webcams sous Linux.
  * [experiment](experiment/README.md) : fonctions en chantier, qui migreront
    un jour dans d'autres catégories.
  * [contrib](contrib/README.md) : des choses peu ou pas maintenues.


## Dépendances

Bien entendu, avant tout, il faut installer quelques outils et
dépendances. Je vais tenter de les lister dans le plus grand
désordre (à la sauce Debian) :

```
apt  install  libtiff-dev
apt  install  libpnglite-dev
apt  install  liblo-dev
apt  install  libv4l2-dev
apt  install  libcfitsio-dev
apt  install  libnetpbm-dev
apt  install  libncurses-dev
```
Bon, OK, je suis en train de changer de machine, et ça serait vraiment
cool d'avoir juste une ligne à c'n'p, donc voila :

```
apt install  libtiff-dev libpnglite-dev  liblo-dev  libv4l2-dev \
             libcfitsio-dev libnetpbm-dev libncurses-dev
```
Il est probable que j'en oublie.
Et ya Debian qui change des trucs, alors, ça marche plus, du
genre que c'est la deuxième fois que ça m'arrive.

```
E: Unable to locate package libv4l2-dev
E: Unable to locate package libnetpbm-dev
```

Ensuite, j'ai dans l'idée de construire
um meta-packet à la sauce Debian pour installer facilement tout ce
qui sert à faire fonctionner ce kluge. Ensuite, j'irais voir du
coté de pkg-config.

Certains outils externes sont aussi utiles :

- gnuplot, pour analyser des données,
- ImageMagick, parce que Brunus aime ça,
- LaTeX, pour la (trop maigre) documentation.

## Documentation

Encore trop légère, mais déja [présente](doc/).
C'est à vous de compiler le
[PDF](http://la.buvette.org/photos/cumul/the_floatimg_hack.pdf)

## TODO

https://berthub.eu/articles/posts/always-do-this-floating-point/

## Conclusion

*Your mileage may vary...*
