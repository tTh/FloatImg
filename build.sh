#!/bin/bash

set -e ; set -u

# ------------------------------------------------------------------

function build
{
echo "============= $1 =============="
curdir=${PWD}
cd $1
make 
error=$?
cd ${curdir}

if [ ${error} -ne 0 ]
then
	echo "=== error on $1 = ${error}"
	exit 1
fi
}

# ------------------------------------------------------------------

build lib
build funcs
build tools
build v4l2
build Fonderie

# ------------------------------------------------------------------

echo "========== Project root ==============="
make

# ------------------------------------------------------------------





