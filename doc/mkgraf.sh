#!/bin/bash

#
#	this script generate some picz for the PDF documentation
#	and was called by mkdoc.sh
#

PI=" 3.141592654 "

#	---------------------------------------------------

OUT="cos01.tex"

gnuplot << __EOF__

set term latex
set output "$OUT"
set title "COS01"
set xlabel "input value"

set ylabel '\rotatebox{90}{corrected value}' 

set grid

plot					\
	[0:1] [0:1]			\
	(0.5 - 0.5 * cos(x*$PI))	\
	with line

__EOF__

wc $OUT

#	---------------------------------------------------

OUT="cos010.tex"

gnuplot << __EOF__

set term latex
set output "$OUT"
set title "COS010"
set xlabel "input value"

set ylabel '\rotatebox{90}{corrected value}' 

set grid

plot					\
	[0:1] [0:1]			\
	(0.5 - 0.5 * cos(x*$PI*2))	\
	with line

__EOF__

wc $OUT

#	---------------------------------------------------
