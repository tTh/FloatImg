# Images en virgule flottante, les outils.

Dans tous les cas, vous pouvez utiliser l'option `-v`
(qui est bien mieux en premier sur la ligne de commande)
pour suivre l'avancée des travaux et l'option `-h` pour
avoir des explications sur ce que vous pouvez faire, 

## mkfimg

Génération d'une image flottante avec des choses dedans.
Un [../scripts/demo-mkfimg.sh](script) permet de créer toutes
les images disponibles.

L'option `-m` rajoute des méta-données, cette option 
**ne** doit **pas** encore être utilisée dans la vrai vie, ymmv.

## cumulfimg

Procède à l'accumulation de plusieurs images flottantes,
c'est-à-dire en faire la moyenne.
Le nom par défaut du fichier résultant est `out.fimg`, mais peut
être changé avec l'option `-o`. Avec `-g` l'image de sortie
sera en niveau de gris.

Ce logicel gagnerait peut-être à utiliser un accumulateur
intermédiaire en double précision pour les cas limite,
ou une [méthode](https://orlp.net/blog/taming-float-sums/)
plus adaptée.



## fimgops
```
usage:
        fimgops [options] A.fimg B.fimg operator D.fimg
options:
        -k N.N          set float value (def=0.500)
        -v              increase verbosity
```

## fimgfx

Available effects:

        cos01 cos010 pow2 sqrt gray0 cmixa xper desat ctr2x2 mirror 
        shift0 trimul classtrial binarize trinarize hilightr 
        abs clamp fakol0 morph0 morph4 morph8 thermocol

Un [../scripts/demo-fimgfx.sh](script) permet de créer toutes
les images avec les effets disponibles.
Vous vous rendrez vite compte que certains ne semblent rien faire,
mais regardez plus attentivement...

## fimgstats

Compute some useless numbers, specially for the enduser...

## fimg2pnm - fimg2png - fimg2tiff - fimg2fips

Exportation d'image flottante vers divers (et trop rares) formats.
Certains d'entre eux ne sont gérés que de façon très rudimentaire.

## fimg2text

Nouveau de l'année 2020+1 : exfiltrer toutes des données d'une image flottante
afin de les rendre machinables. Voir aussi *fimgmetadata*.

```
tth@redlady:~/Devel/FloatImg/tools$ ./fimg2text bar.fimg | head -5
0 0 123.701111 269.273682 10.732136
1 0 109.500832 265.568481 26.176439
2 0 128.529694 292.033844 37.591236
3 0 98.910492 234.900467 17.132524
4 0 187.679092 386.155609 36.663834
```

## fimgmetadata

Affichage des informations diverses stockées dans un fichier `.fimg`,
comme la date de prise de vue ou les réglages du logiciel
de capture.

Expect problems with `time_t` on 32 vs. 64 bits. Fix in
progress.

## fimgextract

En avril 2024, il sert à tester une nouvelle fonctionnalité :
la recopie des méta-données de l'image source.

    $ fimgextract [options] source.fimg width,height,xpos,ypos

Les options utiles : `-o out.fimg` pour nommer le fichier de
sortie, et `-m` qui demande la recopie des metadatas. 

