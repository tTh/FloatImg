/*
 *		FIMGSTATS
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>

#include  "../floatimg.h"

int		verbosity;		/* global */
int		make_csv;

/* --------------------------------------------------------------------- */
int machinable(FloatImg *fimg)
{
float		vals[6];
float		means[4];

if (verbosity) {
	fprintf(stderr, "%s  numbers from %p :\n", __func__, fimg);
	}

fimg_meanvalues(fimg, means);
fimg_get_minmax_rgb(fimg, vals);
printf( "%.3f %.3f %.3f   %.3f %.3f %.3f   %.3f %.3f %.3f\n",
	vals[0], means[0], vals[1],
	vals[2], means[1], vals[3],
	vals[4], means[2], vals[5]);

return 0;
}
/* --------------------------------------------------------------------- */
int various_numbers(FloatImg *fimg)
{
float		moyennes[4];
int		foo;
float		vals[6];

if (verbosity) {
	fprintf(stderr, "%s from %p :\n", __func__, fimg); 
	}

fimg_printhead(fimg);
fprintf(stderr, "surface      %d\n", fimg->width * fimg->height);

fimg_meanvalues(fimg, moyennes);
fprintf(stderr, "mean values:\n");
for (foo=0; foo<4; foo++)
	printf("      %c   %14.6f\n", "RGBA"[foo], moyennes[foo]);

foo = fimg_count_negativ(fimg);
if (foo) {
	fprintf(stderr, "%d negative values\n", foo);
	}

foo = fimg_get_minmax_rgb(fimg, vals);
if (foo) {
	fprintf(stderr, "%s: err %d on fimg_get_minmax_rgb\n",
			__func__, foo);
	return foo;
	}

printf("Rmin  %12.6g    Rmax  %12.6g    delta %12.6g\n",
			vals[0], vals[1], vals[1]-vals[0]);
printf("Gmin  %12.6g    Gmax  %12.6g          %12.6g\n",
			vals[2], vals[3], vals[3]-vals[2]);
printf("Bmin  %12.6g    Bmax  %12.6g          %12.6g\n",
			vals[4], vals[5], vals[5]-vals[4]);

return 0;
}
/* --------------------------------------------------------------------- */
int various_numbers_from_file(char *fname, int k)
{
FloatImg	fimg;
int		foo;

if (verbosity) {
	fprintf(stderr, "------ numbers from '%s' :\n", fname); 
	}

foo = fimg_create_from_dump(fname, &fimg);
if (foo) {
	fprintf(stderr, "create fimg from '%s' -> %d\n", fname, foo);
	return -2;
	}

if (k) {
	foo = machinable(&fimg);
	}
else	{
	foo = various_numbers(&fimg);
	}
fimg_destroy(&fimg);
if (foo) {
	fprintf(stderr, "%s: got a %d\n", __func__, foo);
	return foo;
	}
return 0;
}
/* --------------------------------------------------------------------- */
static void help(int k)
{
fputs(	"usage : fimgstats [options] file.fimg\n"
	"\t-c\tmake a machinable csv\n"
	"\t-v\tincrease verbosity\n"
	, stderr);
if (k)	{
	fimg_print_version(k);
	}
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;
int		do_machinable = 0;
extern char	*optarg;
extern int	optind, opterr, optopt;

if (argc == 1) {
	foo = fimg_print_version(1); help(0);
	exit(0);
	}

while ((opt = getopt(argc, argv, "chmv")) != -1) {
	switch(opt) {
		case 'c':	make_csv++;		break;
		case 'v':	verbosity++;		break;
		case 'm':	do_machinable = 1;	break;
		case 'h':	/* tombe dedans */
		default:	help(1);		exit(1);
		}
	}

if (NULL==argv[optind]) {
	fprintf(stderr, "optind %d is wtf\n", optind);
	return 1;
	}
foo = various_numbers_from_file(argv[optind], do_machinable);
if (foo) {
	fprintf(stderr, "got a %d ?\n", foo);
	}
return 0;
}
/* --------------------------------------------------------------------- */
