/*
 *	conversion vers le format PNM 
 *
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>

#include  "../floatimg.h"

int		verbosity;

/* --------------------------------------------------------------------- */
int convertir_fimg_en_pnm(char *srcname, char *dstname, int to_gray)
{
int		foo, infos[3];
FloatImg	fimg, gris, *outptr;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %25s ( '%s' '%s' %d )\n", __func__,
			srcname, dstname, to_gray);
#endif

foo = fimg_fileinfos(srcname, infos);
if (foo) { fprintf(stderr, "'%s' get dims -> %d\n", srcname, foo); }

if (verbosity) {
	fprintf(stderr, "image '%s' is %d x %d  %s\n",
				srcname, infos[0], infos[1],
				fimg_str_type(infos[2]));
	}

foo = fimg_create_from_dump(srcname, &fimg);
if (foo) {
	fprintf(stderr, "create fimg from '%s' -> %d\n", srcname, foo);
	return -1;
	}

outptr = &fimg;		/* safe default value */

if (to_gray) {
	if (verbosity) puts("converting to gray...");
	foo = fimg_create(&gris, fimg.width, fimg.height, FIMG_TYPE_GRAY);
	if (foo) {
		fprintf(stderr, "err create gray %d\n", foo);
		return -2;
		}
	foo = fimg_mk_gray_from(&fimg, &gris, 0);
	if (foo) {
		fprintf(stderr, "err mk gray %d\n", foo);
		return -4;
		}
	outptr = &gris;
	}

#if DEBUG_LEVEL > 1
print_floatimg(outptr, "created fimg");
#endif

foo = fimg_save_as_pnm(outptr, dstname, 0);
if(foo) { fprintf(stderr, "%p to '%s' -> %d\n", &fimg, dstname, foo); }

if (to_gray) {
	fimg_destroy(&gris);
	outptr = NULL;
	/* please run valgrind every hour */
	}

return 0;
}
/* --------------------------------------------------------------------- */
static void help(int flag)
{
if (flag) {
	fprintf(stderr, "conversion FIMG -> PNM 16 bits\n");
	fimg_print_version(1);
	}
puts("usage :");
puts("\tfimg2pnm [flags] infile.fimg outfile.pnm");
puts("flags :");
puts("\t-g\tconvert to gray");
puts("\t-v\tenhance your verbosity");
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt;
int		to_gray = 0;

if (argc == 1) {
	help(0);
	exit(0);
	}

while ((opt = getopt(argc, argv, "ghv")) != -1) {
	switch(opt) {
		case 'g':	to_gray = 1;		break;
		case 'v':	verbosity++;		break;
		case 'h':	help(1);		exit(1);
		default:	exit(1);
		}
	}

#if DEBUG_LEVEL
/* mmmm, is it the good way ? */
printf("argc %d -> %d\n", argc, argc-optind);
for (foo=optind; foo<argc; foo++) {
	printf("   %d  %s\n", foo, argv[foo]);
	}
#endif

if (2 != argc-optind) {
	fprintf(stderr, "error: %s need two filenames\n", argv[0]);
	exit(1);
	}

if ( 0 != access(argv[optind], R_OK) ) {	/* fimg is NOT readable */
	fprintf(stderr, "%s: %s don't exist.\n", argv[0], argv[optind]);
	exit(2);
	}

foo = convertir_fimg_en_pnm(argv[optind], argv[optind+1], to_gray);
if (foo) fprintf(stderr, "conversion -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */
