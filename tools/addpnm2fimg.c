/*
 *			ADDPNM
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>

#include  "../floatimg.h"

int		verbosity;

/* --------------------------------------------------------------------- */
int add_pnm_to_fimg(char *srcname, char *dstname, int notused)
{
FloatImg	dst, src;
int		foo, x, y, idx;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %s' )\n", __func__, srcname, dstname);
#endif

if (notused) {
	fprintf(stderr, "in %s, notused must be 0, was %d !\n", 
			    __func__,			notused);
	}

foo = fimg_create_from_dump(dstname, &dst);
if (foo) fprintf(stderr, "create dst fimg from '%s' -> %d\n", dstname, foo);
#if DEBUG_LEVEL
fimg_describe(&dst, "created fimg from dump");
#endif

foo = fimg_load_from_pnm(srcname, &src, 0);
if (foo) fprintf(stderr, "create src fimg from '%s' -> %d\n", dstname, foo);
#if DEBUG_LEVEL
fimg_describe(&src, "created fimg from PNM");
#endif

// fprintf(stderr, "src is %dx%d\n", src.width, src.height); 

idx = 0;
for (y=0; y<src.height; y++) {
	for (x=0; x<src.width; x++) {
		dst.R[idx] += src.R[idx];
		dst.G[idx] += src.G[idx];
		dst.B[idx] += src.B[idx];
		idx++;
		}
	}

foo = fimg_dump_to_file(&dst, dstname, 0);
if (foo) { fprintf(stderr, "fimg dump -> %d\n", foo); }

return 0;
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo;
// int		srcW, srcH;
int		infos[3];

if (3 != argc) {
	fimg_print_version(1);
	fprintf(stderr, "usage:\n\t%s img.fimg cumul.fimg\n", argv[0]);
	exit(1);
	}

verbosity = 0;

if ( 0==access(argv[2], R_OK|W_OK) ) {		/* fimg is readable */
	// fprintf(stderr, "%s exist\n", argv[2]);
	}
/*
else	{
	fprintf(stderr, "*** must create '%s' %dx%d first !!!\n",
					argv[2], tgaW, tgaH);
	exit(1);
	} */

foo = fimg_fileinfos(argv[2], infos);
// fprintf(stderr, "get dims of '%s' -> %d\n", argv[2], foo);
if (foo) {
	fprintf(stderr, "*** %s is badly broken\n", argv[2]);
	exit(2);
	}

foo = add_pnm_to_fimg(argv[1], argv[2], 0);

return 0;
}
/* --------------------------------------------------------------------- */


