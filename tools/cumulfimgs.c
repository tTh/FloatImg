/*
 *		This thing is just a mess !
 *		***************************
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <stdint.h>
#include  <string.h>

#include  "../floatimg.h"

int		verbosity;
int		g_width, g_height;

/* --------------------------------------------------------------------- */
int testfile(char *path)
{
int		foo, numbers[3];

foo = fimg_fileinfos(path, numbers);
if (foo) {
	fprintf(stderr, "fileinfo of '%s' -> err %d\n", path, foo);
	return foo;
	}

if (verbosity) {
	fprintf(stderr, "%-20s  %dx%d\n", path, numbers[0], numbers[1]);
	}

if (FIMG_TYPE_RGB != numbers[2]) {
	fprintf(stderr, "file %s, %d : bad type\n", path, numbers[2]);
	return -7;
	}

return 0;
}
/* --------------------------------------------------------------------- */
void help(int v)
{
puts("");
puts("$ cumulfimgs a.fimg b.fimg c-fimg ...");
puts("cumulator options :");
puts("\t-a\tcompute average");
puts("\t-g\tconvert to gray level");
puts("\t-m\tcompute the max of the maxes");
puts("\t-o\tname of output file");
puts("\t-s\trescale end image");
puts("\t-v\tincrease verbosity");
puts("");
fimg_print_version(v);
exit(0);
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, idx;
int		opt;

int		compte		= 0;
int		to_gray		= 0;
int		experiment	= 0;
int		rescale		= 0;
int		src_loaded	= 0;
int		minmax		= 0;

char		*output_file	= "out.fimg";
FloatImg	accu, temp;
float		vals[3];

g_width = g_height = 0;

while ((opt = getopt(argc, argv, "ghmo:svx")) != -1) {
	switch(opt) {
		case 'g':	to_gray = 1;			break;
		case 'h':	help(1);			break;
		case 'm':	minmax++;			break;
		case 'o':	output_file = optarg;		break;
		case 's':	rescale = 1;			break;
		case 'v':	verbosity++;			break;
		case 'x':	experiment++;			break;
		default:	exit(1);
		}
	}

if (verbosity)		fprintf(stderr, "------ cumulfimgs ------\n");
#if DEBUG_LEVEL
fprintf(stderr, "argc = %d, optind = %d\n", argc, optind);
#endif

memset(vals, 0, 3*sizeof(float));

for (idx=optind; idx<argc; idx++) {
#if DEBUG_LEVEL 
	fprintf(stderr, "----------------  %s\n", argv[idx]);
	fflush(stderr);
#endif
	foo = testfile(argv[idx]);
	if (foo) {
		fprintf(stderr, "testfile %s -> %d\n", argv[idx], foo);
		exit(1);
		}

	/*
	 *	load the picture, create a clone of the first
	 */
	if ( ! src_loaded ) {
		foo = fimg_create_from_dump(argv[idx], &accu);
		if (foo) {
			fprintf(stderr, "create from dump -> %d\n", foo);
			exit(1);
			}
		fimg_clone(&accu, &temp, 0);
		src_loaded = 1;
		}
	else	{
		foo = fimg_load_from_dump(argv[idx], &temp);
		if (foo) {
			fprintf(stderr, "load from dump -> %d\n", foo);
			exit(1);
			}
		}

	if (minmax) {
		/*
		 * print the maximum values and compute the
		 * maximum of all the maximums
		 */
		foo = fimg_max_of_max(&temp, vals);
		// fprintf(stderr, "%5d max of max %9.3f  %9.3f  %9.3f\n",
		//			idx, vals[0], vals[1], vals[2]);
		}

	fimg_add_2(&temp, &accu);

	compte++;
	}

if (rescale) {
	fprintf(stderr, "cumulfimg: count = %d\n", compte);
	fimg_div_cste(&accu, (float)compte);
	}


fprintf(stderr, "max of max  %9.3f   %9.3f   %9.3f\n",
				vals[0], vals[1], vals[2]);


if (to_gray) {
	foo = fimg_desaturate(&accu, &accu, 0);
	/* ?
	 * but here, final picture is in RGB shape, so export give us
	 * also an image rgb-shaped. this is not optimal, I think.
	 */
	if (foo) {
		fprintf(stderr, "desaturate: error %d\n", foo);
		}
	}

/* PLEASE CHECK IF EXPORT TO GRAY MAKE A REAL .pgm FILE */

foo = fimg_export_picture(&accu, output_file, 0);
if (foo) {
	fprintf(stderr, "error %d while saving '%s'\n", foo, output_file);
	exit(1);
	}

if (verbosity) {
	/* show some numbers about resultant picture */
	foo = fimg_get_minmax_rgb(&accu, vals);
	if (foo) {
		fprintf(stderr, "err %d on fimg_get_minmax_rgb\n", foo);
		return foo;
		}
	printf("Count   %d\n", compte);
	printf("Rmin  %12.4g    Rmax  %12.4g    delta %12g\n",
				vals[0], vals[1], vals[1]-vals[0]);
	printf("Gmin  %12.4g    Gmax  %12.4g          %12g\n",
				vals[2], vals[3], vals[3]-vals[2]);
	printf("Bmin  %12.4g    Bmax  %12.4g          %12g\n",
				vals[4], vals[5], vals[5]-vals[4]);
	}

fimg_destroy(&accu);	fimg_destroy(&temp);

return 0;
}
/* --------------------------------------------------------------------- */
