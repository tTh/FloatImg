/*
			FIMGFILTERS
			===========
 new: Sun Oct  8 05:51:05 UTC 2023
*/

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <time.h>

#include  "../floatimg.h"

int		verbosity;

/* --------------------------------------------------------------------- */
int filtre_image(char *infname, char *outfname, int type)
{
FloatImg	src, dst;
int		foo;

static FimgFilter3x3   filtre = {
                {
                2.0,    3.0,    2.0,
                3.0,    4.0,    3.0,
                2.0,    3.0,    2.0,
                },
        1.0/24.0,   0.0
        };


#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s' %d )\n", __func__, infname, outfname, type);
fimg_show_filter(NULL, &filtre);
#endif

if (0 != type) {
	fprintf(stderr, "%s: bad type %d\n", __func__, type);
	}

if ((foo = fimg_create_from_dump(infname, &src))) {
	fprintf(stderr, "read error on '%s' is %d\n", infname, foo);
	exit(2);
	}
if ((foo = fimg_clone(&src, &dst, 0))) {
	fprintf(stderr, "clone error on %p is %d\n", &src, foo);
	exit(3);
	}

foo = fimg_filter_3x3(&src, &dst, &filtre);
if (foo) {
	fprintf(stderr, "%s: filtre -> %d\n", __func__, foo);
	exit(4);
	}

foo = fimg_dump_to_file(&dst, outfname, 0);
if (foo) {
	fprintf(stderr, "dumping to file give us a %d\n", foo);
	exit(5);
	}

return 0;
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo;

if (3 != argc) {
	fprintf(stderr, "usage: %s in.fimg out.fimg\n", argv[0]);
	exit(1);
	}

fprintf(stderr, "    +++ %s +++\n", argv[0]);

foo = filtre_image(argv[1], argv[2], 0);
fprintf(stderr, "    filtrage -> %d\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */

