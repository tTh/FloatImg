#!/bin/bash

SRC="../lib/quux.fimg"
K="0.414"
TMP="/tmp/foo.fimg"

for fx in $(./fimgfx -l)
do

	printf "==========   %-12s =========\n" $fx

	outfile=$(printf "/tmp/fx_%s.png" $fx)
	echo $outfile

	rm $TMP
	./fimgfx -v -k $K $fx $SRC $TMP
	if [ $? \!= 0 ] ; then
		echo error $?
		exit 1
	fi

	./fimg2png -v $TMP $outfile

	echo ; sleep 1

done
