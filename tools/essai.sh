#!/bin/bash

#
#	essai des differents outils
#

#  -----------------------------------------------------

TMPF="bar.fimg"

#  -----------------------------------------------------
function essai_metadata
{
echo "     ____ essai metadata ________"
./mkfimg -v -m -t hdeg $TMPF 800 600
echo ; echo
./fimgstats $TMPF
echo ; echo
./fimgmetadata all $TMPF
}
#  -----------------------------------------------------
function essai_rot90
{
echo "=== " $0 " ==="
./mkfimg -v -t hdeg $TMPF 800 600
./fimg2png -v $TMPF foo.png
./fimgfx -v rot90 $TMPF bar.fimg
}
#  -----------------------------------------------------
function essai_drand48
{
./mkfimg -vv -t drand48 $TMPF 800 600
./fimgstats -vv $TMPF
}
#  -----------------------------------------------------
function essai_cmixa
{
./mkfimg -vv -t tpat0 $TMPF 800 600
./fimgfx -vv cmixa $TMPF bar.fimg
}
#  -----------------------------------------------------
function essai_cumulator
{
D=" 800 600 "
./mkfimg -v -t hdeg	hdeg.fimg $D
./mkfimg -v -t vdeg	vdeg.fimg $D
./mkfimg -v -t drand48	rand.fimg $D
./mkfimg -v -t tpat0	pat0.fimg $D

./cumulfimgs -v -g -o foo.fimg			\
	hdeg.fimg pat0.fimg rand.fimg vdeg.fimg
}
#  -----------------------------------------------------
function essai_contour
{
./mkfimg -vv -t tpat0 $TMPF 800 600
./fimgfx -vv ctr2x2 $TMPF bar.fimg
}
#  -----------------------------------------------------
function essai_hilight
{
./fimgfx -vv hilightr $TMPF bar.fimg
}
#  -----------------------------------------------------

essai_metadata

#  -----------------------------------------------------
