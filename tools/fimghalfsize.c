/*
 * halfsizing an fimg picture.
*/

#include  <stdio.h>
#include  <unistd.h>			/* pour getopt */
#include  <stdint.h>
#include  <stdlib.h>
#include  <string.h>


#include  "../floatimg.h"

int		verbosity;

/* ------------------------------------------------------------- */
int faire_un_halfsize(char *iname, char *oname, int to_gray)
{
FloatImg	src, dst;
int		foo;

foo = fimg_create_from_dump(iname, &src);
if (foo) {
	fprintf(stderr, "create fimg from '%s' -> %d\n", iname, foo);
	return -1;
	}

memset(&dst, 0, sizeof(FloatImg));
foo = fimg_halfsize_1(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "halfize 1 fail -> %d\n", foo);
	return -1;
	}

if (to_gray) {
	foo = fimg_to_gray(&dst);
	/* and ? */
	}

foo = fimg_dump_to_file(&dst, oname, 0);
if (foo) {
	fprintf(stderr, "save to '%s' -> %d\n", oname, foo);
	return -1;
	}

return 0;
}

/* ------------------------------------------------------------- */
void help(int u)
{
puts("Usage:\n\tfimghalfsize [options]  in.fimg out.fimg");
puts("Options:");
puts("\t-g\tconvert output to gray");
fimg_print_version(u);
exit(0);
}
/* ------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo, opt;

char		*srcname = "";
char		*dstname = "out.fimg";
int		grayed = 0;

while ((opt = getopt(argc, argv, "ghv")) != -1) {
	switch(opt) {
		case 'g':	grayed = 1;			break;
		case 'h':	help(0);			break;
		case 'v':	verbosity++;			break;
		default:	exit(1);
		}
	}

if (2 != argc-optind) {
	fprintf(stderr, "error: %s need two filenames\n", argv[0]);
	exit(1);
	}

srcname = argv[optind];
dstname = argv[optind+1];

if (verbosity) {
	fprintf(stderr, "%s:  src: %s  dst: %s\n", argv[0],
				 srcname, dstname);
	}

foo = faire_un_halfsize(srcname, dstname, grayed);
if (foo) {
	fprintf(stderr, "in %s: make halfsize give a %d\n", argv[0], foo);
	exit(1);
	}

return 0;
}
/* ------------------------------------------------------------- */
