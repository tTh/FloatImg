/*
 *		This thing is just a mess !
 *		***************************
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <stdint.h>
#include  <string.h>

#include  "../floatimg.h"

int		verbosity;		// nasty global var.

/* --------------------------------------------------------------------- */
/* nouveau - Mon Apr  8 11:52:18 UTC 2024 */
int copy_metadata(FloatImg *src, FloatImg *dst)
{
FimgMetaData	*mdsrc, *mddst;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, src, dst);
#endif

mdsrc = &(src->mdatas);    mddst = &(dst->mdatas);
if (verbosity) fprintf(stderr, "%s:  %p --> %p\n", __func__, mdsrc, mddst);

mdsrc->reserved[1] = 0x55555555;
memcpy(mddst, mdsrc, sizeof(FimgMetaData));

return 0;
}
/* --------------------------------------------------------------------- */
/* nouveau ~ 2 octobre 2022 */
int extractor(char *srcname, char *dstname, FimgArea51 *rect)
{
FloatImg	src, dst;
int		foo;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %s  %s  %p )\n", __func__, srcname, dstname, rect);
#endif

if (verbosity) {
	print_rectangle((char *)__func__, rect);
	}

foo = fimg_create_from_dump(srcname, &src);
if (foo) {
	fprintf(stderr, "%s: load %s from dump --> %d\n", __func__,
					srcname, foo);
	return foo;
	}

foo = fimg_create(&dst, rect->w, rect->h, 3);
if (foo) {
	fprintf(stderr, "%s: fimg create dst --> %d\n", __func__, foo);
	return foo;
	}

/* REAL operation was here ! */
foo = fimg_extractor(&src, &dst, rect);
if (foo) {
	fprintf(stderr, "%s: fimg extractor --> %d\n", __func__, foo);
#ifdef MUST_ABORT
	abort();		// kill me hardly !
#endif
	return foo;
	}

/*	XXX
 *	may be we can also copy the metadate from src to dst ?
 *	with an option on the command line ?
 */
foo = copy_metadata(&src, &dst);

foo = fimg_dumpmd_to_file(&dst, dstname, NULL, 0);
if (foo) {
	fprintf(stderr, "%s: dumping datas to '%s' give us a %d\n",
			__func__, dstname, foo);
	return foo;
	}

return 0;
}
/* --------------------------------------------------------------------- */
void help(void)
{

printf("-- Fimg Extractor -- lib v%d -- %s %s\n", FIMG_VERSION,
					__DATE__, __TIME__);

puts("usage:\n\tfimgextract [options] source.fimg width,height,xpos,ypos");
puts("options:");
puts("\t-m\t\tcopy metadata bloc");
puts("\t-o out.fimg\tname the output file");
puts("\t-v\t\tmake me a blabla box");
puts("\t-x\t\tenable crashy feature");
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo, idx;
int		opt;
int		experiment = 0;
FimgArea51	area;
char		*output_file	= "out.fimg";

while ((opt = getopt(argc, argv, "ho:vx")) != -1) {
	switch(opt) {
		case 'h':	help();				break;
		case 'm':					break;
		case 'o':	output_file = optarg;		break;
		case 'v':	verbosity++;			break;
		case 'x':	experiment++;			break;
		default:	exit(1);
		}
	}

if (verbosity) {
	fprintf(stderr, "argc = %d  optind = %d\n", argc, optind);
	for (idx=optind; idx<argc; idx++) {
		fprintf(stderr, "   %5d   %s\n", idx, argv[idx]);
		}
	}

if (argc==optind) {
	fprintf(stderr, "what the fsck ?\n");
	exit(0);
	}

foo = parse_rectangle(argv[optind+1], &area, 0);
if (4 != foo) {
	fprintf(stderr, "%s: parse_rectangle --> %d\n", argv[0], foo);
	exit(1);
	}

foo = extractor(argv[optind], output_file, &area);
if (foo) {
	fprintf(stderr, "%s: extractor --> %d\n", __func__, foo);
	exit(1);
	}

return 0;
}
/* --------------------------------------------------------------------- */
