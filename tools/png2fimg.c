/*
 *		PNG ---> FIMG
 *
 *	Attention : certains fichiers PNG ne passent pas cette
 *	moulinette, mais le bug est dans la bibliotheque de
 *	fonctions 'libpnglite'. Une solution de remplacement
 *	devrait etre a l'etude un de ces jours...
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>
#include  <unistd.h>
#include  <string.h>

#include  "../floatimg.h"

int		verbosity = 0;

/* --------------------------------------------------------------------- */
void help(int k)
{
if (verbosity) fimg_print_version(k);

exit(0);
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
FloatImg	fimg;
int		foo, opt;

while ((opt = getopt(argc, argv, "hv")) != -1) {
	switch(opt) {
		case 'v':	verbosity++;		break;
		case 'h':	help(1);		exit(1);
		default:	exit(1);
		}
	}

if (2 != argc-optind) {
	fprintf(stderr, "error: %s need two filenames\n", argv[0]);
	exit(1);
	}

memset(&fimg, 0, sizeof(FloatImg));

foo = fimg_create_from_png(argv[optind], &fimg);
if (foo) {
	fprintf(stderr, "%s : err %d, abort.\n", argv[0], foo);
	exit(1);
	}

if (verbosity)	fimg_describe(&fimg, argv[optind+1]);

foo = fimg_dump_to_file(&fimg, argv[optind+1], 0);
if (foo) {
	fprintf(stderr, "save as '%s' -> err %d\n", argv[2], foo);
	exit(1);
	}

return 0;
}
/* --------------------------------------------------------------------- */
