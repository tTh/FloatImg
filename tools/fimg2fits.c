/*
 *	exporting a floatimg to a FITS file
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <stdint.h>

#include  "../floatimg.h"

int		verbosity;

/* ----------------------------------------------------------------- */
int export_fimg_plane_as_fits(char *infile, char *outfile, char plane)
{
FloatImg	fimg;
int		foo;

foo = fimg_create_from_dump(infile, &fimg);
if (foo) {
	fprintf(stderr, "%s: create fimg from '%s' -> %d\n", __func__,
						infile, foo);
	return -1;
	}

foo = fimg_save_plane_as_fits(&fimg, outfile, plane, 0);
if (foo) {
	fprintf(stderr, "%s: err %d on fits export\n", __func__, foo);
	return foo;
	}

fimg_destroy(&fimg);

return 0;
}
/* ----------------------------------------------------------------- */
static void help(int k)
{
puts("export to FITS format");
puts("\t-p  select colorplane : R, G, B");
puts("\t    default is blue");
puts("\t-v  increase verbosity");
fimg_print_version(k);
}
/* ----------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		opt, foo;
int		plane = 'b';

while ((opt = getopt(argc, argv, "p:hv")) != -1) {
	switch(opt) {
		case 'p':	plane = optarg[0];	break;
		case 'v':	verbosity++;		break;
		case 'h':	help(1);		exit(1);
		default:	exit(1);
		}
	}

if (2 != argc-optind) {
	fprintf(stderr, "error: %s need two filenames\n", argv[0]);
	exit(1);
	}

foo = export_fimg_plane_as_fits(argv[optind], argv[optind+1], plane);
if (foo) {
	fprintf(stderr, "export_fimg_plane_as_fits --> %d\n", foo);
	exit(1);
	}
return 0;
}
/* ----------------------------------------------------------------- */
