/*
 *		FIMG METADATA TOOL
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <time.h>

#include  "../floatimg.h"


/* --------------------------------------------------------------------- */
int		verbosity;

enum comId {	C_timestamp, C_daytime, C_count, C_fval, C_cpid, C_origin, 
		C_idcam, C_reserved, C_all, C_dayhour };

typedef struct {
	char		*name;
	int		id;
	} Cmd;

Cmd Cmd_list[] = {
	{ "timestamp",		C_timestamp	},
	{ "daytime",		C_daytime	},
	{ "dayhour",		C_dayhour	},
	{ "cpid",		C_cpid		},
	{ "count",		C_count		},
	{ "fval",		C_fval		},
	{ "origin",		C_origin	},
	{ "idcam",		C_idcam		},
	{ "reserved",		C_reserved	},
	{ "all",		C_all		},
	{ NULL,			0,		}
	};

/* --------------------------------------------------------------------- */
int lookup_cmd(char *txt)
{
Cmd		*cmd;
int		n;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, txt);
#endif

for (n=0, cmd=Cmd_list; cmd->name; cmd++, n++) {
#if DEBUG_LEVEL > 1
	fprintf(stderr, "      try %-12s    %3d\n", cmd->name, n);
#endif
	if (!strcmp(cmd->name, txt)) {
		// fprintf(stderr, "found '%s' at %d\n", txt, n);
		return cmd->id;
		}
	}
return -1;		/* NOT FOUND */
}
/* --------------------------------------------------------------------- */
void list_commands(void)
{
Cmd		*cmd;
int		n;

fputs("Commands:", stderr);
for (n=0, cmd=Cmd_list; cmd->name; cmd++) {
	n += fprintf(stderr, " %s", cmd->name);
	if (n > 40) {
		fputs("\n         ", stderr);
		n = 0;
		}
	}
fputs("\n", stderr);
}
/* --------------------------------------------------------------------- */
int show_reserved(FimgMetaData *pmd)
{
int foo;
for (foo=0; foo<8; foo++) printf("%08x ", pmd->reserved[foo]);
puts("");
return 0;
}
/* --------------------------------------------------------------------- */
int get_print_metadata(char *fname, char *command)
{
int		foo, action;
FimgMetaData	metadata;
time_t		tstamp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' '%s' )\n", __func__, fname, command);
#endif

foo = fimg_get_metadata_from_file(fname, &metadata);
if (foo)	return foo;

/* switch on command here */
action = lookup_cmd(command);
// fprintf(stderr, "    command '%s' -> %d\n", command, action);

tstamp = metadata.timestamp.tv_sec;

switch(action)	{
	case C_timestamp:
		printf("timestamp %ld\n", tstamp);		break;
	case C_daytime:
		printf("%s\n", ctime(&tstamp));			break;
	case C_dayhour:
		printf("%s\n", ctime(&tstamp));			break;
	case C_count:
		printf("count %d\n", metadata.count);		break;
	case C_fval:
		printf("fval %f\n", metadata.fval);		break;
	case C_cpid:
		printf("cpid %ld\n", metadata.cpid);		break;
	case C_origin:
		printf("origin 0x%x\n", metadata.origin);	break;
	case C_idcam:
		printf("camid \"%s\"\n", metadata.idcam);	break;
	case C_reserved:
		show_reserved(&metadata);			break;
	case C_all:
		fimg_show_metadata(&metadata, fname, 0);	break;
	default:
		fprintf(stderr, "%s WTF?\n", __func__);		exit(1);
	}

return 0;
}
/* --------------------------------------------------------------------- */
void help(void)
{
fprintf(stderr, "*** Fimg Metadata Reader (%s, %s)\n", __DATE__, __TIME__);
fimg_print_version(1);
list_commands();
exit(0);
}
/* --------------------------------------------------------------------- */

int main(int argc, char *argv[])
{
int		foo, opt, nbargs;

while ((opt = getopt(argc, argv, "hv")) != -1) {
	switch(opt) {
		case 'h':	help();				break;
		case 'v':	verbosity++;			break;
		default:	exit(1);
		}
	}

nbargs = argc - optind;
// fprintf(stderr, "nbargs = %d\n", nbargs);
if (2 != nbargs) {
	fprintf(stderr, "%s need two args: command & filename\n", argv[0]);
	exit(1);
	}

foo = get_print_metadata(argv[optind+1], argv[optind]);
if (foo) fprintf(stderr, "got a %d from job\n", foo);

return 0;
}
/* --------------------------------------------------------------------- */

