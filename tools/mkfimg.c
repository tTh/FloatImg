/*
 *	making a floatimg with some random datas
 *	an ugly software from tTh - february 2021
 */

#include  <stdio.h>
#include  <string.h>
#include  <stdint.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <time.h>

#include  "../floatimg.h"

int		verbosity;

/* --------------------------------------------------------------------- */
#define		T_BLACK		1
#define		T_DRAND48	2
#define		T_GRAY		3
#define		T_HDEG_A	4
#define		T_VDEG_A	5
#define		T_TPAT0		6
#define		T_MIRCOL1	7
#define		T_BLOUP		8
#define		T_STRIPES	9
typedef struct {
	int		code;
	char		*name;
	} Type;

Type types[] = {
	{ T_BLACK,	"black"			},
	{ T_DRAND48,	"drand48"		},
	{ T_GRAY,	"gray"			},
	{ T_GRAY,	"grey"			},
	{ T_HDEG_A,	"hdeg"			},
	{ T_VDEG_A,	"vdeg"			},
	{ T_TPAT0,	"tpat0"			},
	{ T_MIRCOL1,	"mircol1"		},
	{ T_BLOUP,	"bloup"			},
	{ T_STRIPES,	"stripes"		},
	{ 0,		NULL			}
	};

static int get_type_by_name(char *name)
{
Type	*type;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' )\n", __func__, name);
#endif

for (type = types; type->code; type++) {
	if (!strcmp(name, type->name)) {
		return type->code;
		}
	}
return -1;
}
/* --------------------------------------------------------------------- */
static void list_types(void)
{
Type	*type;

for (type = types; type->code; type++) {
	puts(type->name);
	}
exit(0);
}
/* --------------------------------------------------------------------- */
static void help(char *texte)
{
int		foo, cc;

puts("Usage:\tmkfimg [options]  quux.fimg width height");

puts("\t-k N.N\tgive a float multiplier");
puts("\t-L\tlist howto make a pic");
puts("\t-m\tadd faked metadata");

fputs("\t-t bla\thowto make the pic :\n\t\t  | ", stdout);
for (foo=cc=0; types[foo].code; foo++) {
	cc += printf("%s ", types[foo].name);
	if (cc>35) { cc=0; printf("\n\t\t  | "); }
	}

puts("\n\t-v\tincrease verbosity");

if (verbosity) {
	fimg_print_version(1);
	printf("*** %s compiled  %s, %s\n", texte, __DATE__, __TIME__);
	}

exit(0);
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo, opt, nbargs;
int		width, height;
char		*fname;
float		fvalue = 1.0;
int		type = T_BLACK;
int		wrmdata = 0;
char		*tname = "wtf?";

FloatImg	fimg;
FimgMetaData	metadata;

while ((opt = getopt(argc, argv, "hk:Lmt:v")) != -1) {
	switch(opt) {
		case 'h':	help(argv[0]);			break;
		case 'k':	fvalue = atof(optarg);		break;
		case 'm':	wrmdata = 1;			break;
		case 't':	type = get_type_by_name(tname=optarg);
				break;
		case 'L':	list_types();			break;
		case 'v':	verbosity++;			break;
		default:	exit(1);
		}
	}

#if DEBUG_LEVEL
fprintf(stderr, "argc %d optind %d\n", argc, optind);
for (foo=0; foo<argc; foo++)
	fprintf(stderr, "%3d  %s\n", foo, argv[foo]);
#endif

if (type < 0) {
	fprintf(stderr, "%s: type '%s' is unknow\n", argv[0], tname);
	exit(2);
	}

nbargs = argc-optind;
switch (nbargs) {
	case 2:
		if (2!=parse_WxH(argv[optind+1], &width, &height)) {
			fprintf(stderr, "%s: parse error on '%s'\n",
					argv[0], argv[optind+1]);
			exit(1);
			}
		break;
	case 3:
		width = atoi(argv[optind+1]);
		height = atoi(argv[optind+2]);
		break;
	default:
		fprintf(stderr, "%s need filename, width & height\n",
							argv[0]);
		exit(1);
	}

fname = argv[optind];

if (verbosity)   fprintf(stderr, "::: %s is making  '%s'  %dx%d, type %d\n",
					argv[0], fname, width, height, type);

srand48(getpid() ^ time(NULL));

foo = fimg_create(&fimg, width, height, 3);
if (foo) {
	fprintf(stderr, "create floatimg -> %d\n", foo);
	exit(3);
	}

/* NO ERROR CONTROL ? WTF ? */

switch(type) {
	default:
	case T_BLACK:		fimg_clear(&fimg);			break;
	case T_DRAND48:		fimg_drand48(&fimg, fvalue);		break;
	case T_GRAY:		fimg_rgb_constant(&fimg, fvalue, fvalue, fvalue);
									break;
	case T_HDEG_A:		fimg_hdeg_a(&fimg, fvalue);		break;
	case T_VDEG_A:		fimg_vdeg_a(&fimg, fvalue);		break;
	case T_TPAT0:		fimg_test_pattern(&fimg, 0, fvalue);	break;
	case T_MIRCOL1:		fimg_mircol_1(&fimg, fvalue);		break;
	case T_BLOUP:		fimg_draw_something(&fimg);		break;
	case T_STRIPES:		fimg_do_stripes(&fimg, fvalue, 0);	break;
	case -1:		exit(1);
	}



if (verbosity > 1) fprintf(stderr, "%s: end of switch\n", __func__);

/* NO ERROR CONTROL ? WTF ? */

if (wrmdata) {
	fprintf(stderr, "%s: warning, metadata is bogus\n", argv[0]);
	(void)fimg_default_metadata(&metadata, 9);
	sprintf(metadata.idcam, "mkfimg (libv %d)", FIMG_VERSION);
	foo = fimg_dumpmd_to_file(&fimg, fname, &metadata, 0);
#if DEBUG_LEVEL
	fprintf(stderr, "save w. metadata -> %d\n", foo);
#endif
	}
else	{
	foo = fimg_dump_to_file(&fimg, fname, 0);
	}

if (foo) {
	fprintf(stderr, "*** dump fimg to %s error -> %d\n", fname, foo);
	exit(1);
	}

fimg_destroy(&fimg);

return 0;
}
/* --------------------------------------------------------------------- */
