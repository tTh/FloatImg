/*
 *			ADDTGA
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>

#include  <tthimage.h>

#include  "../floatimg.h"

/* --------------------------------------------------------------------- */
int add_tga_to_fimg(char *srcname, char *dstname, int notused)
{
Image_Desc	*src;
FloatImg	dst;
int		foo, x, y;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %s' )\n", __func__, srcname, dstname);
#endif

if (NULL==(src = Image_TGA_alloc_load(srcname))) {
	return -2;
	}

foo = fimg_create_from_dump(dstname, &dst);
if (foo) fprintf(stderr, "create fimg from '%s' -> %d\n", dstname, foo);
#if DEBUG_LEVEL
fimg_describe(&dst, "created fimg");
#endif

for (y=0; y<src->height; y++) {
	for (x=0; x<src->width; x++) {
		foo = fimg_add_rgb(&dst, x, y,
				(float)Image_R_pixel(src, x, y),
				(float)Image_G_pixel(src, x, y),
				(float)Image_B_pixel(src, x, y));
		}
	}

foo = fimg_dump_to_file(&dst, dstname, 0);
if (foo) { fprintf(stderr, "fimg dump -> %d\n", foo); }

return 0;
}
/* --------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo;
int		tgaW, tgaH;
int		infos[3];

if (3 != argc) {
	fimg_print_version(1);
	fprintf(stderr, "usage:\n\t%s img.tga cumul.fimg\n", argv[0]);
	exit(1);
	}

/* first, check the TGA file dimensions */
// Image_print_version(0);
foo = Image_TGA_get_dims(argv[1], &tgaW, &tgaH);
if (foo) {
	fprintf(stderr, "get dims of '%s' -> %d\n", argv[1], foo);
	Image_print_error("tga get dims", foo);
	exit(1);
	}

if ( 0==access(argv[2], R_OK) ) {		/* fimg is readable */
	fprintf(stderr, "%s is ok.\n", argv[2]);
	}
else	{
	fprintf(stderr, "*** must create '%s' %dx%d first !!!\n",
					argv[2], tgaW, tgaH);
	exit(1);
	}

foo = fimg_fileinfos(argv[2], infos);
// fprintf(stderr, "get dims of '%s' -> %d\n", argv[2], foo);
if (foo) {
	fprintf(stderr, "*** %s is badly broken\n", argv[2]);
	exit(2);
	}

if ( (tgaW != infos[0]) || (tgaW != infos[0]) ) {
	fprintf(stderr, "   TGA          %5d   %5d\n", tgaW,  tgaH);
	fprintf(stderr, "   FIMG         %5d   %5d\n", infos[0], infos[1]);
	fprintf(stderr, "   No dimension match.\n");
	exit(3);
	}

foo = add_tga_to_fimg(argv[1], argv[2], 0);

return 0;
}
/* --------------------------------------------------------------------- */


