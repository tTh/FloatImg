/*
 *	Modeles Numeriques de Terrain  --  UGLY CODE INSIDE !!
 *
 */

#include   <stdio.h>
#include   <math.h>
#include   <stdlib.h>
#include   <stdint.h>
#include   <string.h>

#include   "../floatimg.h"

int		verbosity;

/* ------------------------------------------------------------------- */
/* for debug purpose */
int printf_histo_gray(char *fname, int histo[], int nbre)
{
FILE		*fp;
int		idx; 

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s' %p %d )\n", __func__,
			fname, histo, nbre);
#endif

if (NULL == (fp = fopen(fname, "w"))) {
	perror(fname);
	exit(1);
	}
for (idx=0; idx<nbre; idx++) {
	fprintf(fp, "%6d  %8d\n", idx, histo[idx]);
	}
fclose(fp);

return -1;
}
/* ------------------------------------------------------------------- */
/* for debug purpose */
int calcul_histo_gray(FloatImg *img, char *fname, int nbslots, float *pmax)
{
int		offset, nbpix, ival;
float		pixel, minp, maxp;
int		*counts;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p '%s' %d )\n", __func__,
				img, fname, nbslots);
#endif

if (FIMG_TYPE_GRAY != img->type) {
	fprintf(stderr, "%s: image is not in greylevel\n", __func__);
	return -2;
	}

/* allocate memory for histogram computation */
counts = calloc(nbslots, sizeof(int));
if (NULL == counts) {
	fprintf(stderr, "malloc fail in %s\n", __func__);
	abort();
	}

nbpix = img->width * img->height;
minp = 1e30, maxp = -1e30;

for (offset=0; offset<nbpix; offset++) {
	pixel = img->R[offset];
	if (pixel < minp)	minp = pixel;
	if (pixel > maxp)	maxp = pixel;
	}
// fprintf(stderr, "    values =   %g  <  %g\n", minp, maxp);

*pmax = maxp;		/* copy value for the caller */

/* calcul de l'histogramme avec scaling */
for (offset=0; offset<nbpix; offset++) {
	pixel = img->R[offset];
	ival = (int)((pixel * (float)nbslots) / maxp);
	counts[ival]++;
	// fprintf(stderr, "%6d    %10.6f    %i\n", offset, pixel, ival);
	}

if (NULL != fname)
	{
	printf_histo_gray(fname, counts, nbslots);
	}

/* garbage collect stuff */
free(counts);

return 0;
}
/* ------------------------------------------------------------------- */
/*
 *		Second try - ahem?
 */
int brotche_mnt_style(FloatImg *src, FloatImg *dst)
{
FloatImg	tmp;
int		x, y, foo;
int		offset;
float		z1, z2, z3, z4;
float		a, b, c;
float		pente, minp, maxp, seuil;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( %p %p )\n", __func__, src, dst);
#endif

/*
 *	trying some preprocessor filters
 */
foo = fimg_lissage_3x3(src);
if (foo) fprintf(stderr, "    lissage 1 -> %d\n", foo);
foo = fimg_lissage_3x3(src);
if (foo) fprintf(stderr, "    lissage 2 -> %d\n", foo);

foo = fimg_killborders(src);
if (foo) fprintf(stderr, "    killborder -> %d\n", foo);

#define W    (src->width)
#define DX   1.0
#define DY   1.0

/* allocate a graylevel image for storing la 'pente'		*/
memset(&tmp, 0, sizeof(FloatImg));
foo = fimg_create(&tmp, src->width, src->height, FIMG_TYPE_GRAY);
if (foo) {
	fprintf(stderr, "create tmp pic --> %d\n", foo);
	return foo;
	}

/* calcul de la pente : a vérifier ! */
for (y=0; y<(src->height-1); y++) {
	for (x=0; x<(src->width-1); x++) {
		offset = (y * W) + x;
		z1 = src->R[offset];
		z2 = src->R[offset+1];
		z3 = src->R[offset+W];
		z4 = src->R[offset+W+1];
		a = ( z1 + z2 + z3 + z4) / 4.0;
		b = (-z1 + z2 - z3 + z4) / 2.0 / DX;
		c = (-z1 - z2 + z3 + z4) / 2.0 / DY;
		pente = atanf(sqrt(b*b + c*c));
		tmp.R[offset] = pente;
		}
	}

foo = calcul_histo_gray(&tmp, "histogramme.data", 499, &maxp);
if (foo) fprintf(stderr, "<<< calcul histo -> %d\n", foo);

minp = 1e10;
seuil = 0.700 * maxp;
fprintf(stderr, "    seuil = %f\n", seuil);

/*		ésotérisme, quand tu nous tiens...		*/
for (offset=0; offset<(src->width*src->height); offset++) {
	pente = tmp.R[offset];
	if (pente > seuil) {
		if (pente < minp)	minp = pente;
		}
	if (pente > maxp)		maxp = pente;
	}
// fprintf(stderr, "    minp = %f      maxp = %f\n", minp, maxp);

/* recopie dans l'image destination avec translation hauteur */
fimg_clear(dst);
for (offset=0; offset<(src->width*src->height); offset++) {
	pente = tmp.R[offset] - minp;
	if (pente < 0.0) {
		dst->R[offset] = -pente;
		dst->G[offset] = -pente;
		}
	else	{
		dst->B[offset] = pente;
		dst->G[offset] = pente;
		}
	}

foo = fimg_killborders(dst);
if (foo) fprintf(stderr, "    killborder -> %d\n", foo);

/* clean the memory */
fimg_destroy(&tmp);

return 0;
}
/* ------------------------------------------------------------------- */
int main(int argc, char *argv[])
{
FloatImg	src, dst;
char		*infile, *outfile;
int		foo;

verbosity = 1;		/* FIXME */

if (3 != argc) {
	fprintf(stderr, "'%s' need 2 args : infile & outfile\n", argv[0]);
	fimg_print_version(0);
	exit(1);
	}

infile = argv[1];	outfile = argv[2];

if (verbosity) fprintf(stderr,"*** MNT    %s -> %s\n", infile, outfile);

foo = fimg_create_from_dump(infile, &src);
if (foo) {
	fprintf(stderr, "err %d loading image '%s'\n", foo, infile);
	exit(1);
	}

foo = fimg_clone(&src, &dst, 0);
if (foo) {
	fprintf(stderr, "err %d cloning image\n", foo);
	exit(1);
	}
fimg_clear(&dst);

foo = brotche_mnt_style(&src, &dst);
if (foo) {
	fprintf(stderr, "something weird happen %d\n", foo);
	exit(1);
	}

foo = fimg_export_picture(&dst, outfile, 0);
if (foo) {
	fprintf(stderr, "err %d exporting to %s\n", foo, outfile);
	exit(1);
	}

/* clean the memory */
fimg_destroy(&src);
fimg_destroy(&dst);

return 0;
}
/* ------------------------------------------------------------------- */
