/*
 *		another ugly experiment (please explain)
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <stdint.h>
#include  <string.h>

#include  "../floatimg.h"

int		verbosity;

/* --------------------------------------------------------------- */

int triplane_muxer(FloatImg *sr, FloatImg *sg, FloatImg *sb,
					FloatImg *dst)
{
int		sz;

if (FIMG_TYPE_RGB != dst->type) {
	fprintf(stderr, "%s: dst picz must be RGB, was %d\n",
					__func__, dst->type);
	return -99;
	}

if ( fimg_images_not_compatible(sr, sg) ||
     fimg_images_not_compatible(sr, sb) ||
     fimg_images_not_compatible(sr, dst) ) {
	fprintf(stderr, "%s: compatibility error\n", __func__);
	return -2;
	}

sz = sr->width * sr->height * sizeof(float);

memcpy(dst->R, sr->R, sz);
memcpy(dst->G, sg->G, sz);
memcpy(dst->B, sb->B, sz);

return 0;
}
/* --------------------------------------------------------------- */
int try_this_muxplane(char *fr, char *fg, char *fb, char *dst, int flags)
{
int		foo;
FloatImg	imr, img, imb, dest;

fprintf(stderr, "muxing: %s %s %s -> %s\n", fr, fg, fb, dst);

if (flags) {
	fprintf(stderr, "%s: flag non 0 ?\n", __FILE__);
	}

foo = fimg_create_from_dump(fr, &imr);
if (foo) {
	fprintf(stderr, "%s: err %d loading %s\n", __func__, foo, fr);
	return -1;
	}
foo = fimg_create_from_dump(fg, &img);
if (foo) {
	fprintf(stderr, "%s: err %d loading %s\n", __func__, foo, fg);
	return -1;
	}
foo = fimg_create_from_dump(fb, &imb);
if (foo) {
	fprintf(stderr, "%s: err %d loading %s\n", __func__, foo, fb);
	return -1;
	}

fimg_clone(&imr, &dest, 0);

foo = triplane_muxer(&imr, &img, &imb, &dest);
if (foo) {
	fprintf(stderr, "%s: err %d\n", __func__, foo);
	return foo;
	}

foo = fimg_export_picture(&dest, dst, 0);
if (foo) {
	fprintf(stderr, "%s: err %d exporting to %s\n", __func__, foo, dst);
	return foo;
	}

fimg_destroy(&imr);		fimg_destroy(&img);
fimg_destroy(&imb);		fimg_destroy(&dest);

return 0;
}
/* --------------------------------------------------------------- */
int main(int argc, char *argv[])
{
int		foo;

if (5 != argc) {
	fprintf(stderr, "ERROR: %s need four fimg files arguments\n",
				argv[0]);
	return 1;
	}

/*
 *	and now, we have to decipher options on the
 *	command line.
 */

foo = try_this_muxplane(argv[1], argv[2], argv[3], argv[4], 0);
if (foo) {
	fprintf(stderr, "oups %d\n", foo);
	exit(1);
	}

return 0;
}
/* --------------------------------------------------------------- */
