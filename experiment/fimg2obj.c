
/*
 *		another ugly experiment
 */
#include  <stdio.h>
#include  <stdlib.h>
#include  <unistd.h>
#include  <stdint.h>

#include  "../floatimg.h"

int		verbosity;

/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */

int convert_fimg_to_obj(char *fimgfname, char *objfname, int mode)
{
FloatImg	src;
int		foo, x, y;
FILE		*fp;

#if DEBUG_LEVEL
fprintf(stderr, ">>> %s ( '%s'  '%s'  %d )\n", __func__,
			fimgfname, objfname, mode);
#endif

if (mode) {
	fprintf(stderr, "in %s(), mode must be 0, was %d\n", __func__, mode);
	}

foo = fimg_create_from_dump(fimgfname, &src);
if (foo) {
	fprintf(stderr, "err %d loading %f\n", foo, fimgfname);
	return foo;
	}


	
return -1;
}
/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */

int main(int argc, char *argv[])
{
int		foo, opt;
char		*infile  = "foo.fimg";

fprintf(stderr, "*** fimg2obj   (%s %s)\n", __DATE__, __TIME__);

verbosity = 0;

#if 0
for (foo=0; foo<argc; foo++) {
	fprintf(stderr, "%9d   %s\n", foo, argv[foo]);
	}
#endif

foo = convert_fimg_to_obj(infile, "foo.obj", 0);
if (foo) {
	fprintf(stderr, "convertor give us %d\n", foo);
	exit(1);
	}

return 0;
}

/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */
