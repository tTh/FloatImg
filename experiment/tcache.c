/*
 *		tests du systeme de cache
 */

#include  <stdio.h>
#include  <sys/time.h>

#include  "../floatimg.h"
#include  "cachengn.h"

int		verbosity;

#define IMGW	320
#define IMGH	240

#define SIZE	5		// number of slots
#define NBRI	1000

/* ------------------------------------------------------------ */
int main(int argc, char *argv[])
{
int		foo;
FloatImg	*picz;
char		*fname = "quux.fimg";

fprintf(stderr, "\nTest of the cache engin - %s %s\n",
					__DATE__, __TIME__);
cachengn_print_version(1);

foo = init_empty_cache(IMGW, IMGH, SIZE, NBRI);
fprintf(stderr, "    init_empty_cache --> %d\n", foo);

foo = liste_le_cache((unsigned int)'U');
fprintf(stderr, "    liste le cache --> %d\n", foo);

picz = give_me_thiz_picz(fname, 0);
if (NULL == picz) {
	fprintf(stderr, "    error 'givemeapicz' on '%s'\n", fname);
	}

picz = give_me_thiz_picz(fname, 0);
if (NULL == picz) {
	fprintf(stderr, "    error 'givemeapicz' on '%s'\n", fname);
	}

return 0;
}
/* ------------------------------------------------------------ */

