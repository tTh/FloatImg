#!/bin/bash

GRABOPT=" -vv -d /dev/video0 -n 400 -p 0.5 -u "
SPOOL=${HOME}/TMP

echo ; echo ; echo

for capture in red green blue
do
	image=${SPOOL}/${capture}.fimg
	echo grabbing $image
	grabvidseq ${GRABOPT} -o $image
	echo
done

./muxplanes 	"${SPOOL}/red.fimg"			\
		"${SPOOL}/green.fimg"			\
		"${SPOOL}/blue.fimg"			\
		yo.fimg

echo $0 "got a" $?

fimgstats -v yo.fimg

