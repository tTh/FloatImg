/*
 *		the chache engine - code
 */

#include  <stdio.h>
#include  <unistd.h>
#include  <sys/time.h>
#include  <stdlib.h>
#include  <string.h>

#include  "../floatimg.h"
#include  "cachengn.h"

extern int		verbosity;

static int		nombre_slots = -1;
static int		index_slot   = -1;
static FimgCacheEntry	*le_cache    = NULL;

/* ------------------------------------------------------------ */

void cachengn_print_version(int k)
{

printf("\t!!! this is the version ZERO !!!\n");

fprintf(stderr, "sizeof cache entry: %ld\n", sizeof(FimgCacheEntry));

if (k) fimg_print_version(0);
}
/* ------------------------------------------------------------ */
/*
 *
 */
int init_empty_cache(int iw, int ih, int szc, int wtfparam)
{
int		idx;

fprintf(stderr, ">>> %s ( %d %d %d   %d )\n", __func__,
			iw, ih,  szc, wtfparam);

/* MOLLYGUARD : don't init TWICE, please */
if (NULL != le_cache) {
	fprintf(stderr, "%s: there is a cache at %p\n", __func__,
						le_cache);
	return -2;
	}

/* allocate an prepare memory */
if ( NULL==(le_cache=calloc(szc, sizeof(FimgCacheEntry))) ) {
	fprintf(stderr, "%s: no memory, sorry...\n", __func__);
	return -3;
	}
fprintf(stderr, "    slot array at %p\n", le_cache);
for (idx=0; idx<szc; idx++) {
	le_cache[idx].flags = 0xF0;
	le_cache[idx].index = idx;
	}

/* update private cache metadata */
nombre_slots = szc;

return 0;
}

/* ------------------------------------------------------------ */
/*
 *
 */
FloatImg *give_me_thiz_picz(char *fname, int notused)
{
int			idx, foo, freeslot;
FloatImg		img;
char			*nptr;

fprintf(stderr, ">>> %s ( '%s' %d )\n", __func__, fname, notused);

/* please add molly guard ! */

if (notused)
	fprintf(stderr, "in %s, notused was %d\n", __func__, notused);

/* is the floatimg already in the cahce ? */
for (idx=0; idx<nombre_slots; idx++) {
	nptr = le_cache[idx].filename;
	if (NULL!=nptr && (! strcmp(fname, nptr))) {
		fprintf(stderr, "found '%s' at %d\n", nptr, idx);
		return le_cache[idx].image;
		}
	}

/* we not have this picture in our cache, so we need a free
   slot for it */
freeslot = -1;
for (idx=0; idx<nombre_slots; idx++) {
	if (NULL==le_cache[idx].image) {
		freeslot = idx;
		break;
		}
	}
fprintf(stderr, "freeslot = %d\n", freeslot);

/* check if we can read this file */
foo = access(fname, R_OK);			/* XXX */
if (foo) {
	perror("give_me_thiz_picz");
	return NULL;
	}
fprintf(stderr, "file '%s' is readble\n", fname);

/* try to load the requested file */
foo = fimg_create_from_dump(fname, &img);
if (foo) {
	fprintf(stderr, "oups on %s\n", fname);
	return NULL;
	}
/* OK, we have all the pixels in core memory */

return NULL;
}
/* ------------------------------------------------------------ */
/*
 *
 */
int liste_le_cache(unsigned int flags)
{
int		idx;

fprintf(stderr, ">>> %s ( Ox%X )\n", __func__, flags);

/* please add molly guard here */

fprintf(stderr, "cache at %p : %d slots, idx = %d\n",
				le_cache, nombre_slots, index_slot);

for (idx=0; idx<nombre_slots; idx++) {
	fprintf(stderr, "%5d    ", idx);
	fprintf(stderr, "0x%02x ", le_cache[idx].flags);
	fprintf(stderr, "%p     ", le_cache[idx].image);
	fprintf(stderr, "%s\n",    le_cache[idx].filename);
	}

return 0;
}
/* ------------------------------------------------------------ */
