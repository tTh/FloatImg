/*
 *		assemblage experimental
 */

#include  <stdio.h>
#include  <stdlib.h>
#include  <stdint.h>

#include  "../floatimg.h"

// #include  "incrustator.h"

int		verbosity;

/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */
/*
 *	le prototype de base avec trop peu de parametres, mais
 *	c'est assez dur de formaliser la structure des informations
 *	a transmettre de fonction en fonction, et commant on peut
 *	integrer cette mecanique dans singlepass.c ?!#@
 */
int premier_essai(int largeur, int hauteur, char *outname)
{
FloatImg	grande, incrust;
int		foo;

foo = fimg_create(&grande, largeur, hauteur, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s: Kkrkr %d pour create grande\n", __func__, foo);
	return -1;
	}
fimg_vdeg_a(&grande, 2345);

foo = fimg_create(&incrust, 640, 480, FIMG_TYPE_RGB);
if (foo) {
	fprintf(stderr, "%s: Kkrkr %d pour create incrust\n", __func__, foo);
	return -1;
	}
// fimg_drand48(&incrust, 13.37);
foo = fimg_load_from_dump("foo.fimg", &incrust);
if (foo) {
	fprintf(stderr, "%s: err %d loading image\n", __func__, foo);
	return -1;
	}

#define FLAGS   0
foo = fimg_incrustator_0(&incrust, &grande, 111, 111, FLAGS);
#undef FLAGS
if (foo) {
	fprintf(stderr, "%s: err %d sur incrustator_0\n", __func__, foo);
	return -1;
	}

foo =  fimg_export_picture(&grande, outname, 0);
if (foo) {
	fprintf(stderr, "%s: error %d export '%s'\n", __func__,
					foo, outname);
	return -1;
	}

fimg_destroy(&incrust);		fimg_destroy(&grande);

return 0;
}
/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */
int main(int argc, char *argv[])
{
int		foo;

verbosity = 2;

fimg_print_version(1);

foo = premier_essai(1280, 1024, "out.png");
if (foo) {
	fprintf(stderr, "EPIC FAIL %s\n", argv[0]);
	exit(1);
	}

return 0;
}
/* ---------------------------------------------- ~~~~~~~~~~~~~~~~ */



