#!/bin/bash

make extracteur

IMGS="foo.fimg"

for idx in $(seq 0 69)
do
	outf=$(printf "%s/X%04d.png" "/tmp" $idx)
	echo "work on "$outf

	x=$(( idx * 4  ))
	y=$(( idx + 80 ))

	./extracteur $IMGS 256,128,${x},${y} $outf
	error=$?
	if [ 0 -ne $error ] ; then
		echo error $error
		exit 1
	fi
	echo

done

convert -delay 10 /tmp/X????.png foo.gif

