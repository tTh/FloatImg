/*
 *		the chache engine - header
 */

void cachengn_print_version(int k);

typedef struct {
	int		flags;
	char		*filename;
	FloatImg	*image;
	int		index;
	} FimgCacheEntry;


/*
 *		parameters:
 *	- iw, ik   : image size
 *	- szc      : number of slots
 *	- nbre     : WTF isthat?
 */
int init_empty_cache(int iw, int ih, int szc, int nbre);


/* /!\ the floatimg returned must be view as readonly */
FloatImg *give_me_thiz_picz(char *fname, int notused);


/* utilities functions */
int liste_le_cache(unsigned int flags);
