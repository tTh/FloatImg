# Expérimentations ÀLC

Attention, tout ce qui se trouve dans ce répertoire ne sont que des
essais. Certains aboutissent, et sont migrés vers `lib/`, `funcs/` ou
`tools/`, d'autre échouent et restent trainer dans le coin en attente
du retour de l'inspiration.

Le contenu de ce répertoire doit donc être considéré comme
**volatile**. Si vous y trouvez votre bonheur, il serait sage
d'en faire une copie personnelle...

## MNT

Modèles numériques de terrain. Rien ne marche. Plein de maths.
Un [code](mnt.c) complexe.

## fimg2obj

Création d'un fichier .OBJ (de Wavefront) à partir d'une image
flottante afin d'avoir des vues en 3d pertinentes, bien qu'assez
futiles.

## système de cache

`new: Mon Jul 17 12:49:20 UTC 2023`

En cours : définition d'à peu-près tout ce qu'il reste à faire
pour avoir quelque chose qui fonctionne.
Plus d'information dans le [.h](cachengn.h).
